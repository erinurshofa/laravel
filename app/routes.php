<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function () {

    Route::get('/', 'HomeController@dashboard');
    Route::get('dashboard', 'HomeController@dashboard');
    Route::resource('vendors', 'VendorController');
    Route::post(
        'vendors/search', array(
            'as' => 'vendors.search', 
            'uses' => 'VendorController@searchVendor'
        )
    );
    //rute memproses ke vendor controller*/
    Route::resource('warehouses', 'WarehouseController');
    Route::post(
        'warehouses/search', array(
            'as' => 'warehouses.search', 
            'uses' => 'WarehouseController@searchWarehouse'
        )
    );
    //rute memproses ke project controller*/
    Route::resource('projects', 'ProjectController');
    Route::post(
        'projects/search', array(
            'as' => 'projects.search', 
            'uses' => 'ProjectController@searchProject'
        )
    );
    //rute memproses ke material_categories controller*/
    Route::resource('material_categories', 'MaterialCategoryController');
    Route::post(
        'material_categories/search', array(
            'as' => 'material_categories.search', 
            'uses' => 'MaterialCategoryController@searchMaterialCategory'
        )
    );
    //rute memproses ke material controller*/
    Route::resource('materials', 'MaterialController');
    Route::post(
        'materials/search', array(
            'as' => 'materials.search', 
            'uses' => 'MaterialController@searchMaterial'
        )
    );
    //rute memproses ke material controller*/
    Route::resource('users', 'UserController');
    Route::post(
        'users/search', array(
            'as' => 'users.search', 
            'uses' => 'UserController@searchUser'
        )
    );
    //rute memproses ke project doc controller*/
    Route::resource('project_docs', 'ProjectDocController');
    Route::post(
        'project_docs/search', array(
            'as' => 'project_docs.search', 
            'uses' => 'ProjectDocController@searchProjectDoc'
        )
    );
    Route::resource('material_docs', 'MaterialDocController');
    Route::post(
        'material_docs/search', array(
            'as' => 'material_docs.search', 
            'uses' => 'MaterialDocController@searchMaterialDoc'
        )
    );

    /*
    |--------------------------------------------------------------------------
    | Transaction Material Take Off
    |--------------------------------------------------------------------------
    |
    | Your description about material take off.
    | 
    | 
    |
    */
    Route::resource('material_take_offs', 'MaterialTakeOffController');
    Route::post(
        'material_take_offs/search', array(
            'as' => 'material_take_offs.search', 
            'uses' => 'MaterialTakeOffController@searchMaterialTakeOff'
        )
    );
    //create material_take_off with ajax
    Route::get(
        'material_take_offs/create/createWithAjax', array(
            'as' => 'material_take_offs.createWithAjax', 
            'uses' => 'MaterialTakeOffController@createWithAjax',function($material){
                return $material;
            }
        )
    );
    //edit material_take_off with ajax
    Route::get(
        'material_take_offs/{id}/edit/editWithAjax', array(
            'as' => 'material_take_offs.editWithAjax', 
            'uses' => 'MaterialTakeOffController@editWithAjax',function($material){
                return $material;
            }
        )
    );
    //route for initialize in grid
    Route::get(
        'material_take_offs/create/find/{code}', array(
            'as' => 'material_take_offs.find', 
            'uses' => 'MaterialTakeOffController@find',function($code){
                return $code;
            }
        )
    );
    //route for autocomplete laravel
    Route::get('material_take_offs/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('material_take_offs/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'material_take_offs/findMaterialTakeOffByCode/{code}', array(
            'as' => 'material_take_offs.find', 
            'uses' => 'MaterialTakeOffController@find',function($code){
                return $code;
            }
        )
    );
    //get material take off detail by materialtakeoffid in edit view with parameter material_take_off_id
    Route::get(
        'material_take_offs/getListMaterialTakeOffDetail/{material_take_off_id}', array(
            'as' => 'material_take_offs.listMaterialTakeOffDetail', 
            'uses' => 'MaterialTakeOffController@listMaterialTakeOffDetail',function($material_take_off_id){
                return $material_take_off_id;
            }
        )
    );
    //get material by materialtakeoffid in edit view with parameter material_take_off_id
    Route::get(
        'material_take_offs/getListMaterial/{material_take_off_id}', array(
            'as' => 'material_take_offs.listMaterial', 
            'uses' => 'MaterialTakeOffController@listMaterial',function($material_take_off_id){
                return $material_take_off_id;
            }
        )
    );
    /*
    |--------------------------------------------------------------------------
    | Transaction Purchase Order
    |--------------------------------------------------------------------------
    |
    | Your description about Purchase Order.
    | 
    |   
    |
    */
    Route::resource('purchase_orders', 'PurchaseOrderController');
    Route::post(
        'purchase_orders/search', array(
            'as' => 'purchase_orders.search', 
            'uses' => 'PurchaseOrderController@searchPurchaseOrder'
        )
    );
    //create purchase order with ajax
    Route::get(
        'purchase_orders/create/createWithAjax', array(
            'as' => 'purchase_orders.createWithAjax', 
            'uses' => 'PurchaseOrderController@createWithAjax',function($material){
                return $material;
            }
        )
    );
    //get material by code and initialize in grid
    Route::get(
        'purchase_orders/create/find/{code}', array(
            'as' => 'purchase_orders.find', 
            'uses' => 'PurchaseOrderController@find',function($code){
                return $code;
            }
        )
    );
    //route for autocomplete laravel
    Route::get('purchase_orders/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('purchase_orders/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'purchase_orders/findMaterialTakeOffByCode/{code}', array(
            'as' => 'purchase_orders.find', 
            'uses' => 'PurchaseOrderController@find',function($code){
                return $code;
            }
        )
    );
    //get purchase order detail by purchase order id in edit view with parameter purchase_order_id
    Route::get(
        'purchase_orders/getListPurchaseOrderDetail/{purchase_order_id}', array(
            'as' => 'purchase_orders.listPurchaseOrderDetail', 
            'uses' => 'PurchaseOrderController@listPurchaseOrderDetail',function($purchase_order_id){
                return $purchase_order_id;
            }
        )
    );
    //get material by materialtakeoffid in edit view with parameter material_take_off_id
    Route::get(
        'purchase_orders/getListMaterial/{material_take_off_id}', array(
            'as' => 'purchase_orders.listMaterial', 
            'uses' => 'PurchaseOrderController@listMaterial',function($material_take_off_id){
                return $material_take_off_id;
            }
        )
    );

//edit purchase_order with ajax
Route::get(
    'purchase_orders/{id}/edit/editWithAjax', array(
        'as' => 'purchase_orders.editWithAjax', 
        'uses' => 'PurchaseOrderController@editWithAjax',function($material){
            return $material;
        }
    )
);
    /*
    |--------------------------------------------------------------------------
    | Transaction Material Construction
    |--------------------------------------------------------------------------
    |
    | Your description about Material Construction.
    | 
    | 
    |
    */
    Route::resource('material_constructions', 'MaterialConstructionController');
    Route::post(
        'material_constructions/search', array(
            'as' => 'material_constructions.search', 
            'uses' => 'MaterialConstructionController@searchMaterialConstruction'
        )
    );
    //create material construction with ajax
    Route::get(
        'material_constructions/create/createWithAjax', array(
            'as' => 'material_constructions.createWithAjax', 
            'uses' => 'MaterialConstructionController@createWithAjax',function($material){
                return $material;
            }
        )
    );
    //get material by code and initialize in grid
    Route::get(
        'material_constructions/create/find/{code}', array(
            'as' => 'material_constructions.find', 
            'uses' => 'MaterialConstructionController@find',function($code){
                return $code;
            }
        )
    );
    //route for autocomplete laravel
    Route::get('material_constructions/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('material_constructions/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'material_constructions/findMaterialConstructionByCode/{code}', array(
            'as' => 'material_constructions.find', 
            'uses' => 'MaterialConstructionController@find',function($code){
                return $code;
            }
        )
    );
    //get material construction detail by material construction id in edit view with parameter material_construction_id
    Route::get(
        'material_constructions/getListMaterialConstructionDetail/{material_construction_id}', array(
            'as' => 'material_constructions.listMaterialConstructionDetail', 
            'uses' => 'MaterialConstructionController@listMaterialConstructionDetail',function($material_construction_id){
                return $material_construction_id;
            }
        )
    );
    //get material by materialConstructionid in edit view with parameter material_construction_id
    Route::get(
        'material_constructions/getListMaterial/{material_construction_id}', array(
            'as' => 'material_constuctions.listMaterial', 
            'uses' => 'MaterialConstructionController@listMaterial',function($material_construction_id){
                return $material_construction_id;
            }
        )
    );
    //edit material construction with ajax
    Route::get(
        'material_constructions/{id}/edit/editWithAjax', array(
            'as' => 'material_constructions.editWithAjax', 
            'uses' => 'MaterialConstructionController@editWithAjax',function($material){
                return $material;
            }
        )
    );
    /*
    |--------------------------------------------------------------------------
    | Transaction Material Issuance
    |--------------------------------------------------------------------------
    |
    | Your description about Material Issuance.
    | 
    | 
    |
    */
    Route::resource('material_issuances', 'MaterialIssuanceController');
    Route::post(
        'material_issuances/search', array(
            'as' => 'material_issuances.search', 
            'uses' => 'MaterialIssuanceController@searchMaterialIssuance'
        )
    );

    Route::get(
        'material_issuances/create/createWithAjax', array(
            'as' => 'material_issuances.createWithAjax', 
            'uses' => 'MaterialIssuanceController@createWithAjax',function($material){
                return $material."tester";
            }
        )
    );
    //get material by code and initialize in grid
    Route::get(
        'material_issuances/create/find/{code}', array(
            'as' => 'material_issuances.find', 
            'uses' => 'MaterialIssuanceController@find',function($code){
                return $code;
            }
        )
    );
    //route for autocomplete laravel
    Route::get('material_issuances/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('material_issuances/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'material_issuances/findMaterialIssuanceByCode/{code}', array(
            'as' => 'material_issuances.find', 
            'uses' => 'MaterialIssuanceController@find',function($code){
                return $code;
            }
        )
    );
    //get material receiving detail by material receiving id in edit view with parameter material_receiving_id
    Route::get(
        'material_issuances/getListMaterialIssuanceDetail/{material_issuance_id}', array(
            'as' => 'material_issuances.listMaterialIssuanceDetail', 
            'uses' => 'MaterialIssuanceController@listMaterialIssuanceDetail',function($material_issuance_id){
                return $material_issuance_id;
            }
        )
    );
    //get material by materialIssuanceid in edit view with parameter material_issuance_id
    Route::get(
        'material_issuances/getListMaterial/{material_issuance_id}', array(
            'as' => 'material_issuances.listMaterial', 
            'uses' => 'MaterialIssuanceController@listMaterial',function($material_issuance_id){
                return $material_issuance_id;
            }
        )
    );
    //edit material issuance with ajax
    Route::get(
        'material_issuances/{id}/edit/editWithAjax', array(
            'as' => 'material_issuances.editWithAjax', 
            'uses' => 'MaterialIssuanceController@editWithAjax',function($material){
                return $material;
            }
        )
    );
    /*
    |--------------------------------------------------------------------------
    | Transaction Material Receiving
    |--------------------------------------------------------------------------
    |
    | Your description about Material Receiving.
    | 
    | 
    |
    */
    Route::resource('material_receivings', 'MaterialReceivingController');
    Route::post(
        'material_receivings/search', array(
            'as' => 'material_receivings.search', 
            'uses' => 'MaterialReceivingController@searchMaterialReceiving'
        )
    );
    //create material receiving with ajax
    Route::get(
        'material_receivings/create/createWithAjax', array(
            'as' => 'material_receivings.createWithAjax', 
            'uses' => 'MaterialReceivingController@createWithAjax',function($material){
                return $material;
            }
        )
    );
    //get material by code and initialize in grid
    Route::get(
        'material_receivings/create/find/{code}', array(
            'as' => 'material_receivings.find', 
            'uses' => 'MaterialReceivingController@find',function($code){
                return $code;
            }
        )
    );
    //route for autocomplete laravel
    Route::get('material_receivings/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('material_receivings/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'material_receivings/findMaterialReceivingByCode/{code}', array(
            'as' => 'material_receivings.find', 
            'uses' => 'MaterialReceivingController@find',function($code){
                return $code;
            }
        )
    );
    //get material receiving detail by material receiving id in edit view with parameter material_receiving_id
    Route::get(
        'material_receivings/getListMaterialReceivingDetail/{material_receiving_id}', array(
            'as' => 'material_receivings.listMaterialReceivingDetail', 
            'uses' => 'MaterialReceivingController@listMaterialReceivingDetail',function($material_receiving_id){
                return $material_receiving_id;
            }
        )
    );
    //get material by materialtakeoffid in edit view with parameter material_receiving_id
    Route::get(
        'material_receivings/getListMaterial/{material_receiving_id}', array(
            'as' => 'material_receivings.listMaterial', 
            'uses' => 'MaterialReceivingController@listMaterial',function($material_receiving_id){
                return $material_receiving_id;
            }
        )
    );

    //edit material receiving with ajax
    Route::get(
        'material_receivings/{id}/edit/editWithAjax', array(
            'as' => 'material_receivings.editWithAjax', 
            'uses' => 'MaterialReceivingController@editWithAjax',function($material){
                return $material;
            }
        )
    );
    /*
    |--------------------------------------------------------------------------
    | Transaction Nonconformance
    |--------------------------------------------------------------------------
    |
    | Your description about nonconformance.
    | 
    | 
    |
    */
    Route::resource('nonconformances', 'NonconformanceController');
    Route::post(
        'nonconformances/search', array(
            'as' => 'nonconformances.search', 
            'uses' => 'NonconformanceController@searchNonconformance'
        )
    );
    //create nonconformance with ajax
    Route::get(
        'nonconformances/create/createWithAjax', array(
            'as' => 'nonconformances.createWithAjax', 
            'uses' => 'NonconformanceController@createWithAjax',function($material){
                return $material;
            }
        )
    );
    //edit nonconformance with ajax
    Route::get(
        'nonconformances/{id}/edit/editWithAjax', array(
            'as' => 'nonconformances.editWithAjax', 
            'uses' => 'NonconformanceController@editWithAjax',function($material){
                return $material;
            }
        )
    );
    //route for initialize in grid
    Route::get(
        'nonconformances/create/find/{code}', array(
            'as' => 'nonconformances.find', 
            'uses' => 'NonconformanceController@find',function($code){
                return $code;
            }
    )
);
    //route for autocomplete laravel
    Route::get('nonconformances/create/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get('nonconformances/{id}/edit/autocompleteCodeMaterial',function(){
        $term=Input::get('term');
        $materials =Material::where('code', 'like', '%'.$term.'%')->Get();
        $results = array();
        foreach ($materials as $material)
        {
            $results[]=array(
                "id" => $material->id,
                "value"=>$material->code ,
                "label"=>$material->code." - ".$material->name,
            );
        }
        return $results;
    });
    Route::get(
        'nonconformances/findNonconformanceByCode/{code}', array(
            'as' => 'nonconformances.find', 
            'uses' => 'NonconformanceController@find',function($code){
                return $code;
            }
        )
    );
    //get nonconformance detail by nonconformanceid in edit view with parameter nonconformance_id
    Route::get(
        'nonconformances/getListNonconformanceDetail/{nonconformance_id}', array(
            'as' => 'nonconformances.listNonconformanceDetail', 
            'uses' => 'NonconformanceController@listNonconformanceDetail',function($nonconformance_id){
                return $nonconformance_id;
            }   
        )
    );
    //get material by nonconformanceid in edit view with parameter nonconforamance_id
    Route::get(
        'nonconformances/getListMaterial/{nonconformance_id}', array(
            'as' => 'nonconformances.listMaterial', 
            'uses' => 'NonconformanceController@listMaterial',function($nonconformance_id){
                return $nonconformance_id;
            }
        )
    );

    /*
    |--------------------------------------------------------------------------
    | Reports
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all of the routes for an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the Closure to execute when that URI is requested.
    |
    */
    Route::get('reports/projects',array('uses'=>'ReportController@listProject'));
    Route::get('reports/project_docs',array('uses'=>'ReportController@listProjectDoc'));
    Route::get('reports/vendors',array('uses'=>'ReportController@listVendor'));
    Route::get('reports/warehouses',array('uses'=>'ReportController@listWarehouse'));
    Route::get('reports/users',array('uses'=>'ReportController@listUser'));
    Route::get('reports/material_categories',array('uses'=>'ReportController@listMaterialCategory'));
    Route::get('reports/materials',array('uses'=>'ReportController@listMaterial'));
    Route::get('reports/material_docs',array('uses'=>'ReportController@listMaterialDoc'));

    Route::get('reports/material_take_offs',array('uses'=>'ReportController@listMaterialTakeOff'));
    Route::get('reports/purchase_orders',array('uses'=>'ReportController@listPurchaseOrder'));
    Route::get('reports/material_receivings',array('uses'=>'ReportController@listMaterialReceiving'));
    Route::get('reports/material_issuances',array('uses'=>'ReportController@listMaterialIssuance'));
    Route::get('reports/material_constructions',array('uses'=>'ReportController@listMaterialConstruction'));
    Route::get('reports/nonconformances',array('uses'=>'ReportController@listNonconformance'));

});


//rute memproses login logout dan register*/
Route::get('logout', array('uses' => 'HomeController@doLogout'));
Route::get('login',array('uses'=>'HomeController@showLogin'));
Route::post('login',array('uses'=>'HomeController@doLogin'));
Route::get('register',array('uses'=>'HomeController@showRegister'));
Route::post('register',array('uses'=>'HomeController@doRegister'));

