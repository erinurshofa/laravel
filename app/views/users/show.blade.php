<!-- app/views/users/show.blade.php -->
@extends('layout')

@section('title')
Show User
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Showing User</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        User :{{$user->username}}<br>
    </div> 
@stop

@section('footer')
    @parent
@stop