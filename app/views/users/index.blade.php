<!-- app/views/users/index.blade.php -->
@extends('layout')

@section('title')
User List
@stop
@section('menu')
    @parent
@stop
@section('content')
    <h1>All the Users</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('users/create') }}">Add User</a>
    <a class="button white" href="{{ URL::to('reports/users') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('users.search'))) }}
      {{ Form::text('query', null, array( 'placeholder' => 'Search user...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>User Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $key => $value)
            <tr>
                <!-- <td>{{ $value->id }}</td> -->
                <td>{{ $value->username }}</td>
                <td>
                <div>
                  <!-- show the user (uses the show method found at GET /users/{id} -->
                    <a class="button blue" href="{{ URL::to('users/' . $value->id) }}">Show this User</a>
                    <!-- edit this user (uses the edit method found at GET /users/{id}/edit -->
                    <a class="button green" href="{{ URL::to('users/' . $value->id . '/edit') }}">Edit this User</a>
                    {{Form::model($value->id, array('route' => array('users.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
@section('footer')
    @parent
@stop