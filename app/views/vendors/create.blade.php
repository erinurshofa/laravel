<!-- app/views/vendors/create.blade.php -->

@extends('layout')

@section('title')
Create Vendor
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Vendor</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::open(array('url' => 'vendors')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', Input::old('code')) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', Input::old('name')) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', Input::old('description')) }}
  </div>
    <div>
    {{ Form::label('address', 'Address') }}<br/>
    {{ Form::text('address', Input::old('adress')) }}
  </div>
    <div>
    {{ Form::label('phone', 'Phone') }}<br/>
    {{ Form::text('phone', Input::old('phone')) }}
  </div>


<br/>
  {{ Form::submit('Create the Vendor!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop