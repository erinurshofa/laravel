<!-- app/views/vendors/show.blade.php -->

@extends('layout')

@section('title')
Show Vendor
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Vendors</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Vendor :{{$vendor->code}}<br>
        Name :{{$vendor->name}}<br>
        Description :{{$vendor->description}}<br>
        Address :{{$vendor->address}}<br>
        Phone :{{$vendor->phone}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop