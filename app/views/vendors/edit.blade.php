<!-- app/views/vendors/edit.blade.php -->

@extends('layout')

@section('title')
Edit Vendor
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Vendor</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($vendor, array('route' => array('vendors.update', $vendor->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', null) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', null) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', null) }}
  </div>
    <div>
    {{ Form::label('address', 'Address') }}<br/>
    {{ Form::text('address', null) }}
  </div>
    <div>
    {{ Form::label('phone', 'Phone') }}<br/>
    {{ Form::text('phone', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Vendor!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop