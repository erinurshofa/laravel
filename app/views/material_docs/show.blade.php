<!-- app/views/material_docs/show.blade.php -->

@extends('layout')

@section('title')
Show Material Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Doc</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Material :
        {{Material::where('id','=',$material_doc->material_id)->lists('name','id')[$material_doc->material_id]}}<br>
        Doc Content :{{$material_doc->doc_content}}<br>
        Octet Type :{{$material_doc->octet_type}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop