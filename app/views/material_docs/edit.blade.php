<!-- app/views/material_docs/edit.blade.php -->

@extends('layout')

@section('title')
Edit Material Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Material Doc</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($material_doc, array('route' => array('material_docs.update', $material_doc->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('material_id', 'Material') }}<br/>
    {{ Form::select('material_id',$populate_material, null) }}
  </div>
  <div>
    {{ Form::label('doc_content', 'Doc Content') }}<br/>
    {{ Form::text('doc_content', null) }}
  </div>
    <div>
    {{ Form::label('octet_type', 'Octet Type') }}<br/>
    {{ Form::text('octet_type', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Material Doc!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop