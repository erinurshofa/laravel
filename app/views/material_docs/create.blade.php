<!-- app/views/material_docs/create.blade.php -->

@extends('layout')
@section('title')
Create Material Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Material Doc</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}
{{ Form::open(array('url' => 'material_docs')) }}
  <div>
    {{ Form::label('material_id', 'Material') }}<br/>
    {{ Form::select('material_id', $populate_material , Input::old('material_docs')) }}
  </div>
  <div>
    {{ Form::label('doc_content', 'Doc Content') }}<br/>
    {{ Form::text('doc_content', Input::old('doc_content')) }}
  </div>
    <div>
    {{ Form::label('octet_type', 'Octet Type') }}<br/>
    {{ Form::text('octet_type', Input::old('octet_type')) }}
  </div>

<br/>
  {{ Form::submit('Create the Material Doc!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop