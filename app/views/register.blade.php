@extends('layout')

@section('content')
<h1>Register!</h1>

    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
{{ Form::open(array('url' => 'register')) }}

{{ Form::label('username', 'Username') }}<br/>
{{ Form::text('username', Input::old('username')) }} {{ $errors->first('username') }}<br/>
{{ Form::label('email', 'E-mail') }}<br/>
{{ Form::text('email', Input::old('email')) }} {{ $errors->first('email') }}<br/>
{{ Form::label('password', 'Password') }}<br/>
{{ Form::text('password', Input::old('password')) }}  {{ $errors->first('password') }}<br/>

{{ Form::submit('Register!',array('class'=>'button orange')) }}

{{ Form::token() . Form::close() }}

@stop