<!-- app/views/warehouses/create.blade.php -->

@extends('layout')

@section('title')
Create Warehouse
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Warehouse</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::open(array('url' => 'warehouses')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', Input::old('code')) }}
  </div>
  <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', Input::old('description')) }}
  </div>
    <div>
    {{ Form::label('address', 'Address') }}<br/>
    {{ Form::text('address', Input::old('adress')) }}
  </div>
    <div>
    {{ Form::label('phone', 'Phone') }}<br/>
    {{ Form::text('phone', Input::old('phone')) }}
  </div>
     <div>
    {{ Form::label('administrator', 'Administrator') }}<br/>
    {{ Form::text('administrator', Input::old('administrator')) }}
  </div>

<br/>
  {{ Form::submit('Create the Warehouse!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop