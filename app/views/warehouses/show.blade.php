<!-- app/views/warehouses/show.blade.php -->

@extends('layout')

@section('title')
Show Warehouse
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Warehouse</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Warehouse :{{$warehouse->code}}<br>
        Description :{{$warehouse->description}}<br>
        Address :{{$warehouse->address}}<br>
        Phone :{{$warehouse->phone}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop