<!-- app/views/warehouses/index.blade.php -->

@extends('layout')

@section('title')
Warehouse List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Warehouses</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('warehouses/create') }}">Add Warehouse</a>
    <a class="button white" href="{{ URL::to('reports/warehouses') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('warehouses.search'))) }}
      {{ Form::text('warehouse', null, array( 'placeholder' => 'Search warehouse...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Description</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($warehouses as $key => $value)
            <tr>
                <td>{{ $value->code }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->address }}</td>
                <td>{{ $value->phone }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('warehouses/' . $value->id) }}">Show this Warehouse</a>
                    <a class="button green" href="{{ URL::to('warehouses/' . $value->id . '/edit') }}">Edit this Warehouse</a>
                    {{Form::model($value->id, array('route' => array('warehouses.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $warehouses->links() }}</div>
@stop

@section('footer')
    @parent
@stop