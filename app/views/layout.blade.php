<!-- Stored in app/views/layout.blade.php -->

<!DOCTYPE html>
<html>
    <title>@yield('title')</title>
    {{HTML::style('css/menu.css')}}
    {{HTML::style('css/button.css')}}
    {{HTML::style('css/grid.css')}}
    {{HTML::style('css/alert.css')}}
    {{HTML::style('css/footer.css')}}
    {{HTML::style('css/company.css')}}
    {{HTML::style('css/input.css')}}
    {{HTML::style('css/pagination.css')}}


    {{HTML::style('css/jquery-ui.css')}}

   {{HTML::script('scripts/jquery-1.10.2.js')}} 
   {{HTML::script('scripts/jquery-ui.js')}} 
    <style type="text/css">
        .errMsg
        {
            width: 200px;
            text-align: left;
            color: yellow;
            font: 12px arial;
            background: red;
            padding: 5px;
            display: none;
        }
        
        .tblResult
        {
            border-collapse: collapse;
        }
        
        .tblResult td
        {
            padding: 5px;
            border: 1px solid red;
        }
        
        .tblResult th
        {
            padding: 5px;
            border: 1px solid red;
        }
        
        img
        {
            cursor: pointer;
        }


    </style>


    <body>
        @section('menu')

        <ul class="menu">
          @if(Auth::user())
          <li><a href="#">My dashboard</a></li>

          <li><a href="#">Master</a>
              <ul>
                  <li><a href="{{URL::to('warehouses')}}"><div>W</div>Warehouse</a></li>
                  <li><a href="{{URL::to('vendors')}}"><div>V</div>Vendor</a></li>
                  <li><a href="{{URL::to('projects')}}"><div>P</div>Projects</a></li>
                  <li><a href="{{URL::to('project_docs')}}"><div>PD</div>Project Docs</a></li>
                  <li><a href="{{URL::to('material_categories')}}"><div>MC</div>Material Category</a></li>
                  <li><a href="{{URL::to('materials')}}"><div>M</div>Materials</a></li>
                  <li><a href="{{URL::to('material_docs')}}"><div>MD</div>Material Docs</a></li>
              </ul>   
          </li>
          <li><a href="#">Transaction</a>
              <ul>
                  <li><a href="{{URL::to('material_take_offs')}}"><div>MT</div>Material Take Off</a></li>
                  <li><a href="{{URL::to('purchase_orders')}}"><div>P</div>Purchase Order</a></li>
                  <li><a href="{{URL::to('material_receivings')}}"><div>MR</div>Material Receiving</a></li>
                  <li><a href="{{URL::to('material_issuances')}}"><div>MI</div>Material Issuance</a></li>
                  <li><a href="{{URL::to('material_constructions')}}"><div>MC</div>Material Construction</a></li>
                  <li><a href="{{URL::to('nonconformances')}}"><div>NC</div>Non Conformance</a></li>
              </ul>
          </li>
          <li><a href="#">Reports</a>
                <ul>
                  <li><a href="{{URL::to('reports/warehouses')}}"><div>W</div>Warehouse</a></li>
                  <li><a href="{{URL::to('reports/vendors')}}"><div>V</div>Vendor</a></li>
                  <li><a href="{{URL::to('reports/projects')}}"><div>P</div>Projects</a></li>
                  <li><a href="{{URL::to('project_docs')}}"><div>PD</div>Project Docs</a></li>
                  <li><a href="{{URL::to('reports/material_categories')}}"><div>MC</div>Material Category</a></li>
                  <li><a href="{{URL::to('reports/materials')}}"><div>M</div>Materials</a></li>
                  <li><a href="{{URL::to('material_docs')}}"><div>MD</div>Material Docs</a></li>
                  <li><a href="{{URL::to('reports/material_take_offs')}}"><div>MT</div>Material Take Off</a></li>
                  <li><a href="{{URL::to('reports/purchase_orders')}}"><div>P</div>Purchase Order</a></li>
                  <li><a href="{{URL::to('reports/material_receivings')}}"><div>MR</div>Material Receiving</a></li>
                  <li><a href="{{URL::to('reports/material_issuances')}}"><div>MI</div>Material Issuance</a></li>
                  <li><a href="{{URL::to('reports/material_constructions')}}"><div>MC</div>Material Construction</a></li>
                  <li><a href="{{URL::to('reports/nonconformances')}}"><div>NC</div>Non Conformance</a></li>
              </ul>
          </li>
          <li style="float:right;"><a href="{{ URL::to('logout') }}">Log Out</a></li>
          <li style="float:right;"><a href="#">Welcome {{ucwords(Auth::user()->username)}}</a></li>
          @else
              <li style="float:right;"><a href="{{ URL::to('login') }}">Login</a></li>
          @endif
        </ul>
        @show

        <div style="margin-left:30px;margin-top:30px;margin-right:30px;">
        	@yield('content')
       	</div>
        @section('footer')
        	<div id="footer" style="margin-top:250px;background-color:black;">
			<div style="width:940px;margin:0 auto;text-align:left;">
			<!-- <ul id="company-promos">
			<li><a href="#">{{HTML::image('/img/about.png','About')}}<br><strong style="background:url(http://tapbots.com/img/company_promos/about_text.png);">About Us</strong><br>Learn more about this Inventory and the mad scientists behind these apps.</a></li>
			<li><a href="#">{{HTML::image('/img/blog.png')}}<br><strong style="background: url(http://tapbots.com/img/company_promos/blog_text.png);">From the Blog</strong><br>Stay up to date with the latest appInventory news on our blog.</a></li>
			<li><a href="#">{{HTML::image('/img/help.png')}}<br><strong style="background: url(http://tapbots.com/img/company_promos/help_text.png);">Get Help</strong><br>Having trouble with one of our robots? We are here to help.</a></li>
			<li><a href="#">{{HTML::image('/img/twitter.png')}}<br><strong style="background: url(http://tapbots.com/img/company_promos/twitter_text.png);">Follow appInventory</strong><br>Too cool for RSS? Follow us on Twitter to get the latest.</a></li> </ul> -->
			<p style="font-size:10px;text-transform:uppercase;">©2014 Ericks, LLC. All Rights Reserved. <strong>All Application are Belong to Us.</strong></p>
			</div> 
			</div>
        @show
    </body>
</html>