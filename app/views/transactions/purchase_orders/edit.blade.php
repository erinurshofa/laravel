 app/views/transactions/purchase_orders/edit.blade.php -->

@extends('layout')

@section('title')
Edit Purchase Order
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Purchase Order</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($purchase_order, array('route' => array('purchase_orders.update', $purchase_order->id), 'method' => 'PUT')) }}
{{Form::hidden('purchaseOrderId', $purchase_order->id,array('id'=>'purchase_order_id')) }} 
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_take_off_id', 'Project') }}<br/><br/>
        {{ Form::select('material_take_off_id',array('placeholder'=>'Choose Material Take Off')+$populate_material_take_off),null }}
    </div><br/>
    <div>
        {{ Form::label('vendor_id', 'Vendor') }}<br/><br/>
        {{ Form::select('vendor_id',array('placeholder'=>'Choose Vendor')+$populate_vendor),null }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('po_no', 'PO No') }}<br/><br/>
        {{ Form::text('po_no', null) }}
    </div><br/>
    <div>
        {{ Form::label('po_revision', 'PO Revision') }}<br/><br/>
        {{ Form::text('po_revision', Input::old('po_revision')) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('po_date', 'PO Date') }}<br/><br/>
        {{ Form::text('po_date', null) }}
    </div><br/>
    <div>
        {{ Form::label('po_estimate_date', 'PO Estimate Date') }}<br/><br/>
        {{ Form::text('po_estimate_date', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('po_promised_date', 'PO Promised Date') }}<br/><br/>
        {{ Form::text('po_promised_date', null) }}
    </div><br/>
    <div>
        {{ Form::label('po_description', 'PO Description') }}<br/><br/>
        {{ Form::text('po_description', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('po_status', 'PO Status') }}<br/><br/>
        {{ Form::text('po_status', null) }}
    </div><br/>
</div>
 <div style="clear:both;"></div><br>

 <input type="text" id="material_code" placeholder="enter barcode"\><br><br>
 <table id="material" class="bordered"  onload='loadPurchaseOrderDetail()'>
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Remarks</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
<!--        <th></th>
            <th style="border-left:none;"></th>
            <th style="border-left:none;"></th>
            <th><b>Total Quantity     :</b></th>
            <th><span id="totalQuantity"></span></th>
            <th></th>
            <th></th> -->
        </tfoot>
    </table>
<br/>
<br/>
<!-- {{ Form::submit('Edit the Purchase Order!', array('class' => 'button orange')) }} -->
<input type="button" class="button orange" id="edit" value="Edit the Purchase Order!" \>
<input type="hidden" id="purchase_order_detail_id"\>
{{ Form::close() }}

<script type="text/javascript">
var purchaseOrderId = $("#purchase_order_id").val();
// var materialTakeOffId = $("#material_take_off_id").val();
var prices=[];
var quantities=[];
var remarks=[];
var totalPrice=0;
var totalQuantity = 0;
var totalRow =1;
var materials = [];
var isExists = false;
$( "#po_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#po_estimate_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#po_promised_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

function deleteRow(index)
{
 if (totalRow==1) {
    document.getElementById('material').deleteRow(index);
    remarks.splice(index-1,1); 
    materials.splice(index-1,1);
  }else{
    document.getElementById('material').deleteRow(index+1);
    remarks.splice(index,1); 
    materials.splice(index,1);
  }
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[index];
    totalPrice=totalPrice-prices[index];
    quantities.splice(index-1,1); 
    prices.splice(index-1,1); 
    // console.log(materials[0],"this is material in delete row 1");
    changeQuantity();
    changePrice();
    if(totalRow==0){
        // console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 
  // var index = $(this).closest("tr").index();
  if (index<1) {
    console.log("kesini kah?");
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}
function getPrice(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}
function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
    // totalQuantity=0;
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}
function changePrice()
{
  totalPrice =0;
  for (var i = totalRow; i >= 1; i--) {
    totalPrice += getPrice(parseInt($('#price'+i).val()));
    prices[i-1]=getPrice(parseInt($('#price'+i).val()));
    // console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalPrice))
  {
  }else{
    document.getElementById('totalPrice').innerHTML = "Rp. "+totalPrice.toFixed(2);
  }
}
$("#material_code").autocomplete({
    source:'edit/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'http://localhost:81/apptrackingsystem/public/purchase_orders/findMaterialTakeOffByCode/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          console.log(result,'result when code is change in textbox');
          //check if exists
          for(var i=0;i<materials.length;i++) {
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                changeQuantity();
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {
              }else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="price'+totalRow+'" style="margin-left:20px;" onchange="changePrice()"/></td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+
                               '<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        // alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#price'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Price     :</b><span id="totalPrice"></span></th>'+
                                        '<th><b>Total Quantity     :</b><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}

$("#edit").click(function(){
    var materialTakeOffId = $("#material_take_off_id").val();
    var vendorId = $("#vendor_id").val();
    var poNo = $("#po_no").val();
    var poRevision= $("#po_revision").val();
    var poDate= $("#po_date").val();
    var poEstimateDate=$("#po_estimate_date").val();
    var poPromisedDate=$("#po_promised_date").val();
    var poDescription=$("#po_description").val();
    var poStatus=$("#po_status").val();

    if (totalRow<1) {
      alert("Material is required! Please enter barcode!");
      $("#material_code").focus();
      return;
    };
    if (!materialTakeOffId) {alert('Material Take Off is required');$("#material_take_off_id").focus();return;};
    if (!vendorId) {alert('Vendor is required');$("#vendor_id").focus();return;};
    if (!poNo) {alert('Purchase Order No is required');$("#po_no").focus();return;};
    if (!poRevision) {alert('Purchase Order Revision is required');$("#po_revision").focus();return;};
    if (!poDate) {alert('Purchase Order Date is required');$("#po_date").focus();return;};
    if (!poEstimateDate) {alert('Purchase Order Estimate Date By is required');$("#po_estimate_date").focus();return;};
    if (!poPromisedDate) {alert('Purchase Order Promised Date is required');$("#po_promised_date").focus();return;};
    if (!poDescription) {alert('Purchase Order Description is required');$("#po_description").focus();return;};
    if (!poStatus) {alert('Purchase Order Status is required');$("#po_status").focus();return;};
    $.ajax({
          type: "get",
          url: 'edit/editWithAjax',
          data:{"materialTakeOffId":materialTakeOffId,"vendorId":vendorId,"poNo":poNo,"poRevision":poRevision,"poDate":poDate,"poEstimateDate":poEstimateDate,"poPromisedDate":poPromisedDate,"poDescription":poDescription,"poStatus":poStatus,"totalRow":totalRow,"materials":materials,"prices":prices,"quantities":quantities,"remarks":remarks},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            console.log(jqXHR.status,"this is jqXHR");
            console.log(status,"status");
            console.log(thrownError,"thrownError");
            alert(thrownError);
            },
            success: function (result, test) {
              console.log(result,"this is result");
              window.location.replace("http://localhost:81/apptrackingsystem/public/purchase_orders");
          },
    });
});


$(document).ready(function() {
    //get list material
    $.ajax({
        type:"get",
        url:'http://localhost:81/apptrackingsystem/public/purchase_orders/getListMaterial/'+purchaseOrderId,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('error');
        },
        success: function (result1, test) {
            totalRow = 0;
            totalQuantity=0;
          ///get material detail
          $.ajax({
              type:"get",
              url:'http://localhost:81/apptrackingsystem/public/purchase_orders/getListPurchaseOrderDetail/'+purchaseOrderId,
              dataType:'json',
              contentType: "application/json; charset=utf-8",
              error:function (jqXHR, status, thrownError) {
                  alert('material code not exist');
              },
              success: function (result2, test) {
                  for (var i = 0; i < result1.length; i++){
                    totalRow+=1;
                    materials.push(result1[i]);
                    quantities.push(result2[i].quantity);
                    remarks.push(result2[i].remarks);
                    totalQuantity+=result2[i].quantity;
                            $('#material > tbody:last').append('<tr>'+
                                '<td>'+materials[i].code+'</td>'+
                                '<td>'+materials[i].name+'</td>'+
                                '<td>'+materials[i].description+'</td>'+
                                '<td>'+materials[i].unit+'</td>'+
                                '<td><input type="number" id="price'+totalRow+'" value="'+result2[i].price+'" style="margin-left:20px;" onchange="changePrice()"/></td>'+ 
                                '<td><input type="number" id="quantity'+totalRow+'" value="'+result2[i].quantity+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+ 
                                '<td><input type="text" id="remarks'+totalRow+'" value="'+result2[i].remarks+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                                '</tr>');
                    };
                      //buat tfoot
                      // if(totalRow ==1) {
                          var foot = $("#material").find('tfoot');
                            if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                                foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Price     :</b><span id="totalPrice"></span></th>'+
                                        '<th><b>Total Quantity  :</b><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                        ));
                      // };
                      changePrice();
                      document.getElementById('totalPrice').innerHTML = totalPrice;
                      document.getElementById('totalQuantity').innerHTML = totalQuantity;
                      document.getElementById('totalRow').value=totalRow;
                }
          });
          console.log("lewat ajax");  
        }
    });
});
</script>
@stop
@section('footer')
    @parent
@stop
