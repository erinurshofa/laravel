<!-- app/views/transactions/purchase_orders/index.blade.php -->

@extends('layout')

@section('title')
Transaction Purchase Order List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Purchase Orders</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('purchase_orders/create') }}">Add Purchase Order</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('purchase_orders.search'))) }}
      {{ Form::text('purchase_order', null, array( 'placeholder' => 'Search purchase order...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Material Take Off</th>
                <th>Vendor</th>
                <th>PO No</th>
                <th>PO Revision</th>
                <th>PO Date</th>
                <th>PO Estimate Date</th>
                <th>PO Promised Date</th>
                <th>PO Description</th>
                <th>PO Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($purchase_orders as $key => $value)
            <tr>
                <td>{{MaterialTakeOff::where('id','=',$value->material_take_off_id)->lists('mto_description','id')[$value->material_take_off_id]}}</td>
                <td>{{Vendor::where('id','=',$value->vendor_id)->lists('name','id')[$value->vendor_id]}}</td>
                <td>{{ $value->po_no }}</td>
                <td>{{ $value->po_revision }}</td>
                <td>{{ $value->po_date }}</td>
                <td>{{ $value->po_estimate_date }}</td>
                <td>{{ $value->po_promised_date }}</td>
                <td>{{ $value->po_description }}</td>
                <td>{{ $value->po_status }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('purchase_orders/' . $value->id) }}">Show this Purchase Order</a>
                    <a class="button green" href="{{ URL::to('purchase_orders/' . $value->id . '/edit') }}">Edit this Purchase Order</a>
                    {{Form::model($value->id, array('route' => array('purchase_orders.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $purchase_orders->links() }}</div>
@stop

@section('footer')
    @parent
@stop