<!-- app/views/transactions/purchase_orders/create.blade.php -->
@extends('layout')

@section('title')
Create Purchase Order
@stop

@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Transaction Purchase Order</h1>
<br/>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
<!-- if there are creation errors, they will show here -->
{{HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;'))}}
{{Form::open(array('url' => 'purchase_orders'))}}
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;">
      <div>
          {{ Form::label('material_take_off_id', 'Material Take Off') }}<br/><br/>
          {{ Form::select('material_take_off_id',array('placeholder'=>'Choose Material Take Off')+$populate_material_take_off),null }}
      </div><br/>
      <div>
          {{ Form::label('vendor_id', 'Vendor') }}<br/><br/>
          {{ Form::select('vendor_id',array('placeholder'=>'Choose Vendor')+$populate_vendor),null }}
      </div><br/>
      <div>
          {{ Form::label('po_no', 'PO No') }}<br/><br/>
          {{ Form::text('po_no', Input::old('po_no')) }}
      </div><br/>
      <div>
          {{Form::label('po_revision', 'PO Revision') }}<br/><br/>
          {{Form::input('text', 'po_revision', Input::old('po_revision'))}}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('po_date', 'PO Date') }}<br/><br/>
          {{ Form::text('po_date', Input::old('po_date')) }}
      </div><br/>
      <div>
          {{ Form::label('po_estimate_date', 'PO Estimate Date') }}<br/><br/>
          {{ Form::text('po_estimate_date', Input::old('po_estimate_date')) }}
      </div><br/>
      <div>
          {{ Form::label('po_promised_date', 'PO Promised Date') }}<br/><br/>
          {{Form::input('text', 'po_promised_date', Input::old('po_promised_date'))}}
      </div><br/>
      <div>
          {{ Form::label('po_description', 'PO Description') }}<br/><br/>
          {{ Form::text('po_description', Input::old('po_description')) }}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('po_status', 'PO Status') }}<br/><br/>
          {{Form::input('text', 'po_status', Input::old('po_status'))}}
      </div>
</div>
<div style="clear:both;"></div><br>
<input type="text" id="material_code" placeholder="enter barcode"\><br><br>
<table id="material" class="bordered">
    <thead>
      <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Unit</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Remarks</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr> 
      </tr>
    </tbody>
    <tfoot>
      <tr>
      </tr>
    </tfoot>
</table>

<br/>
<input type="button" class="button orange" id="create" value="Create the Purchase Order!" \>
<script type="text/javascript">
var prices=[];
var quantities=[];
var remarks=[];
var totalQuantity = 0;
var totalPrice=0;
var totalRow =0;
var materials = [];
var isExists = false;
$( "#po_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#po_estimate_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#po_promised_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

function deleteRow(i)
{
    // if (totalRow==1) {
    document.getElementById('material').deleteRow(i+1);
    // if (totalRow == i) {
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[i];
    totalPrice=totalPrice-prices[i];
    
    quantities.splice(i-1,1); 
    prices.splice(i-1,1); 
    
    remarks.splice(i-1,1); 
    
    materials.splice(i-1,1);
    console.log(materials,"this is material in delete row 1");
    changeQuantity();
    changePrice();

    if(totalRow==0){
        console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 

  if (index<1) {
    return 1;
  }else{
    return index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}
function getPrice(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}

function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    // console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    // console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}

function changePrice()
{
  totalPrice =0;
  for (var i = totalRow; i >= 1; i--) {
    totalPrice += getPrice(parseInt($('#price'+i).val()));
    prices[i-1]=getPrice(parseInt($('#price'+i).val()));
    // console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalPrice))
  {
  }else{
    document.getElementById('totalPrice').innerHTML = "Rp. "+totalPrice.toFixed(2);
  }
}
$("#create").click(function(){
    var materialTakeOffId = $("#material_take_off_id").val();
    var vendorId = $("#vendor_id").val();
    var poNo = $("#po_no").val();
    var poRevision= $("#po_revision").val();
    var poDate= $("#po_date").val();
    var poEstimateDate=$("#po_estimate_date").val();
    var poPromisedDate=$("#po_promised_date").val();
    var poDescription=$("#po_description").val();
    var poStatus=$("#po_status").val();
    if (!materialTakeOffId) {alert('Material Take Off is required');$("#material_take_off_id").focus();return;};
    if (materialTakeOffId=='placeholder') {alert('Material Take Off is required');$("#material_take_off_id").focus();return;};
    if (!vendorId) {alert('Vendor is required');$("#vendor_id").focus();return;};
    if (vendorId=='placeholder') {alert('Vendor is required');$("#vendor_id").focus();return;};
    if (!poNo) {alert('Purchase Order No is required');$("#po_no").focus();return;};
    if (!poRevision) {alert('Purchase Order Revision is required');$("#po_revision").focus();return;};
    if (!poDate) {alert('Purchase Order Date is required');$("#po_date").focus();return;};
    if (!poEstimateDate) {alert('Purchase Order Estimate Date is required');$("#po_estimate_date").focus();return;};
    if (!poPromisedDate) {alert('Purchase Order Promised Date is required');$("#po_promised_date").focus();return;};
    if (!poDescription) {alert('Purchase Order Description is required');$("#po_description").focus();return;};
    if (!poStatus) {alert('Purchase Order Status is required');$("#po_Status").focus();return;};

    if (materials.length==0) {
       alert('Material is required, Please enter barcode!');$("#material_code").focus();return;
    };
    $.ajax({
          type: "get",
          url: 'create/createWithAjax',
          data:{"materialTakeOffId":materialTakeOffId,"vendorId":vendorId,"poNo":poNo,"poRevision":poRevision,"poDate":poDate,"poEstimateDate":poEstimateDate,"poPromisedDate":poPromisedDate,"poDescription":poDescription,"poStatus":poStatus,"totalRow":totalRow,"materials":materials,"prices":prices,"quantities":quantities,"remarks":remarks},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            // console.log(jqXHR.status,"this is jqXHR");
            // console.log(status,"status");
            // console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result) {
              alert(result);
              window.location.replace("http://localhost:81/apptrackingsystem/public/purchase_orders");
          },
    });
});
$("#material_code").autocomplete({
    source:'create/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'create/find/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          //check if exists
          for(var i=0;i<materials.length;i++) {
            //jika sudah ada pada grid maka hanya menambah quantitynya
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {}else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="price'+totalRow+'" style="margin-left:20px;" onchange="changePrice()"/></td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#price'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Price     :</b><span id="totalPrice"></span></th>'+
                                        '<th><b>Total Quantity     :</b><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}
</script>

{{ Form::close() }}

@stop
@section('footer')
    @parent
@stop


