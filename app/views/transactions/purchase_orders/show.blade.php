<!-- app/views/transactions/purchase_orders/show.blade.php -->

@extends('layout')

@section('title')
Show Purchase Order
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Purchase Order</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        <!-- Material Take Off :{{$purchase_order->material_take_off_id}}<br> -->
        PO No :{{$purchase_order->po_no}}<br>
        PO Revision :{{$purchase_order->po_revision}}<br>
        PO Date :{{$purchase_order->po_date}}<br>
        PO Estimate Date :{{$purchase_order->po_estimate_date}}<br>
        PO Promised Date :{{$purchase_order->po_promised_date}}<br>
        PO Description:{{$purchase_order->po_description}}<br>
        PO Status :{{$purchase_order->po_status}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop