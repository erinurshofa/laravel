<!-- app/views/transactions/material_take_offs/create.blade.php -->
@extends('layout')

@section('title')
Create Material Take Off
@stop

@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Transaction Material Take Off</h1>
<br/>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}
{{ Form::open(array('url' => 'material_take_offs')) }}
<!-- {{Form::hidden('material_id[]',Input::old('material_id'),array('id'=>'material_id'))}} -->
<!-- {{ Form::text('material[]', Input::old('material.id'),array('id'=>'material_id')) }} -->
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;">
      <div>
          {{ Form::label('project_id', 'Project') }}<br/><br/>
          {{ Form::select('project_id',array('placeholder'=>'Choose Project')+$populate_project),Input::old('project_id') }}
      </div><br/>
      <div>
          {{ Form::label('mto_no', 'No') }}<br/><br/>
          {{ Form::text('mto_no', Input::old('mto_no')) }}
      </div><br/>
      <div>
          {{ Form::label('mto_revision', 'Revision') }}<br/><br/>
          {{ Form::text('mto_revision', Input::old('mto_revision')) }}
      </div><br/>
      <div>
          {{Form::label('mto_date', 'Date') }}<br/><br/>
          {{Form::input('text', 'mto_date', Input::old('mto_date'))}}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('mto_description', 'Description') }}<br/><br/>
          {{ Form::text('mto_description', Input::old('mto_description')) }}
      </div><br/>
      <div>
          {{ Form::label('mto_request_by', 'Request By') }}<br/><br/>
          {{ Form::text('mto_request_by', Input::old('mto_request_by')) }}
      </div><br/>
      <div>
          {{ Form::label('mto_request_date', 'Request Date') }}<br/><br/>
          {{Form::input('text', 'mto_request_date', Input::old('mto_request_date'))}}
      </div><br/>
      <div>
          {{ Form::label('mto_approved_by', 'Approved By') }}<br/><br/>
          {{ Form::text('mto_approved_by', Input::old('mto_approved_by')) }}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('mto_approved_date', 'Approved Date') }}<br/><br/>
          {{Form::input('text', 'mto_approved_date', Input::old('mto_approved_date'))}}
      </div>
</div>
<div style="clear:both;"></div><br>
<input type="text" id="material_code" placeholder="enter barcode"\><br><br>
<table id="material" class="bordered">
    <thead>
      <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Unit</th>
        <th>Quantity</th>
        <th>Remarks</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr> 
      </tr>
    </tbody>
    <tfoot>
      <tr>
   <!--      <th></th>
        <th style="border-left:none;"></th>
        <th style="border-left:none;"></th>
        <th><b>Total Quantity     :</b></th>
        <th><span id="totalQuantity"></span></th>
        <th></th> -->
      </tr>
    </tfoot>
</table>

<br/>
<input type="button" class="button orange" id="create" value="Create the Material Take Off!" \>
<script type="text/javascript">

var quantities=[];
var remarks=[];
var totalQuantity = 0;
var totalRow =0;
var materials = [];
var isExists = false;
$( "#mto_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#mto_request_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#mto_approved_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

function deleteRow(i)
{
    // if (totalRow==1) {
    document.getElementById('material').deleteRow(i+1);
    // if (totalRow == i) {
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[i];
    // quantities.removeByIndex(i);
    quantities.splice(i-1,1); 
    // remarks.removeByIndex(i);
    remarks.splice(i-1,1); 
    // materials.removeByIndex(i-1);
    materials.splice(i-1,1);
    console.log(materials,"this is material in delete row 1");
    changeQuantity();

    if(totalRow==0){
        console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 
  // var index = $(this).closest("tr").index();
  if (index<1) {
    // console.log("kesini kah?");
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}

function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
    // totalQuantity=0;
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}

$("#create").click(function(){
    var projectId = $("#project_id").val();
    var mtoNo = $("#mto_no").val();
    var mtoRevision = $("#mto_revision").val();
    var mtoDate= $("#mto_date").val();
    var mtoDescription= $("#mto_description").val();
    var mtoRequestBy=$("#mto_request_by").val();
    var mtoRequestDate=$("#mto_request_date").val();
    var mtoApprovedBy=$("#mto_approved_by").val();
    var mtoApprovedDate=$("#mto_approved_date").val();
    console.log(mtoApprovedDate,"approved date");
    // console.log(new Date(parseInt(mtoRequestDate.substr(6))),"request date");
    console.log(mtoDate,"date");
    if (!projectId) {alert('Project is required');$("#project_id").focus();return;};
    if (projectId=='placeholder'){alert('Project is required');$("#project_id").focus();return;};
    if (!mtoNo) {alert('Material Take Off No is required');$("#mto_no").focus();return;};
    if (!mtoRevision) {alert('Material Take Off Revision is required');$("#mto_revision").focus();return;};
    if (!mtoDate) {alert('Material Take Off Date is required');$("#mto_date").focus();return;};
    if (!mtoDescription) {alert('Material Take Off Description is required');$("#mto_description").focus();return;};
    if (!mtoRequestBy) {alert('Material Take Off Request By is required');$("#mto_request_by").focus();return;};
    if (!mtoRequestDate) {alert('Material Take Off Request Date is required');$("#mto_request_date").focus();return;};
    
    
    if (materials.length==0) {
      alert('Material is required, Please enter barcode!');$("#material_code").focus();return;
    };
    $.ajax({
          type: "get",
          url: 'create/createWithAjax',
          data:{"projectId":projectId,"mtoNo":mtoNo,"mtoRevision":mtoRevision,"mtoDate":mtoDate,"mtoDescription":mtoDescription,"mtoRequestBy":mtoRequestBy,"mtoRequestDate":mtoRequestDate,"mtoApprovedBy":mtoApprovedBy,"mtoApprovedDate":mtoApprovedDate,"totalRow":totalRow,"materials":materials,"quantities":quantities,"remarks":remarks},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            console.log(jqXHR.status,"this is jqXHR");
            console.log(status,"status");
            console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result, test) {
              console.log(result,"this is result");
              window.location.replace("http://localhost:81/apptrackingsystem/public/material_take_offs");
          },
    });
});
$("#material_code").autocomplete({
    source:'create/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'create/find/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          //check if exists
          for(var i=0;i<materials.length;i++) {
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {}else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#quantity'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b></th>'+
                                        '<th><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}
</script>
<!--{{ Form::submit('Create the Material Take Off!', array('class' => 'button orange')) }}-->

{{ Form::close() }}

@stop
@section('footer')
    @parent
@stop


