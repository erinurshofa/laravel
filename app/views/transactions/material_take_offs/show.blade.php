<!-- app/views/transactions/material_take_offs/show.blade.php -->

@extends('layout')

@section('title')
Show Material Take Off
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Take Off</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        <!-- Project ID :{{$material_take_off->project_id}}<br> -->
        No :{{$material_take_off->mto_no}}<br>
        Revision :{{$material_take_off->mto_revision}}<br>
        Date :{{$material_take_off->mto_date}}<br>
        Description :{{$material_take_off->mto_description}}<br>
        Request by :{{$material_take_off->mto_request_by}}<br>
        Request date:{{$material_take_off->mto_request_date}}<br>
        Approved by :{{$material_take_off->mto_approved_by}}<br>
        Approved date:{{$material_take_off->mto_approved_date}}<br>

    </div>
@stop

@section('footer')
    @parent
@stop