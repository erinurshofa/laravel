<!-- app/views/transactions/material_take_offs/index.blade.php -->

@extends('layout')

@section('title')
Transaction Material Take Off List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material Take Off</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('material_take_offs/create') }}">Add Material Take Off</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('material_take_offs.search'))) }}
      {{ Form::text('material_take_off', null, array( 'placeholder' => 'Search material take off...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Project</th>
                <th>No</th>
                <th>Revision</th>
                <th>Date</th>
                <th>Description</th>
                <th>Request By</th>
                <th>Request Date</th>
                <th>Approved By</th>
                <th>Approved Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_take_offs as $key => $value)
            <tr>
                <td>{{Project::where('id','=',$value->project_id)->lists('name','id')[$value->project_id]}}</td>
                <td>{{ $value->mto_no }}</td>
                <td>{{ $value->mto_revision }}</td>
                <td>{{ $value->mto_date }}</td>
                <td>{{ $value->mto_description }}</td>
                <td>{{ $value->mto_request_by }}</td>
                <td>{{ $value->mto_request_date }}</td>
                <td>{{ $value->mto_approved_by }}</td>
                <td>{{ $value->mto_approved_date }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('material_take_offs/' . $value->id) }}">Show this Material Take Off</a>
                    <a class="button green" href="{{ URL::to('material_take_offs/' . $value->id . '/edit') }}">Edit this Material Take Off</a>
                    {{Form::model($value->id, array('route' => array('material_take_offs.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $material_take_offs->links() }}</div>
@stop

@section('footer')
    @parent
@stop