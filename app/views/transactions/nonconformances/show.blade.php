<!-- app/views/transactions/nonconformances/show.blade.php -->

@extends('layout')

@section('title')
Show Nonconformance
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Nonconformance</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Purchase Order :{{$nonconformance->purchase_order_id}}<br>
        Code :{{$nonconformance->nonconformance_code}}<br>
        Date :{{$nonconformance->nonconformance_date}}<br>
        Description :{{$nonconformance->nonconformance_description}}<br>

    </div>
@stop

@section('footer')
    @parent
@stop