 app/views/transactions/nonconformances/edit.blade.php -->

@extends('layout')

@section('title')
Edit Nonconformance
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Nonconformance</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($nonconformance, array('route' => array('nonconformances.update', $nonconformance->id), 'method' => 'PUT')) }}
{{Form::hidden('nonconformanceId', $nonconformance->id,array('id'=>'nonconformance_id')) }} 
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('purchase_order_id', 'Purchase Order') }}<br/><br/>
        {{ Form::select('purchase_order_id',array('placeholder'=>'Choose Purchase Order')+$populate_purchase_order),null }}
    </div><br/>
    <div>
        {{ Form::label('nonconformance_code', 'Code') }}<br/><br/>
        {{ Form::text('nonconformance_code', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('nonconformance_date', 'Date') }}<br/><br/>
        {{ Form::text('nonconformance_date', null) }}
    </div><br/>
    <div>
        {{ Form::label('nonconformance_description', 'Description') }}<br/><br/>
        {{ Form::text('nonconformance_description', Input::old('nonconformance_description')) }}
    </div><br/>
</div>

 <div style="clear:both;"></div><br>

 <input type="text" id="material_code" placeholder="enter barcode"\><br><br>
 <table id="material" class="bordered"  onload='loadNonconformanceDetail()'>
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Remarks</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
<!--        <th></th>
            <th style="border-left:none;"></th>
            <th style="border-left:none;"></th>
            <th><b>Total Quantity     :</b></th>
            <th><span id="totalQuantity"></span></th>
            <th></th>
            <th></th> -->
        </tfoot>
    </table>
<br/>
<br/>

<!-- {{ Form::submit('Edit the Nonconformance!', array('class' => 'button orange')) }} -->
<input type="button" class="button orange" id="edit" value="Edit the Nonconformance!" \>
<input type="hidden" id="nonconformance_detail_id"\>
{{ Form::close() }}

<script type="text/javascript">
var nonconformanceId = $("#nonconformance_id").val();



var quantities=[];
var remarks=[];
var totalQuantity = 0;
var totalRow =1;
var materials = [];
var isExists = false;
$( "#nonconformance_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

// function getTotalRow(){
//     return $("#totalRow").val();
// }

// function setTotalRow(value){
//     document.getElementById('totalRow').value = value;
// }

function deleteRow(index)
{
 if (totalRow==1) {
    document.getElementById('material').deleteRow(index);
    remarks.splice(index-1,1); 
    materials.splice(index-1,1);
  }else{
    document.getElementById('material').deleteRow(index+1);
    remarks.splice(index,1); 
    materials.splice(index,1);
  }
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[index];
    quantities.splice(index-1,1); 
    // console.log(materials[0],"this is material in delete row 1");
    changeQuantity();

    if(totalRow==0){
        // console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 

  // var index = $(this).closest("tr").index();
  if (index<1) {
    console.log("kesini kah?");
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}

function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
    // totalQuantity=0;
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}

$("#material_code").autocomplete({
    source:'edit/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'http://localhost:81/apptrackingsystem/public/nonconformances/findNonconformanceByCode/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          console.log(result,'result when code is change in textbox');
          //check if exists
          for(var i=0;i<materials.length;i++) {
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                changeQuantity();
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {
              }else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        // alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#quantity'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b></th>'+
                                        '<th><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}

$("#edit").click(function(){
    var purchaseOrderId = $("#purchase_order_id").val();
    var nonconformanceCode = $("#nonconformance_code").val();
    var nonconformanceDate = $("#nonconformance_date").val();
    var nonconformanceDescription= $("#nonconformance_description").val();

    if (totalRow<1) {
      alert("Material is required! Please enter barcode!");
      $("#material_code").focus();
      return;
    };

    if (!purchaseOrderId) {alert('Purchase order is required');$("#purchase_order_id").focus();return;};
    if (!nonconformanceCode) {alert('Nonconformance code is required');$("#nonconformance_code").focus();return;};
    if (!nonconformanceDate) {alert('Nonconformance date is required');$("#nonconformance_date").focus();return;};
    if (!nonconformanceDescription) {alert('Nonconformance description is required');$("#nonconformance_description").focus();return;};

    $.ajax({
          type: "get",
          url: 'edit/editWithAjax',
          data:{"purchaseOrderId":purchaseOrderId,"nonconformanceCode":nonconformanceCode,"nonconformanceDate":nonconformanceDate,"nonconformanceDescription":nonconformanceDescription,"totalRow":totalRow,"materials":materials,"quantities":quantities,"remarks":remarks},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            console.log(jqXHR.status,"this is jqXHR");
            console.log(status,"status");
            console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result, test) {
              console.log(result,"this is result");
              window.location.replace("http://localhost:81/apptrackingsystem/public/nonconformances");
          },
    });
});


$(document).ready(function() {
    //get list material
    $.ajax({
        type:"get",
        url:'http://localhost:81/apptrackingsystem/public/nonconformances/getListMaterial/'+nonconformanceId,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('error');
        },
        success: function (result1, test) {
            totalRow = 0;
            totalQuantity=0;
          ///get material detail
          $.ajax({
              type:"get",
              url:'http://localhost:81/apptrackingsystem/public/nonconformances/getListNonconformanceDetail/'+nonconformanceId,
              dataType:'json',
              contentType: "application/json; charset=utf-8",
              error:function (jqXHR, status, thrownError) {
                  alert('material code not exist');
              },
              success: function (result2, test) {
                  // console.log(result1,"this is result1");
                  // console.log(result2,"this is result2");
                  
                  for (var i = 0; i < result1.length; i++) {
                    totalRow+=1;
                    materials.push(result1[i]);
                    quantities.push(result2[i].quantity);
                    remarks.push(result2[i].remarks);
                    totalQuantity+=result2[i].quantity;
                            $('#material > tbody:last').append('<tr>'+
                                '<td>'+materials[i].code+'</td>'+
                                '<td>'+materials[i].name+'</td>'+
                                '<td>'+materials[i].description+'</td>'+
                                '<td>'+materials[i].unit+'</td>'+
                                '<td><input type="number" id="quantity'+totalRow+'" value="'+result2[i].quantity+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+ 
                                '<td><input type="text" id="remarks'+totalRow+'" value="'+result2[i].remarks+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                                '</tr>');
                    };

                      //buat tfoot
                      // if(totalRow ==1) {
                          var foot = $("#material").find('tfoot');
                            if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                                foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b></th>'+
                                        '<th><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                        ));
                      // };
                      // document.getElementById('nonconformance_detail_id').value = result2[0].id;
                      // document.getElementById('nonconformance_id').value = result2[0].nonconformance_id;
                      document.getElementById('totalQuantity').innerHTML = totalQuantity;
                      document.getElementById('totalRow').value=totalRow;
                }
          });
          console.log("lewat ajax");  
        }
    });
});

</script>

@stop

@section('footer')
    @parent
@stop
