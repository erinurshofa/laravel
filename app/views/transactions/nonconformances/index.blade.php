<!-- app/views/transactions/nonconformances/index.blade.php -->

@extends('layout')

@section('title')
Transaction Nonconformance List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Nonconformance</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('nonconformances/create') }}">Add Nonconformance</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('nonconformances.search'))) }}
      {{ Form::text('nonconformance', null, array( 'placeholder' => 'Search nonconformance...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Purchase Order</th>
                <th>Code</th>
                <th>Date</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($nonconformances as $key => $value)
            <tr>
                <td>{{PurchaseOrder::where('id','=',$value->purchase_order_id)->lists('po_no','id')[$value->purchase_order_id]}}</td>
                <td>{{ $value->nonconformance_code }}</td>
                <td>{{ $value->nonconformance_date }}</td>
                <td>{{ $value->nonconformance_description }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('nonconformances/' . $value->id) }}">Show this Nonconformance</a>
                    <a class="button green" href="{{ URL::to('nonconformances/' . $value->id . '/edit') }}">Edit this Nonconformance</a>
                    {{Form::model($value->id, array('route' => array('nonconformances.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $nonconformances->links() }}</div>
@stop

@section('footer')
    @parent
@stop