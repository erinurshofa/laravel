<!-- app/views/transactions/material_issuance/index.blade.php -->

@extends('layout')

@section('title')
Transaction Material Issuance List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material Issuances</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('material_issuances/create') }}">Add Material Issuance</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('material_issuances.search'))) }}
      {{ Form::text('material_issuance', null, array( 'placeholder' => 'Search material issuance...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Material Take Off</th>
                <th>Warehouse</th>
                <th>Material Issuance No</th>
                <th>Material Issuance Date</th>
                <th>Material Issuance Request By</th>
                <th>Material Issuance Request Date</th>
                <th>Material Issuance Approved By</th>
                <th>Material Issuance Approved Date</th>
                <th>Material Issuance Received By</th>
                <th>Material Issuance Received Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_issuances as $key => $value)
            <tr>
                <td>{{MaterialTakeOff::where('id','=',$value->material_take_off_id)->lists('mto_no','id')[$value->material_take_off_id]}}</td>
                <td>{{Warehouse::where('id','=',$value->warehouse_id)->lists('description','id')[$value->warehouse_id]}}</td>
                <td>{{$value->material_issuance_no}}</td>
                <td>{{$value->material_issuance_date}}</td>
                <td>{{$value->material_issuance_request_by}}</td>
                <td>{{$value->material_issuance_request_date}}</td>
                <td>{{$value->material_issuance_approved_by}}</td>
                <td>{{$value->material_issuance_approved_date}}</td>
                <td>{{$value->material_issuance_received_by}}</td>
                <td>{{$value->material_issuance_received_date}}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('material_issuances/' . $value->id) }}">Show this Material Issuance</a>
                    <a class="button green" href="{{ URL::to('material_issuances/' . $value->id . '/edit') }}">Edit this Material Issuance</a>
                    {{Form::model($value->id, array('route' => array('material_issuances.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $material_issuances->links() }}</div>
@stop

@section('footer')
    @parent
@stop