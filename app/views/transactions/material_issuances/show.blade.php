<!-- app/views/transactions/material_issuances/show.blade.php -->

@extends('layout')

@section('title')
Show Material Issuance
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Issuance</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Material Take Off :{{$material_issuance->material_take_off_id}}<br>
        Warehouse :{{$material_issuance->warehouse_id}}<br>
        Material Issuance No :{{$material_issuance->material_issuance_no}}<br>
        Material Issuance Date :{{$material_issuance->material_issuance_date}}<br>
        Material Issuance Request By :{{$material_issuance->material_issuance_request_by}}<br>
        Material Issuance Request Date :{{$material_issuance->material_issuance_request_date}}<br>
        Material Issuance Received By :{{$material_issuance->material_issuance_received_by}}<br>
        Material Issuance Received Date:{{$material_issuance->material_issuance_received_date}}<br>
        Material Issuance Approved By :{{$material_issuance->material_issuance_approved_by}}<br>
        Material Issuance Approved Date:{{$material_issuance->material_issuance_approved_date}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop