<!-- app/views/transactions/material_issuances/create.blade.php -->
@extends('layout')

@section('title')
Create Material Issuance
@stop

@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Transaction Material Issuance</h1>
<br/>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
<!-- if there are creation errors, they will show here -->
{{HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;'))}}
{{Form::open(array('url' => 'material_issuances'))}}
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;">
      <div>
          {{ Form::label('material_take_off_id', 'Material Take Off') }}<br/><br/>
          {{ Form::select('material_take_off_id',array('placeholder'=>'Choose Material Take Off')+$populate_material_take_off),null }}
      </div><br/>
      <div>
          {{ Form::label('warehouse_id', 'Warehouse') }}<br/><br/>
          {{ Form::select('warehouse_id',array('placeholder'=>'Choose Warehouse')+$populate_warehouse),null }}
      </div><br/>
      <div>
          {{ Form::label('material_issuance_no', 'Material Issuance No') }}<br/><br/>
          {{ Form::text('material_issuance_no', Input::old('material_issuance_no')) }}
      </div><br/>
      <div>
          {{Form::label('material_issuance_date', 'Material Issuance Date') }}<br/><br/>
          {{Form::input('text', 'material_issuance_date', Input::old('material_issuance_date'))}}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{Form::label('material_issuance_request_by', 'Material Issuance Request By') }}<br/><br/>
          {{Form::input('text', 'material_issuance_request_by', Input::old('material_issuance_request_by'))}}
      </div><br/>
      <div>
          {{ Form::label('material_issuance_request_date', 'Material Issuance Request Date') }}<br/><br/>
          {{ Form::text('material_issuance_request_date', Input::old('material_issuance_request_date')) }}
      </div><br/>
      <div>
          {{ Form::label('material_issuance_approved_by', 'Material Issuance Approved By') }}<br/><br/>
          {{ Form::text('material_issuance_approved_by', Input::old('material_issuance_approved_by')) }}
      </div><br/>
      <div>
          {{ Form::label('material_issuance_approved_date', 'Material Issuance Approved Date') }}<br/><br/>
          {{Form::input('text', 'material_issuance_approved_date', Input::old('material_issuance_approved_date'))}}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{Form::label('material_issuance_received_by', 'Material Issuance Received By') }}<br/><br/>
          {{Form::text('material_issuance_received_by', Input::old('material_issuance_received_by')) }}
      </div><br/>
      <div>
          {{Form::label('material_issuance_received_date', 'Material Issuance Received Date')}}<br/><br/>
          {{Form::input('text', 'material_issuance_received_date', Input::old('material_issuance_received_date'))}}
      </div>
</div>
<div style="clear:both;"></div><br>
<input type="text" id="material_code" placeholder="enter barcode"\><br><br>
<table id="material" class="bordered">
    <thead>
      <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Unit</th>
        <th>Quantity</th>
        <th>Remarks</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr> 
      </tr>
    </tbody>
    <tfoot>
      <tr>
      </tr>
    </tfoot>
</table>

<br/>
<input type="button" class="button orange" id="create" value="Create the Material Issuance!" \>
<script type="text/javascript">
var quantities=[];
var remarks=[];
var totalQuantity = 0;
var totalRow =0;
var materials = [];
var isExists = false;
$("#material_issuance_date").datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$("#material_issuance_request_date").datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$("#material_issuance_approved_date").datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$("#material_issuance_received_date").datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

function deleteRow(i)
{
    // if (totalRow==1) {
    document.getElementById('material').deleteRow(i+1);
    // if (totalRow == i) {
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[i];
    
    quantities.splice(i-1,1); 
    
    remarks.splice(i-1,1); 
    
    materials.splice(i-1,1);
    console.log(materials,"this is material in delete row 1");
    changeQuantity();

    if(totalRow==0){
        console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 

  if (index<1) {
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}

function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    // console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    // console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}

$("#create").click(function(){
    var materialTakeOffId = $("#material_take_off_id").val();
    var warehouseId = $("#warehouse_id").val();
    var materialIssuanceNo = $("#material_issuance_no").val();
    var materialIssuanceDate= $("#material_issuance_date").val();
    var materialIssuanceRequestBy= $("#material_issuance_request_by").val();
    var materialIssuanceRequestDate=$("#material_issuance_request_date").val();
    var materialIssuanceApprovedBy=$("#material_issuance_approved_by").val();
    var materialIssuanceApprovedDate=$("#material_issuance_approved_date").val();
    var materialIssuanceReceivedBy=$("#material_issuance_received_by").val();
    var materialIssuanceReceivedDate=$("#material_issuance_received_date").val();
    if (!materialTakeOffId) {alert('Material Take Off is required');$("#material_take_off_id").focus();return;};
    if(materialTakeOffId=='placeholder'){
        alert('Material Take Off is required');$("#material_take_off_id").focus();return;
    };
    if (!warehouseId){alert('Warehouse is required');$("#warehouse_id").focus();return;};
    if(warehouseId=='placeholder'){
        alert('Warehouse is required');$("#warehouse_id").focus();return;
    };
    if (!materialIssuanceNo) {alert('Material Issuance No is required');$("#material_issuance_no").focus();return;};
    if (!materialIssuanceDate) {alert('Material Issuance Date is required');$("#material_issuance_date").focus();return;};
    if (!materialIssuanceRequestBy) {alert('Material Issuance Request By is required');$("#material_issuance_request_by").focus();return;};
    if (!materialIssuanceRequestDate) {alert('Material Issuance Request Date is required');$("#material_issuance_request_date").focus();return;};
    if (!materialIssuanceApprovedBy) {alert('Material Issuance Approved By is required');$("#material_issuance_approved_by").focus();return;};
    if (!materialIssuanceApprovedDate) {alert('Material Issuance Approved Date is required');$("#material_issuance_approved_date").focus();return;};
    if (!materialIssuanceReceivedBy) {alert('Material Issuance Received By is required');$("#material_issuance_received_by").focus();return;};
    if (!materialIssuanceReceivedDate) {alert('Material Issuance Received Date is required');$("#material_issuance_received_date").focus();return;};
    if (materials.length==0) {
      alert('Material is required, Please enter barcode!');$("#material_code").focus();return;
    };
    $.ajax({
          type: "get",
          url: 'create/createWithAjax',
          data:{"materialTakeOffId":materialTakeOffId,"warehouseId":warehouseId,"materialIssuanceNo":materialIssuanceNo,"materialIssuanceDate":materialIssuanceDate,"materialIssuanceRequestBy":materialIssuanceRequestBy,"materialIssuanceRequestDate":materialIssuanceRequestDate,"materialIssuanceApprovedBy":materialIssuanceApprovedBy,"materialIssuanceApprovedDate":materialIssuanceApprovedDate,"materialIssuanceReceivedBy":materialIssuanceReceivedBy,"materialIssuanceReceivedDate":materialIssuanceReceivedDate,"materials":materials,"quantities":quantities,"remarks":remarks,"totalRow":totalRow},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            // console.log(jqXHR.status,"this is jqXHR");
            // console.log(status,"status");
            // console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result){
              //console.log(result,"ini resultnya");
              window.location.replace("http://localhost:81/apptrackingsystem/public/material_issuances");
          },
    });
});
$("#material_code").autocomplete({
    source:'create/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'create/find/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          //check if exists
          for(var i=0;i<materials.length;i++) {
            //jika sudah ada pada grid maka hanya menambah quantitynya
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {}else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#quantity'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}
</script>

{{ Form::close() }}

@stop
@section('footer')
    @parent
@stop


