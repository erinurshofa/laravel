<!-- app/views/transactions/material_issuances/show.blade.php -->

@extends('layout')

@section('title')
Show Material Receiving
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Receiving</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Purchase Order :{{$material_receiving->purchase_order_id}}<br>
        Warehouse :{{$material_receiving->warehouse_id}}<br>
        Material Receiving No :{{$material_receiving->material_receiving_no}}<br>
        Material Receiving PLBL :{{$material_receiving->material_receiving_plbl}}<br>
        Material Receiving Shipment :{{$material_receiving->material_receiving_shipment}}<br>
        Material Receiving Date :{{$material_receiving->material_receiving_date}}<br>
        Material Receiving Unpacked Date :{{$material_receiving->material_receiving_unpacked_date}}<br>
        Material Receiving Checked By:{{$material_receiving->material_receiving_checked_by}}<br>
        Material Receiving Checked Date:{{$material_receiving->material_receiving_checked_date}}<br>
        Material Receiving Approved By:{{$material_receiving->material_receiving_approved_by}}<br>
        Material Receiving Approved Date:{{$material_receiving->material_receiving_approved_date}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop