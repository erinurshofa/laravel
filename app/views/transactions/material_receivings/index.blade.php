<!-- app/views/transactions/material_receivings/index.blade.php -->

@extends('layout')

@section('title')
Transaction Material Receiving List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material Receivings</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('material_receivings/create') }}">Add Material Receiving</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('material_receivings.search'))) }}
      {{ Form::text('material_receiving', null, array( 'placeholder' => 'Search material receiving...' ))}}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Purchase Order</th>
                <th>Warehouse</th>
                <th>Material Receiving No</th>
                <th>Material Receiving PLBL</th>
                <th>Material Receiving Shipment</th> 
                <th>Material Receiving Date</th>
                <th>Material Receiving Unpacked Date</th>
                <th>Material Receiving Checked By</th>
                <th>Material Receiving Checked Date</th>
                <th>Material Receiving Approved By</th>
                <th>Material Receiving Approved Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_receivings as $key => $value)
            <tr>
                <td>{{PurchaseOrder::where('id','=',$value->purchase_order_id)->lists('po_no','id')[$value->purchase_order_id]}}</td>
                <td>{{Warehouse::where('id','=',$value->warehouse_id)->lists('description','id')[$value->warehouse_id]}}</td>
                <td>{{ $value->material_receiving_no }}</td>
                <td>{{ $value->material_receiving_plbl }}</td>
                <td>{{ $value->material_receiving_shipment }}</td>
                <td>{{ $value->material_receiving_date }}</td>
                <td>{{ $value->material_receiving_unpacked_date }}</td>
                <td>{{ $value->material_receiving_checked_by }}</td>
                <td>{{ $value->material_receiving_checked_date }}</td>
                <td>{{ $value->material_receiving_approved_by }}</td>
                <td>{{ $value->material_receiving_approved_date }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('material_receivings/' . $value->id) }}">Show this Material Receiving</a>
                    <a class="button green" href="{{ URL::to('material_receivings/' . $value->id . '/edit') }}">Edit this Material Receiving</a>
                    {{Form::model($value->id, array('route' => array('material_receivings.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $material_receivings->links() }}</div>
@stop

@section('footer')
    @parent
@stop