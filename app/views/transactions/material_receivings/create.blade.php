<!-- app/views/transactions/material_receivings/create.blade.php -->
@extends('layout')

@section('title')
Create Material Receiving
@stop

@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Transaction Material Receiving</h1>
<br/>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
<!-- if there are creation errors, they will show here -->
{{HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;'))}}
{{Form::open(array('url' => 'material_receivings'))}}
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;">
      <div>
          {{ Form::label('purchase_order_id', 'Purchase Order') }}<br/><br/>
          {{ Form::select('purchase_order_id',array('placeholder'=>'Choose Purchase Order')+$populate_purchase_order),null }}
      </div><br/>
      <div>
          {{ Form::label('warehouse_id', 'Warehouse') }}<br/><br/>
          {{ Form::select('warehouse_id',array('placeholder'=>'Choose Warehouse')+$populate_warehouse),null }}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_no', 'Material Receiving No') }}<br/><br/>
          {{ Form::text('material_receiving_no', Input::old('material_receiving_no')) }}
      </div><br/>
      <div>
          {{Form::label('material_receiving_plbl', 'Material Receiving PLBL') }}<br/><br/>
          {{Form::input('text', 'material_receiving_plbl', Input::old('material_receiving_plbl'))}}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('material_receiving_shipment', 'Material Receiving Shipment') }}<br/><br/>
          {{ Form::text('material_receiving_shipment', Input::old('material_receiving_shipment')) }}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_date', 'Material Receiving Date') }}<br/><br/>
          {{ Form::text('material_receiving_date', Input::old('material_receiving_date')) }}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_unpacked_date', 'Material Receiving Unpacked Date') }}<br/><br/>
          {{Form::input('text', 'material_receiving_unpacked_date', Input::old('material_receiving_unpacked_date'))}}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_checked_by', 'Material Receiving Checked By') }}<br/><br/>
          {{ Form::text('material_receiving_checked_by', Input::old('material_receiving_checked_by')) }}
      </div><br/>
</div>
<div style="float:left;margin-left:10px;">
      <div>
          {{ Form::label('material_receiving_checked_date', 'Material Receiving Checked Date') }}<br/><br/>
          {{Form::input('text', 'material_receiving_checked_date', Input::old('material_receiving_checked_date'))}}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_approved_by', 'Material Receiving Approved By') }}<br/><br/>
          {{Form::input('text', 'material_receiving_approved_by', Input::old('material_receiving_approved_by'))}}
      </div><br/>
      <div>
          {{ Form::label('material_receiving_approved_date', 'Material Receiving Approved Date') }}<br/><br/>
          {{Form::input('text', 'material_receiving_approved_date', Input::old('material_receiving_approved_date'))}}
      </div><br/>
</div>
<div style="clear:both;"></div><br>
<input type="text" id="material_code" placeholder="enter barcode"\><br><br>
<table id="material" class="bordered">
    <thead>
      <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Unit</th>
        <th>Quantity</th>
        <th>Remarks</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr> 
      </tr>
    </tbody>
    <tfoot>
      <tr>
      </tr>
    </tfoot>
</table>

<br/>
<input type="button" class="button orange" id="create" value="Create the Material Receiving!" \>
<script type="text/javascript">

var quantities=[];
var remarks=[];
var totalQuantity = 0;

var totalRow =0;
var materials = [];
var isExists = false;
$( "#material_receiving_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#material_receiving_unpacked_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#material_receiving_checked_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#material_receiving_approved_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

function deleteRow(i)
{
    // if (totalRow==1) {
    document.getElementById('material').deleteRow(i+1);
    // if (totalRow == i) {
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[i];

    
    quantities.splice(i-1,1); 

    
    remarks.splice(i-1,1); 
    
    materials.splice(i-1,1);
    console.log(materials,"this is material in delete row 1");
    changeQuantity();


    if(totalRow==0){
        console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 

  if (index<1) {
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}


function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    // console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    // console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}




$("#create").click(function(){
    var purchaseOrderId = $("#purchase_order_id").val();
    var warehouseId = $("#warehouse_id").val();
    var materialReceivingNo = $("#material_receiving_no").val();
    var materialReceivingPlbl= $("#material_receiving_plbl").val();
    var materialReceivingShipment= $("#material_receiving_shipment").val();
    var materialReceivingDate=$("#material_receiving_date").val();
    var materialReceivingUnpackedDate=$("#material_receiving_unpacked_date").val();
    var materialReceivingCheckedBy=$("#material_receiving_checked_by").val();
    var materialReceivingCheckedDate=$("#material_receiving_checked_date").val();
    var materialReceivingApprovedBy=$("#material_receiving_approved_by").val();
    var materialReceivingApprovedDate=$("#material_receiving_approved_date").val();
    if (!purchaseOrderId){alert('Purchase Order is required');$("#purchase_order_id").focus();return;};
    if (purchaseOrderId=='placeholder'){alert('Purchase Order is required');$("#purchase_order_id").focus();return;};
    if (!warehouseId){alert('Warehouse is required');$("#warehouse_id").focus();return;};
    if (warehouseId=='placeholder'){alert('Warehouse is required');$("#warehouse_id").focus();return;};
    if (!materialReceivingNo) {alert('Material Receiving No is required');$("#material_receiving_no").focus();return;};
    if (!materialReceivingPlbl) {alert('Material Receiving Plbl is required');$("#material_receiving_plbl").focus();return;};
    if (!materialReceivingShipment) {alert('Material Receiving Shipment is required');$("#material_receiving_shipment").focus();return;};
    if (!materialReceivingDate) {alert('Material Receiving Date is required');$("#material_receiving_date").focus();return;};
    if (!materialReceivingUnpackedDate) {alert('Material Receiving Unpacked Date is required');$("#material_receiving_unpacked_date").focus();return;};
    if (!materialReceivingCheckedBy) {alert('Material Receiving Checked By is required');$("#material_receiving_checked_by").focus();return;};
    if (!materialReceivingCheckedDate) {alert('Material Receiving Checked Date is required');$("#material_receiving_checked_date").focus();return;};
    if (!materialReceivingApprovedBy) {alert('Material Receiving Approved By is required');$("#material_receiving_approved_by").focus();return;};
    if (!materialReceivingApprovedDate) {alert('Material Receiving Approved Date is required');$("#material_receiving_approved_date").focus();return;};

    
    if (materials.length==0) {
      alert('Material is required, Please enter barcode!');$("#material_code").focus();return;
    };
    $.ajax({
          type: "get",
          url: 'create/createWithAjax',
          data:{"purchaseOrderId":purchaseOrderId,"warehouseId":warehouseId,"materialReceivingNo":materialReceivingNo,"materialReceivingPlbl":materialReceivingPlbl,"materialReceivingShipment":materialReceivingShipment,"materialReceivingDate":materialReceivingDate,"materialReceivingUnpackedDate":materialReceivingUnpackedDate,"materialReceivingCheckedBy":materialReceivingCheckedBy,"materialReceivingCheckedDate":materialReceivingCheckedDate,"materialReceivingApprovedBy":materialReceivingApprovedBy,"materialReceivingApprovedDate":materialReceivingApprovedDate,"materials":materials,"quantities":quantities,"remarks":remarks,"totalRow":totalRow}, 
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            // console.log(jqXHR.status,"this is jqXHR");
            // console.log(status,"status");
            // console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result) {
              window.location.replace("http://localhost:81/apptrackingsystem/public/material_receivings");
          },
    });
});
$("#material_code").autocomplete({
    source:'create/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'create/find/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          //check if exists
          for(var i=0;i<materials.length;i++) {
            //jika sudah ada pada grid maka hanya menambah quantitynya
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {}else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#quantity'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}
</script>

{{ Form::close() }}

@stop
@section('footer')
    @parent
@stop


