 app/views/transactions/material_receivings/edit.blade.php -->

@extends('layout')

@section('title')
Edit Material Receiving
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Material Receiving</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($material_receiving, array('route' => array('material_receivings.update', $material_receiving->id), 'method' => 'PUT')) }}
{{Form::hidden('materialReceivingId', $material_receiving->id,array('id'=>'material_receiving_id')) }} 
{{Form::hidden('totalRow',Input::old('totalRow'),array('id'=>'totalRow'))}}
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('purchase_order_id', 'Project') }}<br/><br/>
        {{ Form::select('purchase_order_id',array('placeholder'=>'Choose Purchase Order')+$populate_purchase_order),null }}
    </div><br/>
    <div>
        {{ Form::label('warehouse_id', 'Project') }}<br/><br/>
        {{ Form::select('warehouse_id',array('placeholder'=>'Choose Warehouse')+$populate_warehouse),null }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_receiving_no', 'Material Receiving No') }}<br/><br/>
        {{ Form::text('material_receiving_no', null) }}
    </div><br/>
    <div>
        {{ Form::label('material_receiving_plbl', 'Material Receiving Plbl') }}<br/><br/>
        {{ Form::text('material_receiving_plbl', Input::old('material_receiving_plbl')) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_receiving_shipment', 'Material Receiving Shipment') }}<br/><br/>
        {{ Form::text('material_receiving_shipment', null) }}
    </div><br/>
    <div>
        {{ Form::label('material_receiving_date', 'Material Receiving Date') }}<br/><br/>
        {{ Form::text('material_receiving_date', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_receiving_unpacked_date', 'Material Receiving Unpacked Date') }}<br/><br/>
        {{ Form::text('material_receiving_unpacked_date', null) }}
    </div><br/>
    <div>
        {{ Form::label('material_receiving_checked_by', 'Checked By') }}<br/><br/>
        {{ Form::text('material_receiving_checked_by', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_receiving_checked_date', 'Checked Date') }}<br/><br/>
        {{ Form::text('material_receiving_checked_date', null) }}
    </div><br/>
    <div>
        {{ Form::label('material_receiving_approved_by', 'Approved By') }}<br/><br/>
        {{ Form::text('material_receiving_approved_by', null) }}
    </div><br/>
</div>
<div style="float:left;margin-left:10px;">
    <div>
        {{ Form::label('material_receiving_approved_date', 'Approved Date') }}<br/><br/>
        {{ Form::text('material_receiving_approved_date', null) }}
    </div><br/>
</div>
 <div style="clear:both;"></div><br>

 <input type="text" id="material_code" placeholder="enter barcode"\><br><br>
 <table id="material" class="bordered"  onload='loadMaterialReceivingDetail()'>
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Remarks</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
<!--        <th></th>
            <th style="border-left:none;"></th>
            <th style="border-left:none;"></th>
            <th><b>Total Quantity     :</b></th>
            <th><span id="totalQuantity"></span></th>
            <th></th>
            <th></th> -->
        </tfoot>
    </table>
<br/>
<br/>

<!-- {{ Form::submit('Edit the Material Take Off!', array('class' => 'button orange')) }} -->
<input type="button" class="button orange" id="edit" value="Edit the Material Take Off!" \>
<input type="hidden" id="material_take_off_detail_id"\>
{{ Form::close() }}

<script type="text/javascript">
var materialReceivingId = $("#material_receiving_id").val();



var quantities=[];
var remarks=[];
var totalQuantity = 0;
var totalRow =1;
var materials = [];
var isExists = false;
$( "#mto_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#mto_request_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});
$( "#mto_approved_date" ).datepicker({
  beforeShowDay: $.datepicker.noWeekends
});

// function getTotalRow(){
//     return $("#totalRow").val();
// }

// function setTotalRow(value){
//     document.getElementById('totalRow').value = value;
// }

function deleteRow(index)
{
 if (totalRow==1) {
    document.getElementById('material').deleteRow(index);
    remarks.splice(index-1,1); 
    materials.splice(index-1,1);
  }else{
    document.getElementById('material').deleteRow(index+1);
    remarks.splice(index,1); 
    materials.splice(index,1);
  }
    totalRow = totalRow-1;
    totalQuantity=totalQuantity-quantities[index];
    quantities.splice(index-1,1); 
    // console.log(materials[0],"this is material in delete row 1");
    changeQuantity();

    if(totalRow==0){
        // console.log("delete tfoot disini");
        var foot = $("#material").find('tfoot');
        foot.remove();
    };
}
function getRowIndex(obj)
{
  var index =obj.parentNode.parentElement.sectionRowIndex; 

  // var index = $(this).closest("tr").index();
  if (index<1) {
    console.log("kesini kah?");
    return 1;
  }else{
    return  index;
  };
}
function getQuantity(value)
{
  if (isNaN(value)) {
    return 0;
  };
  return value;
}

function changeRemarks()
{
  for (var i = totalRow; i >= 1; i--) {
    remarks[i-1]=$('#remarks'+i).val();
    console.log(remarks,"this is remarks");
  };
}
function changeQuantity()
{
  totalQuantity =0;
  for (var i = totalRow; i >= 1; i--) {
    totalQuantity += getQuantity(parseInt($('#quantity'+i).val()));
    quantities[i-1]=getQuantity(parseInt($('#quantity'+i).val()));
    console.log(quantities,"this is quantities");    
  };
  if(isNaN(totalQuantity))
  {
    // totalQuantity=0;
  }else{
    document.getElementById('totalQuantity').innerHTML = totalQuantity;
  }
}

$("#material_code").autocomplete({
    source:'edit/autocompleteCodeMaterial',
    minLength:1,
    select:function(event, ui){
        document.getElementById('material_code').value = ui.item.value;                            
        changeInputMaterialCode();
      }
});

$('#material_code').change(function(e) {
    e.preventDefault();
    changeInputMaterialCode();
});

function changeInputMaterialCode()
{
    var materialCode = $("#material_code").val();
      $.ajax({
        type: "GET",
        url: 'http://localhost:81/apptrackingsystem/public/material_receivings/findMaterialReceivingByCode/'+materialCode,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('material code not exist');
        },
        success: function (result, test) {
          console.log(result,'result when code is change in textbox');
          //check if exists
          for(var i=0;i<materials.length;i++) {
              if(materials[i].id == result[0].id) {
                var quantity = $('#quantity'+result[0].id).val();
                document.getElementById('quantity'+result[0].id).value = parseInt(quantity)+1;
                isExists = true;
                changeQuantity();
                //2 exists
                break;
              }
              isExists =false;
          }
          if (result[0].code === undefined) {
                  alert('Tidak ada item dengan barcode '+materialCode);
          }else if(isExists)///ketika ada yang sama
          {
              }else{
                  materials.push(result[0]);
                  totalRow+=1;
                      $('#material > tbody:last')
                            .append('<tr>'+
                               '<td>'+result[0].code+'</td>'+
                               '<td>'+result[0].name+'</td>'+
                               '<td>'+result[0].description+'</td>'+
                               '<td>'+result[0].unit+'</td>'+
                               '<td><input type="number" id="quantity'+totalRow+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+
                               '<td><input type="text" id="remarks'+totalRow+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                               '</tr>');
                        // alert('Item dengan barcode '+materialCode+ ' telah ditambahkan!');
                        $('#quantity'+totalRow).focus();
                //buat tfoot
                 if(totalRow ==1) {
                    var foot = $("#material").find('tfoot');
                    if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                          foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b></th>'+
                                        '<th><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                ));
                  };
                  document.getElementById('totalRow').value=totalRow;
              };
            },
        });
}

$("#edit").click(function(){
    var purchaseOrderId = $("#purchase_order_id").val();
    var warehouseId = $("#warehouse_id").val();
    var materialReceivingNo = $("#material_receiving_no").val();
    var materialReceivingPlbl = $("#material_receiving_plbl").val();
    var materialReceivingShipment= $("#material_receiving_shipment").val();
    var materialReceivingDate= $("#material_receiving_date").val();
    var materialReceivingUnpackedDate=$("#material_receiving_unpacked_date").val();
    var materialReceivingCheckedBy=$("#material_receiving_checked_by").val();
    var materialReceivingCheckedDate=$("#material_receiving_checked_date").val();
    var materialReceivingApprovedBy=$("#material_receiving_approved_by").val();
    var materialReceivingApprovedDate=$("#material_receiving_approved_date").val();
    // var materialReceivingId=$("#material_take_off_id").val();
    // var materialReceivingDetailId=$("#material_take_off_detail_id").val();
    
    if (totalRow<1) {
      alert("Material is required! Please enter barcode!");
      $("#material_code").focus();
      return;
    };

    if (!purchaseOrderId) {alert('Purchase Order is required');$("#purchase_order_id").focus();return;};
    if (!warehouseId) {alert('Warehouse is required');$("#warehouse_id").focus();return;};
    if (!materialReceivingNo) {alert('Material Receiving No is required');$("#material_receiving_no").focus();return;};
    if (!materialReceivingPlbl) {alert('Material Receiving Plbl is required');$("#material_receiving_plbl").focus();return;};
    if (!materialReceivingShipment) {alert('Material Receiving Shipment is required');$("#material_receiving_shipment").focus();return;};
    if (!materialReceivingDate) {alert('Material Receiving Date is required');$("#material_receiving_date").focus();return;};
    if (!materialReceivingCheckedBy) {alert('Material Receiving Checked By is required');$("#material_receiving_checked_by").focus();return;};
    if (!materialReceivingCheckedDate) {alert('Material Receiving Checked Date is required');$("#material_receiving_checked_date").focus();return;};
    if (!materialReceivingApprovedBy) {alert('Material Receiving Approved By is required');$("#material_receiving_approved_by").focus();return;};
    if (!materialReceivingApprovedDate) {alert('Material Receiving Approved Date is required');$("#material_receiving_approved_date").focus();return;};
    $.ajax({
          type: "get",
          url: 'edit/editWithAjax',
          data:{"purchaseOrderId":purchaseOrderId,"warehouseId":warehouseId,"materialReceivingNo":materialReceivingNo,"materialReceivingPlbl":materialReceivingPlbl,"materialReceivingShipment":materialReceivingShipment,"materialReceivingDate":materialReceivingDate,"materialReceivingCheckedBy":materialReceivingCheckedBy,"materialReceivingCheckedDate":materialReceivingCheckedDate,"materialReceivingApprovedBy":materialReceivingApprovedBy,"materialReceivingApprovedDate":materialReceivingApprovedDate,"totalRow":totalRow,"materials":materials,"quantities":quantities,"remarks":remarks},
          dataType:'html',
          contentType: "application/json; charset=utf-8",
          error:function (jqXHR, status, thrownError) {
            console.log(jqXHR.status,"this is jqXHR");
            console.log(status,"status");
            console.log(thrownError,"thrownError");
            alert(thrownError);

            },
            success: function (result, test) {
              console.log(result,"this is result");
              window.location.replace("http://localhost:81/apptrackingsystem/public/material_receivings");
          },
    });
});


$(document).ready(function() {
    //get list material
    $.ajax({
        type:"get",
        url:'http://localhost:81/apptrackingsystem/public/material_receivings/getListMaterial/'+materialReceivingId,
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        error:function (jqXHR, status, thrownError) {
            alert('error');
        },
        success: function (result1, test) {
            totalRow = 0;
            totalQuantity=0;
          ///get material detail
          $.ajax({
              type:"get",
              url:'http://localhost:81/apptrackingsystem/public/material_receivings/getListMaterialReceivingDetail/'+materialReceivingId,
              dataType:'json',
              contentType: "application/json; charset=utf-8",
              error:function (jqXHR, status, thrownError) {
                  alert('material code not exist');
              },
              success: function (result2, test) {
                  
                  for (var i = 0; i < result1.length; i++) {
                    totalRow+=1;
                    materials.push(result1[i]);
                    quantities.push(result2[i].quantity);
                    remarks.push(result2[i].remarks);
                    totalQuantity+=result2[i].quantity;
                            $('#material > tbody:last').append('<tr>'+
                                '<td>'+materials[i].code+'</td>'+
                                '<td>'+materials[i].name+'</td>'+
                                '<td>'+materials[i].description+'</td>'+
                                '<td>'+materials[i].unit+'</td>'+
                                '<td><input type="number" id="quantity'+totalRow+'" value="'+result2[i].quantity+'" style="margin-left:20px;" onchange="changeQuantity()"/></td>'+ 
                                '<td><input type="text" id="remarks'+totalRow+'" value="'+result2[i].remarks+'" onchange="changeRemarks()"/></td>'+'<td><input type="button" value="delete" class="button red" onclick="deleteRow(getRowIndex(this))"/></td>'+
                                '</tr>');
                    };

                      //buat tfoot
                      // if(totalRow ==1) {
                          var foot = $("#material").find('tfoot');
                            if (!foot.length) foot = $('<tfoot>').appendTo("#material"); 
                                foot.append($('<th></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th style="border-left:none;"></th>'+
                                        '<th><b>Total Quantity     :</b></th>'+
                                        '<th><span id="totalQuantity"></span></th>'+
                                        '<th></th>'+
                                        '<th></th>'
                                        ));
                      // };
                      // document.getElementById('material_receiving_detail_id').value = result2[0].id;
                      // document.getElementById('material_receiving_id').value = result2[0].material_receiving_id;
                      document.getElementById('totalQuantity').innerHTML = totalQuantity;
                      document.getElementById('totalRow').value=totalRow;
                }
          });
          console.log("lewat ajax");  
        }
    });
});

</script>

@stop

@section('footer')
    @parent
@stop
