<!-- app/views/transactions/material_constructions/show.blade.php -->

@extends('layout')

@section('title')
Show Material Construction
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Construction</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Material Issuance :{{$material_construction->material_issuance_id}}<br>
        Remarks :{{$material_construction->remarks}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop