<!-- app/views/transactions/material_construction/index.blade.php -->

@extends('layout')

@section('title')
Transaction Material Construction List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material Constructions</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    
    <a class="button orange" href="{{ URL::to('material_constructions/create') }}">Add Material Construction</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('material_constructions.search'))) }}
      {{ Form::text('material_construction', null, array( 'placeholder' => 'Search material construction...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Material Issuance</th>
                <th>Remarks</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_constructions as $key => $value)
            <tr>
                <td>{{MaterialIssuance::where('id','=',$value->material_issuance_id)->lists('material_issuance_no','id')[$value->material_issuance_id]}}</td>
                <td>{{ $value->remarks }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('material_constructions/' . $value->id) }}">Show this Material Construction</a>
                    <a class="button green" href="{{ URL::to('material_constructions/' . $value->id . '/edit') }}">Edit this Material Construction</a>
                    {{Form::model($value->id, array('route' => array('material_constructions.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $material_constructions->links() }}</div>
@stop

@section('footer')
    @parent
@stop