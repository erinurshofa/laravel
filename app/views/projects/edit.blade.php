<!-- app/views/projects/edit.blade.php -->

@extends('layout')

@section('title')
Edit Project
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Project</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($project, array('route' => array('projects.update', $project->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', null) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', null) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', null) }}
  </div>
      <div>
    {{ Form::label('owner_name', 'Owner Name') }}<br/>
    {{ Form::text('owner_name', null) }}
  </div>
    <div>
    {{ Form::label('owner_address', 'Owner Address') }}<br/>
    {{ Form::text('owner_address', null) }}
  </div>
    <div>
    {{ Form::label('owner_phone', 'Owner Phone') }}<br/>
    {{ Form::text('owner_phone', null) }}
  </div>
      <div>
    {{ Form::label('budget', 'Budget') }}<br/>
    {{ Form::text('budget', null) }}
  </div>
      <div>
    {{ Form::label('start_date', 'Start Date') }}<br/>
    {{Form::input('date', 'start_date','start_date')}} 
    <!-- {{ Form::text('start_date', null) }} -->
  </div>
      <div>
    {{ Form::label('duration', 'Duration') }}<br/>
    {{ Form::text('duration', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Project!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop