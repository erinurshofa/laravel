<!-- app/views/vendors/index.blade.php -->

@extends('layout')

@section('title')
Project List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Projects</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('projects/create') }}">Add Project</a>
    <a class="button white" href="{{ URL::to('reports/projects') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('projects.search'))) }}
      {{ Form::text('project', null, array( 'placeholder' => 'Search project...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Owner Name</th>
                <th>Owner Address</th>
                <th>Owner Phone</th>
                <th>Budget</th>
                <!-- <th>Start Date</th> -->
                <th>Duration</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projects as $key => $value)
            <tr>
                <td>{{ $value->code }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->owner_name }}</td>
                <td>{{ $value->owner_address }}</td>
                <td>{{ $value->owner_phone }}</td>
                <td>{{ $value->budget }}</td>
                <!-- <td>{{ $value->start_date }}</td> -->
                <td>{{ $value->duration }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('projects/' . $value->id) }}">Detail</a>
                    <a class="button green" href="{{ URL::to('projects/' . $value->id . '/edit') }}">Edit</a>
                    {{Form::model($value->id, array('route' => array('projects.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $projects->links() }}</div>
@stop

@section('footer')
    @parent
@stop