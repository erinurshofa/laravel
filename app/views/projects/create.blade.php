<!-- app/views/projects/create.blade.php -->

@extends('layout')

@section('title')
Create Project
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Project</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::open(array('url' => 'projects')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', Input::old('code')) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', Input::old('name')) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', Input::old('description')) }}
  </div>
      <div>
    {{ Form::label('owner_name', 'Owner Name') }}<br/>
    {{ Form::text('owner_name', Input::old('owner_name')) }}
  </div>
    <div>
    {{ Form::label('owner_address', 'Owner Address') }}<br/>
    {{ Form::text('owner_address', Input::old('owner_adress')) }}
  </div>
    <div>
    {{ Form::label('owner_phone', 'Owner Phone') }}<br/>
    {{ Form::text('owner_phone', Input::old('owner_phone')) }}
  </div>
    <div>
    {{ Form::label('budget', 'Budget') }}<br/>
    {{ Form::text('budget', Input::old('budget')) }}
  </div>
      <div>
    {{ Form::label('start_date', 'Start Date') }}<br/>
    {{Form::input('date', 'start_date', Input::old('start_date'))}}
  </div>
      <div>
    {{ Form::label('duration', 'Duration') }}<br/>
    {{ Form::text('duration', Input::old('duration')) }}
  </div>

<br/>
  {{ Form::submit('Create the Project!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop