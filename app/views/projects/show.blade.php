<!-- app/views/projects/show.blade.php -->

@extends('layout')

@section('title')
Show Project
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Projects</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Project :{{$project->code}}<br>
        Name :{{$project->name}}<br>
        Description :{{$project->description}}<br>
        Owner Name :{{$project->owner_name}}<br>
        Owner Address :{{$project->owner_address}}<br>
        Owner Phone :{{$project->owner_phone}}<br>
        Budget :{{$project->budget}}<br>
        Start Date :{{$project->start_date}}<br>
        Duration :{{$project->duration}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop