<!-- app/views/material_categories/create.blade.php -->

@extends('layout')

@section('title')
Create Material Category
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Material Category</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::open(array('url' => 'material_categories')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', Input::old('code')) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', Input::old('name')) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', Input::old('description')) }}
  </div>

<br/>
  {{ Form::submit('Create the Material Category!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop