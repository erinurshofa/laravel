<!-- app/views/material_categories/index.blade.php -->

@extends('layout')

@section('title')
Material Category List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material Category</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
        @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('material_categories/create') }}">Add Material Category</a>
    <a class="button white" href="{{ URL::to('reports/material_categories') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('material_categories.search'))) }}
      {{ Form::text('material_category', null, array( 'placeholder' => 'Search material category...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_categories as $key => $value)
            <tr>
                <td>{{ $value->code }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('material_categories/' . $value->id) }}">Show this Material Category</a>
                    <a class="button green" href="{{ URL::to('material_categories/' . $value->id . '/edit') }}">Edit this Material Category</a>
                    {{Form::model($value->id, array('route' => array('material_categories.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $material_categories->links() }}</div>
@stop

@section('footer')
    @parent
@stop