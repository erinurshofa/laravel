<!-- app/views/material_categories/edit.blade.php -->

@extends('layout')

@section('title')
Edit Material Category
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Material Category</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($material_category, array('route' => array('material_categories.update', $material_category->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', null) }}
  </div>
  <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', null) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Material Category!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop