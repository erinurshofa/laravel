<!-- app/views/material_categories/show.blade.php -->

@extends('layout')

@section('title')
Show Material Category
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material Categories</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Material Category :{{$material_category->code}}<br>
        Name :{{$material_category->name}}<br>
        Description :{{$material_category->description}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop