<!-- app/views/materials/index.blade.php -->

@extends('layout')

@section('title')
Material List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Material</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
        @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('materials/create') }}">Add Material</a>
    <a class="button white" href="{{ URL::to('reports/materials') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('materials.search'))) }}
      {{ Form::text('material', null, array( 'placeholder' => 'Search material...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Material Category</th>
                <th>Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($materials as $key => $value)
            <tr>
                <td>{{MaterialCategory::where('id','=',$value->material_category_id)->lists('name','id')[$value->material_category_id]}}</td>
                <td>{{ $value->code }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->unit }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('materials/' . $value->id) }}">Show this Material</a>
                    <a class="button green" href="{{ URL::to('materials/' . $value->id . '/edit') }}">Edit this Material</a>
                    {{Form::model($value->id, array('route' => array('materials.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $materials->links() }}</div>
@stop

@section('footer')
    @parent
@stop