<!-- app/views/materials/create.blade.php -->

@extends('layout')
@section('title')
Create Material
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Material</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}
{{ Form::open(array('url' => 'materials')) }}
  <div>
    {{ Form::label('material_category_id', 'Material Category') }}<br/>
    {{ Form::select('material_category_id', $populate_material_category , Input::old('material_categories')) }}
  </div>
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', Input::old('code')) }}
  </div>
    <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', Input::old('name')) }}
  </div>
    <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', Input::old('description')) }}
  </div>
    <div>
    {{ Form::label('unit', 'Unit') }}<br/>
    {{ Form::text('unit', Input::old('unit')) }}
  </div>

<br/>
  {{ Form::submit('Create the Material!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop