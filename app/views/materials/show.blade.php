<!-- app/views/materials/show.blade.php -->

@extends('layout')

@section('title')
Show Material
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Material</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Material Category ID :{{$material->material_category_id}}<br>
        Code :{{$material->code}}<br>
        Name :{{$material->name}}<br>
        Description :{{$material->description}}<br>
        Unit :{{$material->unit}}<br>
        
    </div>
@stop

@section('footer')
    @parent
@stop