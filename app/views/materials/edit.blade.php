<!-- app/views/materials/edit.blade.php -->

@extends('layout')

@section('title')
Edit Material
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Material</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($material, array('route' => array('materials.update', $material->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('material_category_id', 'Material Category') }}<br/>
    {{ Form::select('material_category_id',$populate_material_category, null) }}
  </div>
  <div>
    {{ Form::label('code', 'Code') }}<br/>
    {{ Form::text('code', null) }}
  </div>
    <div>
    {{ Form::label('name', 'Name') }}<br/>
    {{ Form::text('name', null) }}
  </div>
      <div>
    {{ Form::label('description', 'Description') }}<br/>
    {{ Form::text('description', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Material!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop