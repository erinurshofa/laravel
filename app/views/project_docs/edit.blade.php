<!-- app/views/project_docs/edit.blade.php -->

@extends('layout')

@section('title')
Edit Project Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Edit a Project Doc</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}

{{ Form::model($project_doc, array('route' => array('project_docs.update', $project_doc->id), 'method' => 'PUT')) }}
  <div>
    {{ Form::label('project_id', 'Project') }}<br/>
    {{ Form::select('project_id',$populate_project, null) }}
  </div>
  <div>
    {{ Form::label('doc_content', 'Doc Content') }}<br/>
    {{ Form::text('doc_content', null) }}
  </div>
    <div>
    {{ Form::label('octet_type', 'Octet Type') }}<br/>
    {{ Form::text('octet_type', null) }}
  </div>

<br/>
  {{ Form::submit('Edit the Project Doc!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop