<!-- app/views/project_docs/show.blade.php -->

@extends('layout')

@section('title')
Show Project Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
    <h1>Showing Project Doc</h1>
    <div class="alert alert-info" style="margin-left:30px;margin-top:30px;margin-right:30px;">
        Project :{{$project_doc->project_id}}<br>
        Doc Content :{{$project_doc->doc_content}}<br>
        Octet Type :{{$project_doc->octet_type}}<br>
    </div>
@stop

@section('footer')
    @parent
@stop