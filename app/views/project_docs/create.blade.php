<!-- app/views/project_docs/create.blade.php -->

@extends('layout')
@section('title')
Create Project Doc
@stop
@section('menu')
    @parent
@stop

@section('content')
<h1>Create a Project Doc</h1>
<br/>
<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all(),array('class' => 'alert alert-error','style'=>'list-style-type:none;')) }}
{{ Form::open(array('url' => 'project_docs')) }}
  <div>
    {{ Form::label('project_id', 'Project') }}<br/>
    {{ Form::select('project_id', $populate_project , Input::old('project_docs')) }}
  </div>
  <div>
    {{ Form::label('doc_content', 'Doc Content') }}<br/>
    {{ Form::text('doc_content', Input::old('doc_content')) }}
  </div>
    <div>
    {{ Form::label('octet_type', 'Octet Type') }}<br/>
    {{ Form::text('octet_type', Input::old('octet_type')) }}
  </div>

<br/>
  {{ Form::submit('Create the Project Doc!', array('class' => 'button orange')) }}

{{ Form::close() }}
@stop

@section('footer')
    @parent
@stop