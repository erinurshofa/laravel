<!-- app/views/project_docs/index.blade.php -->

@extends('layout')

@section('title')
Project Doc List
@stop
@section('menu')
    @parent
@stop

@section('content')
 <h1>All the Project Doc</h1>
    <br/>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-error">{{ Session::get('warning') }}</div>
    @endif
    <a class="button orange" href="{{ URL::to('project_docs/create') }}">Add Project Doc</a>
    <a class="button white" href="{{ URL::to('reports/project_docs') }}">Reports</a>
      <div class="search" style="display:inline;float:right;">
      {{ Form::model(null, array('route' => array('project_docs.search'))) }}
      {{ Form::text('project_doc', null, array( 'placeholder' => 'Search project doc...' )) }}
      {{ Form::submit('Search',array('class'=>'button black')) }}
      {{ Form::close() }}
      </div>
    <br/>
    <br/>
    <table class="bordered">
        <thead>
            <tr>
                <!-- <td>ID</td> -->
                <th>Project</th>
                <th>Doc Content</th>
                <th>Octet Type</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($project_docs as $key => $value)
            <tr>
                <td>{{Project::where('id','=',$value->project_id)->lists('name','id')[$value->project_id]}}</td>
                <td>{{ $value->doc_content }}</td>
                <td>{{ $value->octet_type }}</td>
                <td>
                <div>
                    <a class="button blue" href="{{ URL::to('project_docs/' . $value->id) }}">Show this Project Doc</a>
                    <a class="button green" href="{{ URL::to('project_docs/' . $value->id . '/edit') }}">Edit this Project Doc</a>
                    {{Form::model($value->id, array('route' => array('project_docs.destroy', $value->id), 'method' => 'DELETE','style'=>'display:inline;')) }}
                    {{Form::hidden('id',$value->id)}}
                    {{Form::submit('Delete',array('class'=>'button red')) }}
                    {{Form::close()}}
                </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:inline;float:right;">{{ $project_docs->links() }}</div>
@stop

@section('footer')
    @parent
@stop