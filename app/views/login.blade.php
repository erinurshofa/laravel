<!-- app/views/login.blade.php -->

@extends('layout')

@section('title')
Login
@stop
@section('menu')
    @parent
@stop


@section('content')
    <!-- will be used to show any messages -->
       @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
{{ Form::open(array('url' => 'login')) }}
		<h1 style="font-color:white;">Silahkan Login!</h1>
		<!-- if there are login errors, show them here -->
		<div>
			{{ Form::label('username', 'Username') }}<br/>
			{{ Form::text('username', Input::old('username'), array('placeholder' => 'your username')) }}      {{ $errors->first('email') }}
		</div>
		<div>
			{{ Form::label('email', 'Email Address') }}<br/>
			{{ Form::text('email', Input::old('email'), array('placeholder' => 'your email')) }}      {{ $errors->first('email') }}
		</div>
		<div>
			{{ Form::label('password', 'Password') }}<br/>
			{{ Form::text('password', Input::old('password'), array('placeholder' => 'your password')) }}      {{ $errors->first('password') }}
		</div>
		{{ Form::submit('Login!',array('class'=>'button blue')) }}<a href="{{URL::to('register')}}" class="button green">Register</a>
{{ Form::close() }} 
@stop

@section('footer')
    @parent
@stop