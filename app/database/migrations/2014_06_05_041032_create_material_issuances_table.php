<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialIssuancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('material_issuances', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('material_take_off_id');
			$table->integer('warehouse_id');
			$table->string('material_issuance_no');
			$table->dateTime('material_issuance_date');
			$table->string('material_issuance_request_by');
			$table->dateTime('material_issuance_request_date');
			$table->string('material_issuance_approved_by');
			$table->dateTime('material_issuance_approved_date');
			$table->string('material_issuance_received_by');
			$table->dateTime('material_issuance_received_date');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('material_issuances');
	}

}
