<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialReceivingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('material_receivings', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('purchase_order_id');
			$table->integer('warehouse_id');
			$table->integer('material_receiving_no');
			$table->string('material_receiving_plbl');
			$table->string('material_receiving_shipment');
			$table->dateTime('material_receiving_date');
			$table->dateTime('material_receiving_unpacked_date');
			$table->string('material_receiving_checked_by');
			$table->dateTime('material_receiving_checked_date');
			$table->string('material_receiving_approved_by');
			$table->dateTime('material_receiving_approved_date');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('material_receivings');
	}
}
