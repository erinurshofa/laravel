<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialConstructionDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('material_construction_details', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('material_construction_id');
			$table->integer('material_id');
			$table->integer('quantity');
			$table->string('remarks');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('material_construction_details');
	}

}
