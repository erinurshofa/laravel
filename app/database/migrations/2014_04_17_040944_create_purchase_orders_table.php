<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_orders', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('material_take_off_id');
			$table->integer('vendor_id');
			$table->string('po_no');
			$table->string('po_revision');
			$table->dateTime('po_date');
			$table->string('po_estimate_date');
			$table->string('po_promised_date');
			$table->string('po_description');
			$table->string('po_status');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchase_orders');
	}

}
