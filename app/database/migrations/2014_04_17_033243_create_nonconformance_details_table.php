<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonconformanceDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nonconformance_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('nonconformance_id');
			$table->integer('material_id');
			$table->string('remarks');
			$table->integer('quantity');
			$table->string('created_user');
			$table->string('updated_user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nonconformance_details');
	}

}
