<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonconformancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nonconformances', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('purchase_order_id');
			$table->string('nonconformance_code');
			$table->dateTime('nonconformance_date');
			$table->string('nonconformance_description');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('nonconformances');
	}

}
