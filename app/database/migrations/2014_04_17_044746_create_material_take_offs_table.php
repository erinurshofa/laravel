<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialTakeOffsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('material_take_offs', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('project_id');
			$table->string('mto_no');
			$table->string('mto_revision');
			$table->dateTime('mto_date');
			$table->string('mto_description');
			$table->string('mto_request_by');
			$table->dateTime('mto_request_date');
			$table->string('mto_approved_by');
			$table->dateTime('mto_approved_date');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('material_take_offs');
	}

}