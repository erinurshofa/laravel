<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialDocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('material_docs', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('material_id');
			$table->string('doc_content');
			$table->string('octet_type');
			$table->string('created_user');
			$table->string('updated_user');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('material_docs');
	}

}
