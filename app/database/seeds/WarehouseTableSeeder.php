<?php

/**
* 
*/
class WarehouseTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('warehouses')->delete();
		Warehouse::create(array(
			'code'=>'WR001',
			'description'=>'Gudang 1',
			'address'=>'Jln Syuhada',
			'phone'=>'082348293432',
			'administrator'=>'admin',
			'created_user'=>'john'
			));

	}
}