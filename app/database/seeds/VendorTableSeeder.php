<?php

class VendorTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('vendors')->delete();
		Vendor::create(array(
			'code'=>'VR001',
			'name'=>'nokia',
			'description'=>'this is description',
			'address'=>'jln syuhada',
			'phone'=>'0932039288',
			'created_user'=>'john',
			'updated_user'=>'john'
			));

	}
}