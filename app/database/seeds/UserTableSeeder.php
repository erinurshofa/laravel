<?php


class UserTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'username'=>'admin',
			'email'=>'erinurshofa@gmail.com',
			'password'=>Hash::make('admin'),
			'remember_token'=>'',
			));
	}
}