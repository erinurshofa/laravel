<?php

class MaterialIssuanceController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_issuances = MaterialIssuance::paginate(5);
    	$this->layout->content = View::make('transactions.material_issuances.index')
    		-> with('material_issuances', $material_issuances);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_material_take_off = DB::table('material_take_offs')->orderBy('mto_no', 'asc')->lists('mto_no','id');
		$populate_warehouse = DB::table('warehouses')->orderBy('description', 'asc')->lists('description','id');
		$populate_material = DB::table('materials')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('transactions.material_issuances.create', array('populate_material_take_off' => $populate_material_take_off,
			'populate_warehouse' => $populate_warehouse,'populate_material' => $populate_material));
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_issuance =MaterialIssuance::find($id);

		$this->layout->content = View::make('transactions.material_issuances.show')
			->with('material_issuance',$material_issuance);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_issuance_details = MaterialIssuanceDetail::where('material_issuance_id', '=', $id)->get();
		$material_issuance = MaterialIssuance::find($id);

		$populate_material_take_off = DB::table('material_take_offs')->orderBy('mto_no', 'asc')->lists('mto_no','id');
		$populate_warehouse = DB::table('warehouses')->orderBy('description', 'asc')->lists('description','id');
		$populate = array();
		foreach ($material_issuance_details as $key => $value) {
			$test = Material::find($value->material_id);
			if ($test) {
				if ($test->count()!=0) {
				$populate[] = $test;
				}
			}

		}
		$this->layout->content = 
					View::make('transactions.material_issuances.edit', 
						array('populate_material_take_off' => $populate_material_take_off, 
							'populate_material'=>$populate))
			->with('material_issuance',$material_issuance)
			->with('material_issuance_details',$material_issuance_details)
			->with('populate_warehouse',$populate_warehouse);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_construction =DB::table('material_constructions')->where('material_constructions.material_issuance_id', '=', $id);
		$material_issuance_detail =DB::table('material_issuance_details')->where('material_issuance_details.material_issuance_id', '=', $id);
		if ($material_construction->count()==0 && $material_issuance_detail->count()== 0){
			$material_issuance = MaterialIssuance::find($id);
			$material_issuance->delete();
			Session::flash('message','Successfully deleted the material issuance');
		}if($material_issuance_detail->count() != 0){
			Session::flash('warning','Material issuance still used in material issuance detail');
		}if($material_construction->count() != 0){
			Session::flash('warning','Material issuance still used in material construction');
		}
		return Redirect::to('material_issuances');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialIssuance()
	{
		$q = Input::get('material_issuance');
		$search='%'.$q.'%';
		$material_issuances = DB::table('material_issuances')
					->select(array('material_issuances.id','material_issuances.material_take_off_id','material_issuances.warehouse_id', 
								'material_issuances.material_issuance_no','material_issuances.material_issuance_date', 
								'material_issuances.material_issuance_request_by', 'material_issuances.material_issuance_request_date',
								'material_issuances.material_issuance_approved_by','material_issuances.material_issuance_approved_date',
								'material_issuances.material_issuance_received_by','material_issuances.mto_received_date'))
					->where('material_issuances.mto_no', 'like', $search)
					->paginate(5);
		if ($material_issuances->count() > 0) {
			Session::flash('message','Successfully search material issuance!' );
		}else{
			Session::flash('warning','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.material_issuances.index')
			-> with('material_issuances', $material_issuances);
	}
	/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}

	/**
	 * listMaterialIssuanceDetail digunakan pada edit view material issuance.
	 *
	 * @return Response
	 */
	public function listMaterialIssuanceDetail($material_issuance_id)
	{
		$materialIssuanceDetail=MaterialIssuanceDetail::where('material_issuance_id', '=', $material_issuance_id)->Get();
        return Response::json($materialIssuanceDetail);
	}

	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($material_issuance_id)
	{
		$materialIssuanceDetail=MaterialIssuanceDetail::where('material_issuance_id', '=', $material_issuance_id)->Get();
    
    	$materials=array();
    	foreach ($materialIssuanceDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$materialIssuanceDate=Input::get('materialIssuanceDate');
		$materialIssuanceRequestDate=Input::get('materialIssuanceRequestDate');
		$materialIssuanceApprovedDate =Input::get('materialIssuanceApprovedDate');
		$materialIssuanceReceivedDate=Input::get('materialIssuanceReceivedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');

    	$date = date("Y-m-d", strtotime($materialIssuanceDate));
    	$requestDate = date("Y-m-d", strtotime($materialIssuanceRequestDate));
    	$approvedDate = date("Y-m-d", strtotime($materialIssuanceApprovedDate));
    	$receivedDate = date("Y-m-d", strtotime($materialIssuanceReceivedDate));

    	$rules = array(
            'materialTakeOffId'  =>'required',
            'warehouseId'=>'required',
            'materialIssuanceNo'=>'required',
            'materialIssuanceDate'=>'required',
            'materialIssuanceRequestBy'=>'required',
        );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_issuances/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else{
            $material_issuance = new MaterialIssuance;
            $material_issuance->material_take_off_id =Input::get('materialTakeOffId');
            $material_issuance->warehouse_id =Input::get('warehouseId');
            $material_issuance->material_issuance_no =Input::get('materialIssuanceNo');
            $material_issuance->material_issuance_date =$date;
            $material_issuance->material_issuance_request_by =Input::get('materialIssuanceRequestBy');
            $material_issuance->material_issuance_request_date =$requestDate;
            $material_issuance->material_issuance_approved_by =Input::get('materialIssuanceApprovedBy');
            $material_issuance->material_issuance_approved_date =$approvedDate;
            $material_issuance->material_issuance_received_by =Input::get('materialIssuanceReceivedBy');
            $material_issuance->material_issuance_received_date =$receivedDate;
            $material_issuance->save();

            for ($i=0; $i <$totalRow; $i++){ 
                $material_issuance_detail = new MaterialIssuanceDetail;
                $material_issuance_detail->material_issuance_id=$material_issuance->id;
                $material_issuance_detail->material_id=$materials[$i]["id"];
                $material_issuance_detail->quantity=$quantities[$i];
                $material_issuance_detail->remarks=$remarks[$i];
                $material_issuance_detail->created_user=Auth::user()->username;
                $material_issuance_detail->updated_user=Auth::user()->username;
                $material_issuance_detail->save();
            }

            Session::flash('message','Successfully created material issuance!');
        }
    	var_dump($totalRow);
        return "Successfully Created";
	}


	public function editWithAjax($id)
	{
		$materialIssuanceDate=Input::get('materialIssuanceDate');
		$materialIssuanceRequestDate=Input::get('materialIssuanceRequestDate');
		$materialIssuanceApprovedDate =Input::get('materialIssuanceApprovedDate');
		$materialIssuanceReceivedDate=Input::get('materialIssuanceReceivedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');

    	$date = date("Y-m-d", strtotime($materialIssuanceDate));
    	$requestDate = date("Y-m-d", strtotime($materialIssuanceRequestDate));
    	$approvedDate = date("Y-m-d", strtotime($materialIssuanceApprovedDate));
    	$receivedDate = date("Y-m-d", strtotime($materialIssuanceReceivedDate));

    	$rules = array(
            'materialTakeOffId'  =>'required',
            'warehouseId'=>'required',
            'materialIssuanceNo'=>'required',
            'materialIssuanceDate'=>'required',
            'materialIssuanceRequestBy'=>'required',
        );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_issuances/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_issuance = MaterialIssuance::find($id);
            $material_issuance->material_take_off_id =Input::get('materialTakeOffId');
            $material_issuance->warehouse_id =Input::get('warehouseId');
            $material_issuance->material_issuance_no =Input::get('materialIssuanceNo');
            $material_issuance->material_issuance_date =$date;
            $material_issuance->material_issuance_request_by =Input::get('materialIssuanceRequestBy');
            $material_issuance->material_issuance_request_date =$requestDate;
            $material_issuance->material_issuance_approved_by =Input::get('materialIssuanceApprovedBy');
            $material_issuance->material_issuance_approved_date =$approvedDate;
            $material_issuance->material_issuance_received_by =Input::get('materialIssuanceReceivedBy');
            $material_issuance->material_issuance_received_date =$receivedDate;
            $material_issuance->save();

            //material issuance detail old
            $materialIssuanceDetail=MaterialIssuanceDetail::where('material_issuance_id', '=', $material_issuance->id)->Get();

            foreach ($materialIssuanceDetail as $key => $value) {
            	$test =MaterialIssuanceDetail::find($value->id);
            	$test->delete();
            }

            for ($i=0; $i <$totalRow; $i++) { 

            	$material_issuance_detail = new MaterialIssuanceDetail;
                $material_issuance_detail->material_issuance_id=$material_issuance->id;
                $material_issuance_detail->material_id=$materials[$i]["id"];
                $material_issuance_detail->quantity=$quantities[$i];
                $material_issuance_detail->remarks=$remarks[$i];
                $material_issuance_detail->created_user=Auth::user()->username;
                $material_issuance_detail->updated_user=Auth::user()->username;
                $material_issuance_detail->save();
            }
            Session::flash('message','Successfully updated material issuance!');
        }
        return "Successfully updated material issuance!";
	}

}
