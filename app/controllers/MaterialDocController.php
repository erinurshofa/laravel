<?php

class MaterialDocController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	*/
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_docs = MaterialDoc::paginate(5);
    	$this->layout->content = View::make('material_docs.index')
    		-> with('material_docs', $material_docs);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_material = DB::table('materials')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('material_docs.create', array('populate_material' => $populate_material));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'material_id'	=>'required',
			'doc_content'	=>'required',
			'octet_type'=>'required',

		);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('material_docs/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_doc = new MaterialDoc;
			$material_doc->material_id =Input::get('material_id');
			$material_doc->doc_content =Input::get('doc_content');
			$material_doc->octet_type =Input::get('octet_type');
            $material_doc->created_user=Auth::user()->username;
            $material_doc->updated_user=Auth::user()->username;
			
			$material_doc->save();

			Session::flash('message','Successfully created material doc!');
			return Redirect::to('material_docs');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_doc =MaterialDoc::find($id);

		$this->layout->content = View::make('material_docs.show')
			->with('material_doc',$material_doc);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_doc = MaterialDoc::find($id);
		$populate_material = DB::table('materials')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('material_docs.edit', array('populate_material' => $populate_material))
			->with('material_doc',$material_doc);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'material_id'	=>'required',
			'doc_content'	=>'required',
			'octet_type'=>'required',

		);

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('material_docs/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_doc=MaterialDoc::find($id);

			$material_doc->material_id =Input::get('material_id');
			$material_doc->doc_content =Input::get('doc_content');
			$material_doc->octet_type =Input::get('octet_type');
            $material_doc->created_user=Auth::user()->username;
            $material_doc->updated_user=Auth::user()->username;

			$material_doc->save();
			Session::flash('message','Successfully updated material doc!' );
			return Redirect::to('material_docs');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_doc = MaterialDoc::find($id);
		$material_doc->delete();

		Session::flash('message','Successfully deleted the material doc');
		return Redirect::to('material_docs');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialDoc()
	{
		$q = Input::get('material_doc');
		$search='%'.$q.'%';
		$material_docs = DB::table('material_docs')
					->select(array('material_docs.id','material_docs.material_id', 'material_docs.doc_content',
								'material_docs.octet_type'))
					->where('material_docs.doc_content', 'like', $search)
					->paginate(5);
		if ($material_docs->count() > 0) {
			Session::flash('message','Successfully search material doc!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View:: make('material_docs.index')
			-> with('material_docs', $material_docs);
	}


}
