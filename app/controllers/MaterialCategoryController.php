<!-- app/controller/MaterialCategoryController.php -->
<?php

class MaterialCategoryController extends \BaseController {

	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_categories = MaterialCategory::paginate(5);
    	$this->layout->content = View::make('material_categories.index')
    		-> with('material_categories', $material_categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('material_categories.create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('material_categories/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_category = new MaterialCategory;
			$material_category->code =Input::get('code');
			$material_category->name =Input::get('name');
			$material_category->description =Input::get('description');
			$material_category->save();

			Session::flash('message','Successfully created material category!');
			return Redirect::to('material_categories');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_category =MaterialCategory::find($id);

		$this->layout->content = View::make('material_categories.show')
			->with('material_category',$material_category);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_category = MaterialCategory::find($id);

		$this->layout->content = View::make('material_categories.edit')
			->with('material_category',$material_category);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules=array(
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
		);	

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('material_categories/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_category=MaterialCategory::find($id);

			$material_category->code =Input::get('code');
			$material_category->name =Input::get('name');
			$material_category->description =Input::get('description');

			$material_category->save();

			Session::flash('message','Successfully updated material category!');
			return Redirect::to('material_categories');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material =DB::table('materials')->where('materials.material_category_id', '=', $id);
		if ($material->count() == 0) {
			$material_category = MaterialCategory::find($id);
			$material_category->delete();
			Session::flash('message','Successfully deleted the material category!');
		}else{
			Session::flash('warning','Material Category cannot be delete because still being used in material!');
		}
		return Redirect::to('material_categories');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialCategory()
	{
		$q = Input::get('material_category');
		$search='%'.$q.'%';
		$material_categories = DB::table('material_categories')
					->select(array('material_categories.id','material_categories.code','material_categories.name', 'material_categories.description'))
					->where('material_categories.name', 'like', $search)
					->paginate(5);
		if ($material_categories->count() > 0) {
			Session::flash('message','Successfully search material category!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('material_categories.index')
			-> with('material_categories', $material_categories);
	}

}
