<?php

class UserController extends \BaseController {

	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();

		$this->layout->content= View::make('users.index')
			->with('users', $users);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user=User::find($id);
		$this->layout->content = View::make('users.show')
			->with('user',$user);
	}

	public function searchUser()
	{
		$q = Input::get('query');
		$search='%'.$q.'%';
		$users = DB::select('select * from users where username like ?', array($search));
		Session::flash('message','Search' );
		$this->layout->content= View::make('users.index', compact('users'));
	}

}
