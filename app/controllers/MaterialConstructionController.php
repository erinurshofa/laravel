<?php

class MaterialConstructionController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_constructions = MaterialConstruction::paginate(5);
    	$this->layout->content = View::make('transactions.material_constructions.index')
    		-> with('material_constructions', $material_constructions);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_material_issuance = DB::table('material_issuances')->orderBy('material_issuance_no', 'asc')->lists('material_issuance_no','id');
		$populate_material = DB::table('materials')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('transactions.material_constructions.create', array('populate_material_issuance' => $populate_material_issuance,'populate_material' => $populate_material));
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_construction =MaterialConstruction::find($id);

		$this->layout->content = View::make('transactions.material_constructions.show')
			->with('material_construction',$material_construction);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_construction_details = MaterialConstructionDetail::where('material_construction_id', '=', $id)->get();
		$material_construction = MaterialConstruction::find($id);

		$populate_material_issuance = DB::table('material_issuances')->orderBy('material_issuance_no', 'asc')->lists('material_issuance_no','id');
		$populate = array();
		foreach ($material_construction_details as $key => $value) {
			$test = Material::find($value->material_id);
			if ($test) {
				if ($test->count()!=0) {
				$populate[] = $test;
				}
			}

		}
		$this->layout->content = 
					View::make('transactions.material_constructions.edit', 
						array('populate_material_issuance' => $populate_material_issuance, 
							'populate_material'=>$populate))
			->with('material_construction',$material_construction)
			->with('material_construction_details',$material_construction_details);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_construction_detail =DB::table('material_construction_details')->where('material_construction_details.material_construction_id', '=', $id);
		if ($material_construction_detail->count() == 0) {
			$material_construction = MaterialConstruction::find($id);
			$material_construction->delete();

			Session::flash('message','Successfully deleted the material construction');
		}else{

			Session::flash('warning','Material construction still used in material construction details');
		}
		return Redirect::to('material_constructions');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialConstruction()
	{
		$q = Input::get('material_construction');
		$search='%'.$q.'%';
		$material_constructions = DB::table('material_constructions')
					->select(array('material_constructions.id','material_constructions.material_issuance_id', 'material_constructions.remarks'))
					->where('material_constructions.remarks', 'like', $search)
					->paginate(5);
		if ($material_constructions->count() > 0) {
			Session::flash('message','Successfully search material construction!');
		}else{
			Session::flash('warning','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.material_constructions.index')
			-> with('material_constructions', $material_constructions);
	}
	/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}
	/**
	 * listMaterialConstructionDetail digunakan pada edit view material construction.
	 *
	 * @return Response
	 */
	public function listMaterialConstructionDetail($material_construction_id)
	{
		$materialConstructionDetail=MaterialConstructionDetail::where('material_construction_id', '=', $material_construction_id)->Get();
        return Response::json($materialConstructionDetail);
	}
	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($material_construction_id)
	{
		$materialConstructionDetail=MaterialConstructionDetail::where('material_construction_id', '=', $material_construction_id)->Get();
    
    	$materials=array();
    	foreach ($materialConstructionDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');


    	$rules = array(
            'materialIssuanceId'  =>'required',
            'remarks'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_constructions/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_construction = new MaterialConstruction;
            $material_construction->material_issuance_id =Input::get('materialIssuanceId');
            $material_construction->remarks =Input::get('materialConstructionRemarks');
            $material_construction->created_user=Auth::user()->username;
            $material_construction->updated_user=Auth::user()->username;
            $material_construction->save();

            
            for ($i=0; $i <$totalRow; $i++) { 
                $material_construction_detail = new MaterialConstructionDetail;
                $material_construction_detail->material_construction_id=$material_construction->id;
                $material_construction_detail->material_id=$materials[$i]["id"];
                $material_construction_detail->quantity=$quantities[$i];
                $material_construction_detail->remarks=$remarks[$i];
                $material_construction_detail->created_user=Auth::user()->username;
                $material_construction_detail->updated_user=Auth::user()->username;
                $material_construction_detail->save();
            }
            Session::flash('message','Successfully created material construction!');
        }
    	var_dump(Input::get('materialIssuanceRemarks'));
        return "Successfully Created";
	}


	public function editWithAjax($id)
	{
		// $mtoDate=Input::get('mtoDate');
		// $mtoRequestDate =Input::get('mtoRequestDate');
		// $mtoApprovedDate=Input::get('mtoApprovedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$materiaConstructionld = Input::get('materialConstructionId');
    	// $date = date("Y-m-d", strtotime($mtoDate));
    	// $requestDate = date("Y-m-d", strtotime($mtoRequestDate));
    	// $approvedDate = date("Y-m-d", strtotime($mtoApprovedDate));
    	$rules = array(
            'materialIssuanceId'  =>'required',
            'remarks'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_constructions/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_construction = MaterialConstruction::find($id);
            $material_construction->material_issuance_id =Input::get('materialIssuanceId');
            $material_construction->remarks =Input::get('materialConstructionRemarks');
            $material_construction->created_user=Auth::user()->username;
            $material_construction->updated_user=Auth::user()->username;
            $material_construction->save();

            //material construction detail old
            $materialConstructionDetail=MaterialConstructionDetail::where('material_construction_id', '=', $material_construction->id)->Get();

            foreach ($materialConstructionDetail as $key => $value) {
            	$test =MaterialConstructionDetail::find($value->id);
            	$test->delete();
            }

            for ($i=0; $i <$totalRow; $i++) { 

            	$material_construction_detail = new MaterialConstructionDetail;
                $material_construction_detail->material_construction_id=$material_construction->id;
                $material_construction_detail->material_id=$materials[$i]["id"];
                $material_construction_detail->quantity=$quantities[$i];
                $material_construction_detail->remarks=$remarks[$i];
                $material_construction_detail->created_user=Auth::user()->username;
                $material_construction_detail->updated_user=Auth::user()->username;
                $material_construction_detail->save();
            }
            Session::flash('message','Successfully updated material construction!');
        }
        return "Successfully updated material construction!";
	}

}
