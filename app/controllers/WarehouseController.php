<!-- app/controller/WarehouseController.php -->
<?php

class WarehouseController extends \BaseController {

	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$warehouses = Warehouse::paginate(5);
    	$this->layout->content = View::make('warehouses.index')
    		-> with('warehouses', $warehouses);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('warehouses.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'code'	=>'required',
			'description'=>'required',
			'address'=>'required',
			'phone'=>'required',
			'administrator'=>'required'
			);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('warehouses/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$warehouse = new Warehouse;
			$warehouse->code =Input::get('code');
			$warehouse->description =Input::get('description');
			$warehouse->address =Input::get('address');
			$warehouse->phone =Input::get('phone');
			$warehouse->administrator =Input::get('administrator');
			$warehouse->save();

			Session::flash('message','Successfully created warehouse!');
			return Redirect::to('warehouses');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$warehouse =Warehouse::find($id);

		$this->layout->content = View::make('warehouses.show')
			->with('warehouse',$warehouse);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$warehouse = Warehouse::find($id);

		$this->layout->content = View::make('warehouses.edit')
			->with('warehouse',$warehouse);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules=array(
			'code'	=>'required',
			'description'=>'required',
			'address'=>'required',
			'phone'=>'required',
			'administrator'=>'required'
		);	

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('warehouses/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$warehouse=Warehouse::find($id);

			$warehouse->code =Input::get('code');
			$warehouse->description =Input::get('description');
			$warehouse->address =Input::get('address');
			$warehouse->phone =Input::get('phone');
			$warehouse->administrator =Input::get('administrator');
			$warehouse->save();

			Session::flash('message','Successfully updated warehouse!' );
			return Redirect::to('warehouses');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_receiving =DB::table('material_receivings')->where('material_receivings.warehouse_id', '=', $id);
		$material_issuance =DB::table('material_issuances')->where('material_issuances.warehouse_id', '=', $id);
		if ($material_issuance->count()==0 && $material_receiving->count()== 0){
			$warehouse = Warehouse::find($id);
			$warehouse->delete();
			Session::flash('message','Successfully deleted the warehouse');
			return Redirect::to('warehouses');
		}if($material_issuance->count() != 0){
			Session::flash('warning','Material take off still used in material issuance');
			return Redirect::to('warehouses');
		}if($material_receiving->count() != 0){
			Session::flash('warning','Material take off still used in material receiving');
			return Redirect::to('warehouses');
		}
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchWarehouse()
	{
		$q = Input::get('warehouse');
		$search='%'.$q.'%';
		$warehouses = DB::table('warehouses')
					->select(array('warehouses.id','warehouses.code', 'warehouses.description', 'warehouses.address', 'warehouses.phone', 'warehouses.administrator'))
					->where('warehouses.description', 'like', $search)
					->paginate(5);
		if ($warehouses->count() > 0) {
			Session::flash('message','Successfully search warehouse!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('warehouses.index')
			-> with('warehouses', $warehouses);
	}

}
