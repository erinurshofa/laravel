<?php

class HomeController extends BaseController {

	public $layout = 'layout';
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function showRegister()
	{
		//show the form
		$this->layout->content = View::make('register');
	}

	public function dashboard()
	{
		$this->layout->content = View::make('dashboard');
	}

	public function doRegister()
	{
	// validate the info, create rules for the inputs
		$rules = array(
			'username'=>'required',
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			Session::flash('message','Register Failed!');
			$this->layout->content = Redirect::to('register')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$user = new User;
			$user->username =Input::get('username');
			$user->email =Input::get('email');
			$user->password =Hash::make(Input::get('password'));
			$user->save();

			Session::flash('message','You are already registered!, Please login!');
			$this->layout->content = Redirect::intended('dashboard');
		}
	}


	public function showLogin()
	{
		// $id = $user->getAuthIdentifier();

  //    $this->session->put($this->getName(), $id);

  //    // If the user should be permanently "remembered" by the application we will
  //    // queue a permanent cookie that contains the encrypted copy of the user
  //    // identifier. We will then decrypt this later to retrieve the users.
  //    if ($remember)
  //    {
  //        $this->queuedCookies[] = $this->createRecaller($id);
  //    }

  //    // If we have an event dispatcher instance set we will fire an event so that
  //    // any listeners will hook into the authentication events and run actions
  //    // based on the login and logout events fired from the guard instances.
  //    if (isset($this->events))
  //    {
  //        $this->events->fire('auth.login', array($user, $remember));
  //    }

  //    $this->setUser($user);
		//show the form
		$this->layout->content = View::make('login');
	}

	public function doLogin()
	{

		// validate the info, create rules for the inputs
		$rules = array(
			'username'=>'required',
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		}else{

			// create our user data for the authentication
			$userdata = array(
				'username'=> Input::get('username'),
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password'),
			);
			// attempt to do the login
			if(Auth::attempt($userdata)){
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				Session::flash('message','Successfully Login!');
				return Redirect::to('dashboard');

			}else{	 	

				// validation not successful, send back to form	
				Session::flash('message','Login Failed!');
				return Redirect::to('login');
			}

		}
	}

	// app/controllers/HomeController.php
	public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		Session::flash('message','Successfully log out!');
		return Redirect::to('login'); // redirect the user to the login screen
	}

}