<?php

class MaterialController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$materials = Material::paginate(5);
    	$this->layout->content = View::make('materials.index')
    		-> with('materials', $materials);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_material_category = DB::table('material_categories')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('materials.create', array('populate_material_category' => $populate_material_category));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'material_category_id'	=>'required',
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			'unit'=>'required',
		);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('materials/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material = new Material;
			$material->material_category_id =Input::get('material_category_id');
			$material->code =Input::get('code');
			$material->name =Input::get('name');
			$material->description =Input::get('description');
			$material->unit =Input::get('unit');
			// $material->created_user =Input::get('created_user');
			// $material->updated_user =Input::get('updated_user');
			$material->save();

			Session::flash('message','Successfully created material!');
			return Redirect::to('materials');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material =Material::find($id);

		$this->layout->content = View::make('materials.show')
			->with('material',$material);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material = Material::find($id);
		$populate_material_category = DB::table('material_categories')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('materials.edit', array('populate_material_category' => $populate_material_category))
			->with('material',$material);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'material_category_id'	=>'required',
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			'unit'=>'required',
		);

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('materials/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material=Material::find($id);

			$material->material_category_id =Input::get('material_category_id');
			$material->code =Input::get('code');
			$material->name =Input::get('name');
			$material->description =Input::get('description');
			$material->unit =Input::get('unit');
			// $material->created_user =Input::get('created_user');
			// $material->updated_user =Input::get('updated_user');
			
			$material->save();
			Session::flash('message','Successfully updated material!' );
			return Redirect::to('materials');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_doc =DB::table('material_docs')->where('material_docs.material_id', '=', $id);
		$material_issuance_detail =DB::table('material_issuance_details')->where('material_issuance_details.material_id', '=', $id);
		$purchase_order_detail =DB::table('purchase_order_details')->where('purchase_order_details.material_id', '=', $id);
		$material_receiving_detail=DB::table('material_receiving_details')->where('material_receiving_details.material_id', '=', $id);
		$material_take_off_detail=DB::table('material_take_off_details')->where('material_take_off_details.material_id', '=', $id);
		$material_construction_detail=DB::table('material_construction_details')->where('material_construction_details.material_id', '=', $id);
		$nonconformance_detail=DB::table('nonconformance_details')->where('nonconformance_details.material_id', '=', $id);

		if ($material_doc->count()==0 && $material_issuance_detail->count()== 0 && $purchase_order_detail->count()== 0
			&& $material_receiving_detail->count()== 0&& $material_take_off_detail->count()== 0&& $material_construction_detail->count()== 0
			&& $nonconformance_detail->count()== 0){
			$material = Material::find($id);
			$material->delete();
			Session::flash('message','Successfully deleted the material');
		}if($material_issuance_detail->count() != 0){
			Session::flash('warning','Material still used in material issuance detail');
		}if($purchase_order_detail->count() != 0){
			Session::flash('warning','Material still used in purchase order');
		}if($material_receiving_detail->count() != 0){
			Session::flash('warning','Material still used in material receiving');
		}if($material_take_off_detail->count() != 0){
			Session::flash('warning','Material still used in material take off');
		}if($material_construction_detail->count() != 0){
			Session::flash('warning','Material still used in material construction');
		}
		return Redirect::to('materials');
	}
	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterial()
	{
		$q = Input::get('material');
		$search='%'.$q.'%';
		$materials = DB::table('materials')
					->select(array('materials.id','materials.material_category_id', 'materials.code',
								'materials.name', 'materials.description', 
								'materials.unit'))
					->where('materials.name', 'like', $search)
					->paginate(5);
		if ($materials->count() > 0) {
			Session::flash('message','Successfully search material!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('materials.index')
			-> with('materials', $materials);
	}


}
