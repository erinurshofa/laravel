<?php

class MaterialTakeOffController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_take_offs = MaterialTakeOff::paginate(5);
    	$this->layout->content = View::make('transactions.material_take_offs.index')
    		-> with('material_take_offs', $material_take_offs);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_project = DB::table('projects')->orderBy('name', 'asc')->lists('name','id');
		$populate_material = DB::table('materials')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('transactions.material_take_offs.create', array('populate_project' => $populate_project,'populate_material' => $populate_material));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'project_id'	=>'required',
			'mto_no'	=>'required',
			'mto_revision'=>'required',
			'mto_date'=>'required',
			'mto_description'=>'required',
			'mto_request_by'=>'required',
			'mto_request_date'=>'required',
			'mto_approved_by'=>'required',
			'mto_approved_date'=>'required',
			
		);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('material_take_offs/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_take_off = new MaterialTakeOff;
			$material_take_off->project_id =Input::get('project_id');
			$material_take_off->mto_no =Input::get('mto_no');
			$material_take_off->mto_revision =Input::get('mto_revision');
			$material_take_off->mto_date =Input::get('mto_date');
			$material_take_off->mto_description =Input::get('mto_description');
			$material_take_off->mto_request_by =Input::get('mto_request_by');
			$material_take_off->mto_request_date =Input::get('mto_request_date');
			$material_take_off->mto_approved_by =Input::get('mto_approved_by');
			$material_take_off->mto_approved_date =Input::get('mto_approved_date');
			
			$material_take_off->save();

			$data =array();
			$totalRow =Input::get('totalRow');
			// $material_take_off_detail = array();
			for ($i=0; $i < $totalRow ; $i++) {
				$data = array(
    				array(
    					'material_take_off_id'=>$material_take_off->id,
    					'material_id'=>Input::get('material_id'.$totalRow),
    					'quantity'=>Input::get('quantity'.$totalRow),
    					'remarks'=>Input::get('remarks'.$totalRow),
    				)
				); 
				MaterialTakeOffDetail::insert($data);
			}
			Session::flash('message','Successfully created material take off!');
			return Redirect::to('material_take_offs');
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_take_off =MaterialTakeOff::find($id);

		$this->layout->content = View::make('transactions.material_take_offs.show')
			->with('material_take_off',$material_take_off);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_take_off_details = MaterialTakeOffDetail::where('material_take_off_id', '=', $id)->get();
		$material_take_off = MaterialTakeOff::find($id);

		$populate_project = DB::table('projects')->orderBy('name', 'asc')->lists('name','id');
		$populate = array();
		foreach ($material_take_off_details as $key => $value) {
			$test = Material::find($value->material_id);
			if ($test) {
				if ($test->count()!=0) {
				$populate[] = $test;
				}
			}

		}
		$this->layout->content = 
					View::make('transactions.material_take_offs.edit', 
						array('populate_project' => $populate_project, 
							'populate_material'=>$populate))
			->with('material_take_off',$material_take_off)
			->with('material_take_off_details',$material_take_off_details);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'project_id'	=>'required',
			'mto_no'	=>'required',
			'mto_revision'=>'required',
			'mto_date'=>'required',
			'mto_description'=>'required',
			'mto_request_by'=>'required',
			'mto_request_date'=>'required',
			'mto_approved_by'=>'required',
			'mto_approved_date'=>'required',
		);

		$mtoDate=Input::get('mto_date');
		$mtoRequestDate =Input::get('mto_request_date');
		$mtoApprovedDate=Input::get('mto_approved_date');

    	$date = date("Y-m-d", strtotime($mtoDate));
    	$requestDate = date("Y-m-d", strtotime($mtoRequestDate));
    	$approvedDate = date("Y-m-d", strtotime($mtoApprovedDate));

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('material_take_offs/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$material_take_off=MaterialTakeOff::find($id);

			$material_take_off->project_id =Input::get('project_id');
			$material_take_off->mto_no =Input::get('mto_no');
			$material_take_off->mto_revision =Input::get('mto_revision');
			$material_take_off->mto_date =$date;
			$material_take_off->mto_description =Input::get('mto_description');
			$material_take_off->mto_request_by =Input::get('mto_request_by');
			$material_take_off->mto_request_date =$requestDate;
			$material_take_off->mto_approved_by =Input::get('mto_approved_by');
			$material_take_off->mto_approved_date =$approvedDate;
			
			$material_take_off->save();			

			Session::flash('message','Successfully updated material take off!' );
			var_dump($date);
			var_dump($mtoDate);
			return Redirect::to('material_take_offs');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_take_off_detail =DB::table('material_take_off_details')->where('material_take_off_details.material_take_off_id', '=', $id);
		$material_issuance =DB::table('material_issuances')->where('material_issuances.material_take_off_id', '=', $id);
		$purchase_order =DB::table('purchase_orders')->where('purchase_orders.material_take_off_id', '=', $id);
		if ($material_take_off_detail->count()==0 && $material_issuance->count()== 0 && $purchase_order->count()== 0){
			$material_take_off = MaterialTakeOff::find($id);
			$material_take_off->delete();
			Session::flash('message','Successfully deleted the material take off');
		}if($material_take_off_detail->count() != 0){
			Session::flash('warning','Material take off still used in material take off detail');
		}if($purchase_order->count() != 0){
			Session::flash('warning','Material take off still used in purchase order');
		}if($material_issuance->count() != 0){
			Session::flash('warning','Material take off still used in material issuance');
		}
		return Redirect::to('material_take_offs');
		
	}
	 /**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialTakeOff()
	{
		$q = Input::get('material_take_off');
		$search='%'.$q.'%';
		$material_take_offs = DB::table('material_take_offs')
					->select(array('material_take_offs.id','material_take_offs.project_id', 'material_take_offs.mto_no',
								'material_take_offs.mto_revision', 'material_take_offs.mto_date', 
								'material_take_offs.mto_description','material_take_offs.mto_request_by',
								'material_take_offs.mto_request_date','material_take_offs.mto_approved_by','material_take_offs.mto_approved_date'))
					->where('material_take_offs.mto_no', 'like', $search)
					->paginate(5);
		if ($material_take_offs->count() > 0) {
			Session::flash('message','Successfully search material take off!' );
		}else{
			Session::flash('warning','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.material_take_offs.index')
			-> with('material_take_offs', $material_take_offs);
	}
	/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}

	/**
	 * listMaterialTakeOffDetail digunakan pada edit view material take off.
	 *
	 * @return Response
	 */
	public function listMaterialTakeOffDetail($material_take_off_id)
	{
		$materialTakeOffDetail=MaterialTakeOffDetail::where('material_take_off_id', '=', $material_take_off_id)->Get();
        return Response::json($materialTakeOffDetail);
	}

	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($material_take_off_id)
	{
		$materialTakeOffDetail=MaterialTakeOffDetail::where('material_take_off_id', '=', $material_take_off_id)->Get();
    
    	$materials=array();
    	foreach ($materialTakeOffDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$mtoDate=Input::get('mtoDate');
		$mtoRequestDate =Input::get('mtoRequestDate');
		$mtoApprovedDate=Input::get('mtoApprovedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');

    	$date = date("Y-m-d", strtotime($mtoDate));
    	$requestDate = date("Y-m-d", strtotime($mtoRequestDate));
    	$approvedDate = date("Y-m-d", strtotime($mtoApprovedDate));

    	$rules = array(
            'projectId'  =>'required',
            'mtoNo'=>'required',
            'mtoRevision'=>'required',
            'mtoDate'=>'required',
            'mtoDescription'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_take_offs/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_take_off = new MaterialTakeOff;
            $material_take_off->project_id =Input::get('projectId');
            $material_take_off->mto_no =Input::get('mtoNo');
            $material_take_off->mto_revision =Input::get('mtoRevision');
            $material_take_off->mto_date =$date;
            $material_take_off->mto_description =Input::get('mtoDescription');
            $material_take_off->mto_request_by =Input::get('mtoRequestBy');
            $material_take_off->mto_request_date =$requestDate;
            $material_take_off->mto_approved_by =Input::get('mtoApprovedBy');
            $material_take_off->mto_approved_date =$approvedDate;
            $material_take_off->save();

            
            for ($i=0; $i <$totalRow; $i++) { 
                $material_take_off_detail = new MaterialTakeOffDetail;
                $material_take_off_detail->material_take_off_id=$material_take_off->id;
                $material_take_off_detail->material_id=$materials[$i]["id"];
                $material_take_off_detail->quantity=$quantities[$i];
                $material_take_off_detail->remarks=$remarks[$i];
                $material_take_off_detail->created_user=Auth::user()->username;
                $material_take_off_detail->updated_user=Auth::user()->username;
                $material_take_off_detail->save();
            }

            Session::flash('message','Successfully created material take off!');
        }
        return "Successfully Created".Input::get('mtoRequestDate');
	}


	public function editWithAjax($id)
	{
		$mtoDate=Input::get('mtoDate');
		$mtoRequestDate =Input::get('mtoRequestDate');
		$mtoApprovedDate=Input::get('mtoApprovedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$materiaTakeOffld = Input::get('materialTakeOffId');

    	$date = date("Y-m-d", strtotime($mtoDate));
    	$requestDate = date("Y-m-d", strtotime($mtoRequestDate));
    	$approvedDate = date("Y-m-d", strtotime($mtoApprovedDate));

    	$rules = array(
            'projectId'  =>'required',
            'mtoNo'=>'required',
            'mtoRevision'=>'required',
            'mtoDate'=>'required',
            'mtoDescription'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('material_take_offs/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_take_off = MaterialTakeOff::find($id);

            $material_take_off->project_id =Input::get('projectId');
            $material_take_off->mto_no =Input::get('mtoNo');
            $material_take_off->mto_revision =Input::get('mtoRevision');
            $material_take_off->mto_date =$date;
            $material_take_off->mto_description =Input::get('mtoDescription');
            $material_take_off->mto_request_by =Input::get('mtoRequestBy');
            $material_take_off->mto_request_date =$requestDate;
            $material_take_off->mto_approved_by =Input::get('mtoApprovedBy');
            $material_take_off->mto_approved_date =$approvedDate;
            $material_take_off->save();

            //material take off detail old
            $materialTakeOffDetail=MaterialTakeOffDetail::where('material_take_off_id', '=', $material_take_off->id)->Get();

            foreach ($materialTakeOffDetail as $key => $value) {
            	$test =MaterialTakeOffDetail::find($value->id);
            	$test->delete();
            }

            for ($i=0; $i <$totalRow; $i++) { 

            	$material_take_off_detail = new MaterialTakeOffDetail;
                $material_take_off_detail->material_take_off_id=$material_take_off->id;
                $material_take_off_detail->material_id=$materials[$i]["id"];
                $material_take_off_detail->quantity=$quantities[$i];
                $material_take_off_detail->remarks=$remarks[$i];
                $material_take_off_detail->created_user=Auth::user()->username;
                $material_take_off_detail->updated_user=Auth::user()->username;
                $material_take_off_detail->save();
            }
            Session::flash('message','Successfully updated material take off!');
        }
        return "'Successfully updated material take off!'".Input::get('mtoRequestDate');
	}

}
