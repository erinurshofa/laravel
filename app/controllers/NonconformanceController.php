<?php

class NonconformanceController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$nonconformances = Nonconformance::paginate(5);
    	$this->layout->content = View::make('transactions.nonconformances.index')
    		-> with('nonconformances', $nonconformances);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_purchase_order = DB::table('purchase_orders')->orderBy('po_no', 'asc')->lists('po_no','id');

		$this->layout->content = View::make('transactions.nonconformances.create', array('populate_purchase_order' => $populate_purchase_order));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$nonconformance =Nonconformance::find($id);

		$this->layout->content = View::make('transactions.nonconformances.show')
			->with('nonconformance',$nonconformance);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$nonconformance_details = NonconformanceDetail::where('nonconformance_id', '=', $id)->get();
		$nonconformance = Nonconformance::find($id);

		$populate_purchase_order = DB::table('purchase_orders')->orderBy('po_no', 'asc')->lists('po_no','id');
		$populate = array();
		foreach ($nonconformance_details as $key => $value) {
			$test = Material::find($value->material_id);
			if ($test) {
				if ($test->count()!=0) {
				$populate[] = $test;
				}
			}

		}
		$this->layout->content = 
					View::make('transactions.nonconformances.edit', 
						array('populate_purchase_order' => $populate_purchase_order, 
							'populate_material'=>$populate))
			->with('nonconformance',$nonconformance)
			->with('nonconformance_details',$nonconformance_details);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$nonconformance_detail =DB::table('nonconformance_details')->where('nonconformance_details.nonconformance_id', '=', $id);
		if ($nonconformance_detail->count() == 0) {
			$nonconformance = Nonconformance::find($id);
			$nonconformance->delete();

			Session::flash('message','Successfully deleted the nonconformance');
		}else{

			Session::flash('warning','Nonconformance still used in nonconformance details');
		}
		return Redirect::to('nonconformances');
	}

		/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchNonconformance()
	{
		$q = Input::get('nonconformance');
		$search='%'.$q.'%';
		$nonconformances = DB::table('nonconformances')
					->select(array('nonconformances.id','nonconformances.project_id', 'nonconformances.mto_no',
								'nonconformances.mto_request_date','conformances.mto_approved_by','nonconformances.mto_approved_date'))
					->where('nonconformances.mto_no', 'like', $search)
					->paginate(5);
		if ($nonconformances->count() > 0) {
			Session::flash('message','Successfully search nonconformance!' );
		}else{
			Session::flash('warning','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.nonconformances.index')
			-> with('nonconformances', $nonconformances);
	}
	/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}

	/**
	 * listNonconformaneDetail digunakan pada edit view nonconformance.
	 *
	 * @return Response
	 */
	public function listNonconformanceDetail($nonconformance_id)
	{
		$nonconformanceDetail=NonconformanceDetail::where('nonconformance_id', '=', $nonconformance_id)->Get();
        return Response::json($nonconformanceDetail);
	}

	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($nonconformance_id)
	{
		$nonconformanceDetail=NonconformanceDetail::where('nonconformance_id', '=', $nonconformance_id)->Get();
    
    	$materials=array();
    	foreach ($nonconformanceDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$nonconformanceDate=Input::get('nonconformanceDate');
		$totalRow =Input::get('totalRow'	);
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');

    	$date = date("Y-m-d", strtotime($nonconformanceDate));

    	$rules = array(
            'purchaseOrderId'  =>'required',
            'nonconformanceCode'=>'required',
            'nonconformanceDate'=>'required',
            'nonconformanceDescription'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('nonconformances/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $nonconformance = new Nonconformance;
            $nonconformance->purchase_order_id =Input::get('purchaseOrderId');
            $nonconformance->nonconformance_code =Input::get('nonconformanceCode');
            $nonconformance->nonconformance_date =$date;
            $nonconformance->nonconformance_description =Input::get('nonconformanceDescription');
            // $nonconformance->created_at =Input::get('nonconformanceDescription');
            $nonconformance->created_user =Auth::user()->username;
            $nonconformance->updated_user =Auth::user()->username;
            $nonconformance->save();

            
            for ($i=0; $i <$totalRow; $i++) { 
                $nonconformance_detail = new NonconformanceDetail;
                $nonconformance_detail->nonconformance_id=$nonconformance->id;
                $nonconformance_detail->material_id=$materials[$i]["id"];
                $nonconformance_detail->quantity=$quantities[$i];
                $nonconformance_detail->remarks=$remarks[$i];
                $nonconformance_detail->created_user=Auth::user()->username;
                $nonconformance_detail->updated_user=Auth::user()->username;
                $nonconformance_detail->save();
            }

            Session::flash('message','Successfully created nonconformance!');
        }
        return "Successfully Created";
	}


	public function editWithAjax($id)
	{
		$nonconformanceDate=Input::get('nonconformanceDate');
		$totalRow =Input::get('totalRow'	);
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');

    	$date = date("Y-m-d", strtotime($nonconformanceDate));

    	$rules = array(
            'purchaseOrderId'  =>'required',
            'nonconformanceCode'=>'required',
            'nonconformanceDate'=>'required',
            'nonconformanceDescription'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('nonconformances/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $nonconformance = Nonconformance::find($id);

            $nonconformance->purchase_order_id =Input::get('purchaseOrderId');
            $nonconformance->nonconformance_code =Input::get('nonconformanceCode');
            $nonconformance->nonconformance_date =$date;
            $nonconformance->nonconformance_description =Input::get('nonconformanceDescription');
            // $nonconformance->created_at =Input::get('nonconformanceDescription');
            $nonconformance->created_user =Auth::user()->username;
            $nonconformance->updated_user =Auth::user()->username;
            $nonconformance->save();

            //nonconformance detail old
            $nonconformanceDetail=NonconformanceDetail::where('nonconformance_id', '=', $nonconformance->id)->Get();

            foreach ($nonconformanceDetail as $key => $value) {
            	$test =NonconformanceDetail::find($value->id);
            	$test->delete();
            }

            for ($i=0; $i <$totalRow; $i++) { 

            	$nonconformance_detail = new NonconformanceDetail;
                $nonconformance_detail->nonconformance_id=$nonconformance->id;
                $nonconformance_detail->material_id=$materials[$i]["id"];
                $nonconformance_detail->quantity=$quantities[$i];
                $nonconformance_detail->remarks=$remarks[$i];
                $nonconformance_detail->created_user=Auth::user()->username;
                $nonconformance_detail->updated_user=Auth::user()->username;
                $nonconformance_detail->save();
            }
            Session::flash('message','Successfully updated nonconformance!');
        }
        return "Successfully updated nonconformance!";
	}

}
