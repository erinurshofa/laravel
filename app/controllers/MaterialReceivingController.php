<?php

class MaterialReceivingController extends \BaseController {


	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$material_receivings = MaterialReceiving::paginate(5);
    	$this->layout->content = View::make('transactions.material_receivings.index')
    		-> with('material_receivings', $material_receivings);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_purchase_order = DB::table('purchase_orders')->orderBy('po_description', 'asc')->lists('po_description','id');
		$populate_warehouse = DB::table('warehouses')->orderBy('description', 'asc')->lists('description','id');
		$this->layout->content = View::make('transactions.material_receivings.create', array('populate_warehouse' => $populate_warehouse,'populate_purchase_order' => $populate_purchase_order));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$material_receiving =MaterialReceiving::find($id);

		$this->layout->content = View::make('transactions.material_receivings.show')
			->with('material_receiving',$material_receiving);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material_receiving = MaterialReceiving::find($id);
		$populate_purchase_order = DB::table('purchase_orders')->orderBy('po_description', 'asc')->lists('po_description','id');
		$populate_warehouse = DB::table('warehouses')->orderBy('description', 'asc')->lists('description','id');
		$this->layout->content = View::make('transactions.material_receivings.edit', 
			array('populate_warehouse' => $populate_warehouse),
			array('populate_purchase_order' => $populate_purchase_order))
			->with('material_receiving',$material_receiving);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_receiving = MaterialReceiving::find($id);
		$material_receiving->delete();

		Session::flash('message','Successfully deleted the material receiving');
		return Redirect::to('material_receivings');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchMaterialReceiving()
	{
		$q = Input::get('material_receiving');
		$search='%'.$q.'%';
		$material_receivings = DB::table('material_receivings')
					->select(array('material_receivings.id','material_receivings.purchase_order_id', 'material_receivings.warehouse_id',
								'material_receivings.material_receiving_no', 'material_receivings.material_receiving_plbl', 
								'material_receivings.material_receiving_shipment','material_receivings.material_receiving_date',
								'material_receivings.material_receiving_unpacked_date','material_receivings.material_receiving_checked_by',
								'material_receivings.material_receiving_checked_date','material_receivings.material_receiving_approved_by',
								'material_receivings.material_receiving_approved_date'))
					->where('material_receivings.material_receiving_no', 'like', $search)
					->paginate(5);
		if ($material_receivings->count() > 0) {
			Session::flash('message','Successfully search material receiving!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.material_receivings.index')
			-> with('material_receivings', $material_receivings);
	}

		/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}
	/**
	 * listPurchaseOrderDetail digunakan pada edit view purchase order.
	 *
	 * @return Response
	 */
	public function listMaterialReceivingDetail($material_receiving_id)
	{
		$materialReceivingDetail=MaterialReceivingDetail::where('material_receiving_id', '=', $material_receiving_id)->Get();
        return Response::json($materialReceivingDetail);
	}
	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($material_receiving_id)
	{
		$materialReceivingDetail=MaterialReceivingDetail::where('material_receiving_id', '=', $material_receiving_id)->Get();
    	$materials=array();
    	foreach ($materialReceivingDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$purchaseOrderId=Input::get('purchaseOrderId');
		$warehouseId =Input::get('warehouseId');
		$materialReceivingNo=Input::get('materialReceivingNo');
		$materialReceivingPlbl=Input::get('materialReceivingPlbl');
		$materialReceivingShipment=Input::get('materialReceivingShipment');
		$materialReceivingDate=Input::get('materialReceivingDate');
		$materialReceivingUnpackedDate=Input::get('materialReceivingUnpackedDate');
		$materialReceivingCheckedBy=Input::get('materialReceivingCheckedBy');
		$materialReceivingCheckedDate=Input::get('materialReceivingCheckedDate');
		$materialReceivingApprovedBy=Input::get('materialReceivingApprovedBy');
		$materialReceivingApprovedDate=Input::get('materialReceivingApprovedDate');

		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$date = date("Y-m-d", strtotime($materialReceivingDate));
    	$unpackedDate = date("Y-m-d", strtotime($materialReceivingUnpackedDate));
    	$checkedDate = date("Y-m-d", strtotime($materialReceivingUnpackedDate));
    	$approvedDate = date("Y-m-d", strtotime($materialReceivingApprovedDate));
    	$rules = array(
            'purchaseOrderId'  =>'required',
            'warehouseId'=>'required',
            'materialReceivingNo'=>'required',
            'materialReceivingPlbl'=>'required',
            'materialReceivingShipment'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::to('material_receivings/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_receiving = new MaterialReceiving;
            $material_receiving->purchase_order_id =Input::get('purchaseOrderId');
            $material_receiving->warehouse_id =Input::get('warehouseId');
            $material_receiving->material_receiving_no =Input::get('materialReceivingNo');
            $material_receiving->material_receiving_plbl =Input::get('materialReceivingPlbl');
            $material_receiving->material_receiving_shipment =Input::get('materialReceivingShipment');
            $material_receiving->material_receiving_date =$date;
            $material_receiving->material_receiving_unpacked_date =$unpackedDate;
            $material_receiving->material_receiving_checked_by =$materialReceivingCheckedBy;
            $material_receiving->material_receiving_checked_date =$checkedDate;
            $material_receiving->material_receiving_approved_by =$materialReceivingApprovedBy;
            $material_receiving->material_receiving_approved_date =$approvedDate;
            $material_receiving->created_user=Auth::user()->username;
            $material_receiving->updated_user=Auth::user()->username;
            $material_receiving->save();
            
            for ($i=0; $i <$totalRow; $i++) { 
                $material_receiving_detail = new MaterialReceivingDetail;
                $material_receiving_detail->material_receiving_id=$material_receiving->id;
                $material_receiving_detail->material_id=$materials[$i]["id"];
                $material_receiving_detail->quantity=$quantities[$i];
                $material_receiving_detail->remarks=$remarks[$i];
                $material_receiving_detail->created_user=Auth::user()->username;
                $material_receiving_detail->updated_user=Auth::user()->username;
                $material_receiving_detail->save();
            }

            Session::flash('message','Successfully created material receiving!');
       }
        return "Successfully Created";
	}

	public function editWithAjax($id)
	{
		$purchaseOrderId=Input::get('purchaseOrderId');
		$warehouseId =Input::get('warehouseId');
		$materialReceivingNo=Input::get('materialReceivingNo');
		$materialReceivingPlbl=Input::get('materialReceivingPlbl');
		$materialReceivingShipment=Input::get('materialReceivingShipment');
		$materialReceivingDate=Input::get('materialReceivingDate');
		$materialReceivingUnpackedDate=Input::get('materialReceivingUnpackedDate');
		$materialReceivingCheckedBy=Input::get('materialReceivingCheckedBy');
		$materialReceivingCheckedDate=Input::get('materialReceivingCheckedDate');
		$materialReceivingApprovedBy=Input::get('materialReceivingApprovedBy');
		$materialReceivingApprovedDate=Input::get('materialReceivingApprovedDate');

		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$date = date("Y-m-d", strtotime($materialReceivingDate));
    	$unpackedDate = date("Y-m-d", strtotime($materialReceivingUnpackedDate));
    	$checkedDate = date("Y-m-d", strtotime($materialReceivingUnpackedDate));
    	$approvedDate = date("Y-m-d", strtotime($materialReceivingApprovedDate));
    	$rules = array(
            'purchaseOrderId'  =>'required',
            'warehouseId'=>'required',
            'materialReceivingNo'=>'required',
            'materialReceivingPlbl'=>'required',
            'materialReceivingShipment'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::to('material_receivings/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $material_receiving = MaterialReceiving::find($id);

     		$material_receiving->purchase_order_id =Input::get('purchaseOrderId');
            $material_receiving->warehouse_id =Input::get('warehouseId');
            $material_receiving->material_receiving_no =Input::get('materialReceivingNo');
            $material_receiving->material_receiving_plbl =Input::get('materialReceivingPlbl');
            $material_receiving->material_receiving_shipment =Input::get('materialReceivingShipment');
            $material_receiving->material_receiving_date =$date;
            $material_receiving->material_receiving_unpacked_date =$unpackedDate;
            $material_receiving->material_receiving_checked_by =$materialReceivingCheckedBy;
            $material_receiving->material_receiving_checked_date =$checkedDate;
            $material_receiving->material_receiving_approved_by =$materialReceivingApprovedBy;
            $material_receiving->material_receiving_approved_date =$approvedDate;
            $material_receiving->save();

			$materialReceivingDetail=MaterialReceivingDetail::where('material_receiving_id', '=', $id)->Get();

            foreach ($materialReceivingDetail as $key => $value) {
            	$test =MaterialReceivingDetail::find($value->id);
            	$test->delete();
            }
            // $materialReceivingDetail=MaterialReceivingDetail::where('purchase_order_id', '=', $purchase_order->id)->Get();
            for ($i=0; $i <$totalRow; $i++) { 
                // $purchase_order_detail = new MaterialReceivingDetail::find($materialReceivingDetail[$i]->id);
                $material_receiving_detail=new MaterialReceivingDetail();
                $material_receiving_detail->material_receiving_id=$material_receiving->id;
                $material_receiving_detail->material_id=$materials[$i]["id"];
                $material_receiving_detail->quantity=$quantities[$i];
                $material_receiving_detail->remarks=$remarks[$i];
                $material_receiving_detail->created_user=Auth::user()->username;
                $material_receiving_detail->updated_user=Auth::user()->username;
                $material_receiving_detail->save();
            }
            Session::flash('message','Successfully updated Material Receiving!');
        }
        return "Successfully updated purchase order!";
	}
}
