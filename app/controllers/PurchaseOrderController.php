<?php

class PurchaseOrderController extends \BaseController {
	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$purchase_orders = PurchaseOrder::paginate(5);
    	$this->layout->content = View::make('transactions.purchase_orders.index')
    		-> with('purchase_orders', $purchase_orders);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_material_take_off = DB::table('material_take_offs')->orderBy('mto_description', 'asc')->lists('mto_description','id');
		$populate_vendor = DB::table('vendors')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('transactions.purchase_orders.create', array('populate_vendor' => $populate_vendor,'populate_material_take_off' => $populate_material_take_off));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$purchase_order =PurchaseOrder::find($id);

		$this->layout->content = View::make('transactions.purchase_orders.show')
			->with('purchase_order',$purchase_order);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$purchase_order = PurchaseOrder::find($id);
		$populate_material_take_off = DB::table('material_take_offs')->orderBy('mto_description', 'asc')->lists('mto_description','id');
		$populate_vendor = DB::table('vendors')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('transactions.purchase_orders.edit', 
			array('populate_vendor' => $populate_vendor),
			array('populate_material_take_off' => $populate_material_take_off))
			->with('purchase_order',$purchase_order);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_receiving =DB::table('material_receivings')->where('material_receivings.purchase_order_id', '=', $id);
		$nonconformance =DB::table('nonconformances')->where('nonconformances.purchase_order_id', '=', $id);
		if ($material_receiving->count()==0 && $nonconformance->count()== 0){
			$purchase_order = PurchaseOrder::find($id);
			$purchase_order->delete();
			Session::flash('message','Successfully deleted the purchase order');
		}if($nonconformance->count() != 0){
			Session::flash('warning','Purchase order still used in nonconformance');
		}if($material_receiving->count() != 0){
			Session::flash('warning','Purchase order still used in material receiving');
		}
		return Redirect::to('purchase_orders');
	}
	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchPurchaseOrder()
	{
		$q = Input::get('purchase_order');
		$search='%'.$q.'%';
		$purchase_orders = DB::table('purchase_orders')
					->select(array('purchase_orders.id','purchase_orders.material_take_off_id', 'purchase_orders.vendor_id',
								'purchase_orders.po_no', 'purchase_orders.po_revision', 
								'purchase_orders.po_date','purchase_orders.po_estimate_date',
								'purchase_orders.po_promised_date','purchase_orders.po_description','purchase_orders.po_status'))
					->where('purchase_orders.po_no', 'like', $search)
					->paginate(5);
		if ($purchase_orders->count() > 0) {
			Session::flash('message','Successfully search purchase order!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('transactions.purchase_orders.index')
			-> with('purchase_orders', $purchase_orders);
	}

		/**
	 * find  of the resource.
	 *
	 * @return Response
	 */
	public function find($code)
	{
		$material=Material::where('code', '=', $code)->Get();
        return Response::json($material);
	}
	/**
	 * listPurchaseOrderDetail digunakan pada edit view purchase order.
	 *
	 * @return Response
	 */
	public function listPurchaseOrderDetail($purchase_order_id)
	{
		$purchaseOrderDetail=PurchaseOrderDetail::where('purchase_order_id', '=', $purchase_order_id)->Get();
        return Response::json($purchaseOrderDetail);
	}
	/**
	 * listMaterial digunakan pada edit view material.
	 *
	 * @return Response
	 */
	public function listMaterial($purchase_order_id)
	{
		$purchaseOrderDetail=PurchaseOrderDetail::where('purchase_order_id', '=', $purchase_order_id)->Get();
    	$materials=array();
    	foreach ($purchaseOrderDetail as $key => $value) {
    		$data = Material::find($value->material_id);
			if ($data) {
				if ($data->count()!=0) {
				$materials[] = $data->toArray();
				}
			}
    	}
       return Response::json($materials);
	}

	public function createWithAjax()
	{
		$poDate=Input::get('poDate');
		$poEstimateDate =Input::get('poEstimateDate');
		$poPromisedDate=Input::get('poPromisedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$prices =Input::get('prices');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$date = date("Y-m-d", strtotime($poDate));
    	$estimateDate = date("Y-m-d", strtotime($poEstimateDate));
    	$promisedDate = date("Y-m-d", strtotime($poPromisedDate));
    	$rules = array(
            'materialTakeOffId'  =>'required',
            'vendorId'=>'required',
            'poNo'=>'required',
            'poRevision'=>'required',
            'poDate'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::to('purchase_orders/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $purchase_order = new PurchaseOrder;
            $purchase_order->material_take_off_id =Input::get('materialTakeOffId');
            $purchase_order->vendor_id =Input::get('vendorId');
            $purchase_order->po_no =Input::get('poNo');
            $purchase_order->po_date =$date;
            $purchase_order->po_revision =Input::get('poRevision');
            $purchase_order->po_estimate_date =$estimateDate;
            $purchase_order->po_promised_date =$promisedDate;
            $purchase_order->po_status =Input::get('poStatus');
            $purchase_order->po_description =Input::get('poDescription');
            $purchase_order->save();
            
            for ($i=0; $i <$totalRow; $i++) { 
                $purchase_order_detail = new PurchaseOrderDetail;
                $purchase_order_detail->purchase_order_id=$purchase_order->id;
                $purchase_order_detail->material_id=$materials[$i]["id"];
                $purchase_order_detail->price=$prices[$i];
                $purchase_order_detail->quantity=$quantities[$i];
                $purchase_order_detail->remarks=$remarks[$i];
                $purchase_order_detail->created_user=Auth::user()->username;
                $purchase_order_detail->updated_user=Auth::user()->username;
                $purchase_order_detail->save();
            }

            Session::flash('message','Successfully created purchase order!');
       }
        return "Successfully Created".$estimateDate;
	}

	public function editWithAjax($id)
	{
		$poDate=Input::get('poDate');
		$poEstimateDate =Input::get('poEstimateDate');
		$poPromisedDate=Input::get('poPromisedDate');
		$totalRow =Input::get('totalRow');
    	$materials=Input::get('materials');
    	$prices =Input::get('prices');
    	$quantities=Input::get('quantities');
    	$remarks=Input::get('remarks');
    	$date = date("Y-m-d", strtotime($poDate));
    	$estimateDate = date("Y-m-d", strtotime($poEstimateDate));
    	$promisedDate = date("Y-m-d", strtotime($poPromisedDate));
    	$rules = array(
            'materialTakeOffId'  =>'required',
            'vendorId'=>'required',
            'poNo'=>'required',
            'poRevision'=>'required',
            'poDate'=>'required',
            );
        $validator = Validator::make(Input::all(),$rules);

        if($validator->fails()){
            return Redirect::to('purchase_orders/' .$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $purchase_order = PurchaseOrder::find($id);

            $purchase_order->material_take_off_id =Input::get('materialTakeOffId');
            $purchase_order->vendor_id =Input::get('vendorId');
            $purchase_order->po_no =Input::get('poNo');
            $purchase_order->po_date =$date;
            $purchase_order->po_revision =Input::get('poRevision');
            $purchase_order->po_estimate_date =$estimateDate;
            $purchase_order->po_promised_date =$promisedDate;
            $purchase_order->po_status =Input::get('poStatus');
            $purchase_order->po_description =Input::get('poDescription');
            $purchase_order->save();

			$purchaseOrderDetail=PurchaseOrderDetail::where('purchase_order_id', '=', $id)->Get();

            foreach ($purchaseOrderDetail as $key => $value) {
            	$test =PurchaseOrderDetail::find($value->id);
            	$test->delete();
            }
            // $purchaseOrderDetail=PurchaseOrderDetail::where('purchase_order_id', '=', $purchase_order->id)->Get();
            for ($i=0; $i <$totalRow; $i++) { 
                // $purchase_order_detail = new PurchaseOrderDetail::find($purchaseOrderDetail[$i]->id);
                $purchase_order_detail=new PurchaseOrderDetail();
                $purchase_order_detail->purchase_order_id=$purchase_order->id;
                $purchase_order_detail->material_id=$materials[$i]["id"];
                $purchase_order_detail->price=$prices[$i];
                $purchase_order_detail->quantity=$quantities[$i];
                $purchase_order_detail->remarks=$remarks[$i];
                $purchase_order_detail->created_user=Auth::user()->username;
                $purchase_order_detail->updated_user=Auth::user()->username;
                $purchase_order_detail->save();
            }
            Session::flash('message','Successfully updated purchase order!');
        }
        return "Successfully updated purchase order!";
	}
}
