<?php

class ReportController extends \BaseController {

	public function listProject()
	{
		$projects = Project::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "PROJECT REPORT";
        $header = array(
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"NAME", "length"=>50, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>80, "align"=>"L"),
                array("label"=>"OWNER NAME", "length"=>30, "align"=>"L")
        );
        $data =array();
        foreach ($projects as $key => $value) {
            $data[] = array(
                array("label"=>$value->code, "length"=>30, "align"=>"L"),
                array("label"=>$value->name, "length"=>50, "align"=>"L"),
                array("label"=>$value->description, "length"=>80, "align"=>"L"),
                array("label"=>$value->owner_name, "length"=>30, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        // $fpdf->Cell(60,10,'Powered by FPDF.',0,1,'C');
        $fpdf->Output();
        exit;
	}

    public function listProjectDoc()
    {
        $project_docs = ProjectDoc::all();
        $fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "PROJECT Doc REPORT";
        $header = array(
                array("label"=>"PROJECT", "length"=>30, "align"=>"L"),
                array("label"=>"DOC CONTENT", "length"=>50, "align"=>"L"),
                array("label"=>"OCTET TYPE", "length"=>80, "align"=>"L")
        );
        $data =array();
        foreach ($project_docs as $key => $value) {
            $data[] = array(
                array("label"=>$value->project_id, "length"=>30, "align"=>"L"),
                array("label"=>$value->doc_content, "length"=>50, "align"=>"L"),
                array("label"=>$value->octet_type, "length"=>80, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        // $fpdf->Cell(60,10,'Powered by FPDF.',0,1,'C');
        $fpdf->Output();
        exit;
    }

	public function listVendor()
	{
		$vendors = Vendor::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "VENDOR REPORT";
        $header= array(
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"NAME", "length"=>50, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>80, "align"=>"L"),
                array("label"=>"ADDRESS", "length"=>30, "align"=>"L")
        );
        $data = array();
        foreach ($vendors as $key => $value) {
            $data[] = array(
                array("label"=>$value->code, "length"=>30, "align"=>"L"),
                array("label"=>$value->name, "length"=>50, "align"=>"L"),
                array("label"=>$value->description, "length"=>80, "align"=>"L"),
                array("label"=>$value->address, "length"=>30, "align"=>"L")
            );
        }
        
       #tampilkan title laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        // $fpdf->Cell(60,10,'Powered by FPDF.',0,1,'C');
        $fpdf->Output();
        exit;
	}

	public function listWarehouse()
	{
		$warehouses = Warehouse::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "WAREHOUSE REPORT";
        $header= array(
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>50, "align"=>"L"),
                array("label"=>"ADDRESS", "length"=>80, "align"=>"L"),
                array("label"=>"PHONE", "length"=>30, "align"=>"L")
        );
        $data = array();
        foreach ($warehouses as $key => $value) {
            $data[] = array(
                array("label"=>$value->code, "length"=>30, "align"=>"L"),
                array("label"=>$value->description, "length"=>50, "align"=>"L"),
                array("label"=>$value->address, "length"=>80, "align"=>"L"),
                array("label"=>$value->phone, "length"=>30, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	$fpdf->SetFont('Arial','I',8);
    	// Page number
        $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listMaterialCategory()
	{
		$material_categories = MaterialCategory::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL CATEGORY REPORT";
        $header= array(
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"NAME", "length"=>50, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>60, "align"=>"L"),
                array("label"=>"CREATED AT", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($material_categories as $key => $value){
            $data[] = array(
                array("label"=>$value->code, "length"=>30, "align"=>"L"),
                array("label"=>$value->name, "length"=>50, "align"=>"L"),
                array("label"=>$value->description, "length"=>60, "align"=>"L"),
                array("label"=>$value->created_at, "length"=>50, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	$fpdf->SetFont('Arial','I',8);
    	// Page number
        $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listMaterial()
	{
		$materials = Material::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL REPORT";
        $header= array(
                array("label"=>"MATERIAL CATEGORY", "length"=>50, "align"=>"L"),
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"NAME", "length"=>60, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($materials as $key => $value){
        	$category =MaterialCategory::find($value->material_category_id);

            $data[] = array(
                array("label"=>$category['name'], "length"=>50, "align"=>"L"),
                array("label"=>$value->code, "length"=>30, "align"=>"L"),
                array("label"=>$value->name, "length"=>60, "align"=>"L"),
                array("label"=>$value->description, "length"=>50, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	$fpdf->SetFont('Arial','I',8);
    	// Page number
        $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

    public function listMaterialDoc()
    {
        $material_docs = MaterialDoc::all();
        $fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL DOC REPORT";
        $header= array(
                array("label"=>"MATERIAL", "length"=>50, "align"=>"L"),
                array("label"=>"DOC CONTENT", "length"=>30, "align"=>"L"),
                array("label"=>"OCTET TYPE", "length"=>60, "align"=>"L")
        );
        $data = array();
        foreach ($material_docs as $key => $value){
            $material =Material::find($value->material_id);

            $data[] = array(
                array("label"=>$material['name'], "length"=>50, "align"=>"L"),
                array("label"=>$value->doc_content, "length"=>30, "align"=>"L"),
                array("label"=>$value->octet_type, "length"=>60, "align"=>"L")
            );
        }
        
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
        // Arial italic 8
        $fpdf->SetFont('Arial','I',8);
        // Page number
        $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
    }


	public function listMaterialTakeOff()
	{
		$material_take_offs = MaterialTakeOff::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL TAKE OFF REPORT";
        $header= array(
                array("label"=>"PROJECT", "length"=>50, "align"=>"L"),
                array("label"=>"MTO NO", "length"=>30, "align"=>"L"),
                array("label"=>"MTO REVISION", "length"=>60, "align"=>"L"),
                array("label"=>"MTO DATE", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($material_take_offs as $key => $value){
        	$project =Project::find($value->project_id);

            $data[] = array(
                array("label"=>$project['name'], "length"=>50, "align"=>"L"),
                array("label"=>$value->mto_no, "length"=>30, "align"=>"L"),
                array("label"=>$value->mto_revision, "length"=>60, "align"=>"L"),
                array("label"=>$value->mto_date, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	// $fpdf->SetFont('Arial','I',8);
    	// Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listPurchaseOrder()
	{
		$purchase_orders = PurchaseOrder::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "PURCHASE ORDER REPORT";
        $header= array(
                array("label"=>"MATERIAL TAKE OFF", "length"=>50, "align"=>"L"),
                array("label"=>"VENDOR", "length"=>30, "align"=>"L"),
                array("label"=>"PO NO", "length"=>60, "align"=>"L"),
                array("label"=>"PO REVISION", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($purchase_orders as $key => $value){
        	$material_take_off =MaterialTakeOff::find($value->material_take_off_id);
        	$vendor =Vendor::find($value->vendor_id);
            $data[] = array(
                array("label"=>$material_take_off['mto_no'], "length"=>50, "align"=>"L"),
                array("label"=>$vendor['name'], "length"=>30, "align"=>"L"),
                array("label"=>$value->po_no, "length"=>60, "align"=>"L"),
                array("label"=>$value->po_revision, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	// $fpdf->SetFont('Arial','I',8);
    	// Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listMaterialReceiving()
	{
		$material_receivings = MaterialReceiving::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL RECEIVING REPORT";
        $header= array(
                array("label"=>"PURCHASE ORDER", "length"=>50, "align"=>"L"),
                array("label"=>"WAREHOUSE", "length"=>30, "align"=>"L"),
                array("label"=>"MATERIAL RECEIVING NO", "length"=>60, "align"=>"L"),
                array("label"=>"MATERIAL RECEIVING PLBL", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($material_receivings as $key => $value){
        	$purchase_order =PurchaseOrder::find($value->purchase_order_id);
        	$warehouse =Warehouse::find($value->warehouse_id);
            $data[] = array(
                array("label"=>$purchase_order['po_no'], "length"=>50, "align"=>"L"),
                array("label"=>$warehouse['description'], "length"=>30, "align"=>"L"),
                array("label"=>$value->material_receiving_no, "length"=>60, "align"=>"L"),
                array("label"=>$value->material_receiving_plbl, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	// $fpdf->SetFont('Arial','I',8);
    	// Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listMaterialIssuance()
	{
		$material_issuances = MaterialIssuance::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL ISSUANCE REPORT";
        $header= array(
                array("label"=>"MATERIAL TAKE OFF", "length"=>50, "align"=>"L"),
                array("label"=>"WAREHOUSE", "length"=>30, "align"=>"L"),
                array("label"=>"MATERIAL ISSUANCE NO", "length"=>60, "align"=>"L"),
                array("label"=>"MATERIAL ISSUANCE DATE", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($material_issuances as $key => $value){
        	$material_take_off =MaterialTakeOff::find($value->material_take_off_id);
        	$warehouse =Warehouse::find($value->warehouse_id);
            $data[] = array(
                array("label"=>$material_take_off['mto_no'], "length"=>50, "align"=>"L"),
                array("label"=>$warehouse['description'], "length"=>30, "align"=>"L"),
                array("label"=>$value->material_issuance_no, "length"=>60, "align"=>"L"),
                array("label"=>$value->material_issuance_date, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	// $fpdf->SetFont('Arial','I',8);
    	// Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

	public function listMaterialConstruction()
	{
		$material_constructions = MaterialConstruction::all();
    	$fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "MATERIAL CONSTRUCTION REPORT";
        $header= array(
                array("label"=>"MATERIAL ISSUANCE", "length"=>50, "align"=>"L"),
                array("label"=>"REMARKS", "length"=>30, "align"=>"L"),
                array("label"=>"CREATED USER", "length"=>60, "align"=>"L"),
                array("label"=>"UPDATED USER", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($material_constructions as $key => $value){
        	$material_issuance=MaterialIssuance::find($value->material_issuance_id);

            $data[] = array(
                array("label"=>$material_issuance['material_issuance_no'], "length"=>50, "align"=>"L"),
                array("label"=>$value->remarks, "length"=>30, "align"=>"L"),
                array("label"=>$value->created_user, "length"=>60, "align"=>"L"),
                array("label"=>$value->updated_user, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
    	// Arial italic 8
    	// $fpdf->SetFont('Arial','I',8);
    	// Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
	}

    public function listNonconformance()
    {
        $nonconformances = Nonconformance::all();
        $fpdf = new Fpdf();
        $fpdf->AddPage();
        $title = "NONCONFORMANCE REPORT";
        $header= array(
                array("label"=>"PURCHASE ORDER", "length"=>50, "align"=>"L"),
                array("label"=>"CODE", "length"=>30, "align"=>"L"),
                array("label"=>"DATE", "length"=>60, "align"=>"L"),
                array("label"=>"DESCRIPTION", "length"=>50, "align"=>"L")
        );
        $data = array();
        foreach ($nonconformances as $key => $value){
            $purchase_order=PurchaseOrder::find($value->purchase_order_id);

            $data[] = array(
                array("label"=>$purchase_order['po_no'], "length"=>50, "align"=>"L"),
                array("label"=>$value->nonconformance_code, "length"=>30, "align"=>"L"),
                array("label"=>$value->nonconformance_date, "length"=>60, "align"=>"L"),
                array("label"=>$value->nonconformance_description, "length"=>50, "align"=>"L")
            );
        }
       #tampilkan judul laporan
        $fpdf->SetFont('Arial','B','16');
        $fpdf->Cell(0,20, $title, '0', 1, 'C');
 
        #buat header tabel
        $fpdf->SetFont('Arial','','10');
        $fpdf->SetFillColor(28,113,240);
        $fpdf->SetTextColor(255);
        $fpdf->SetDrawColor(128,0,0);
        foreach ($header as $kolom) {
            $fpdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
        }
        $fpdf->Ln();
        #tampilkan data tabelnya
        $fpdf->SetFillColor(224,235,255);
        $fpdf->SetTextColor(0);
        $fpdf->SetFont('');
        $fill=false;
        foreach ($data as $baris) {
            // $i = 0;
            foreach ($baris as $cell) {
                $fpdf->Cell($cell['length'], 5, $cell['label'], 1, '0', $cell['align'], true);
                // $i++;
            }
            // $fill = !$fill;
            $fpdf->Ln();
        }
        $fpdf->SetAuthor('Erick' , true);
        $fpdf->SetTitle($title , true);
     
        // Arial italic 8
        // $fpdf->SetFont('Arial','I',8);
        // Page number
        // $fpdf->Cell(0,230,'Page '.$fpdf->PageNo(),0,30,'C');
        $fpdf->Output();
        exit;
    }
}