<!-- app/controller/ProjectController.php -->
<?php

class ProjectController extends \BaseController {

	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = Project::paginate(5);
    	$this->layout->content = View::make('projects.index')
    		-> with('projects', $projects);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('projects.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			'owner_name'=>'required',
			'owner_address'=>'required',
			);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('projects/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$project = new Project;
			$project->code =Input::get('code');
			$project->name =Input::get('name');
			$project->description =Input::get('description');
			$project->owner_name =Input::get('owner_name');
			$project->owner_address =Input::get('owner_address');
			$project->owner_phone =Input::get('owner_phone');
			$project->budget =Input::get('budget');
			$project->start_date =Input::get('start_date');
			$project->duration =Input::get('duration');
			$project->save();

			Session::flash('message','Successfully created project!');
			return Redirect::to('projects');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project =Project::find($id);

		$this->layout->content = View::make('projects.show')
			->with('project',$project);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::find($id);

		$this->layout->content = View::make('projects.edit')
			->with('project',$project);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules=array(
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			'owner_name'=>'required',
			'owner_address'=>'required',
		);	

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('projects/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$project=Project::find($id);

			$project->code =Input::get('code');
			$project->name =Input::get('name');
			$project->description =Input::get('description');
			$project->owner_name =Input::get('owner_name');
			$project->owner_address =Input::get('owner_address');
			$project->owner_phone =Input::get('owner_phone');
			$project->budget =Input::get('budget');
			$project->start_date =Input::get('start_date');
			$project->duration =Input::get('duration');

			$project->save();

			Session::flash('message','Successfully updated project!' );
			return Redirect::to('projects');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material_take_off =DB::table('material_take_offs')->where('material_take_offs.project_id', '=', $id);
		$project_doc =DB::table('project_docs')->where('project_docs.project_id', '=', $id);
		if ($material_take_off->count()==0 && $project_doc->count()== 0){
			$project = Project::find($id);
			$project->delete();
			Session::flash('message','Successfully deleted the project');
		}if($project_doc->count() != 0){
			Session::flash('warning','Project still used in project doc');
		}if($material_take_off->count() != 0){
			Session::flash('warning','Project still used in material take off');
		}
		return Redirect::to('projects');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchProject()
	{
		$q = Input::get('project');
		$search='%'.$q.'%';
		$projects = DB::table('projects')
					->select(array('projects.id','projects.code','projects.name', 'projects.description', 'projects.owner_name', 'projects.owner_address', 'projects.owner_phone', 'projects.budget', 'projects.start_date', 'projects.duration'))
					->where('projects.name', 'like', $search)
					->paginate(5);
		if ($projects->count() > 0) {
			Session::flash('message','Successfully search project!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('projects.index')
			-> with('projects', $projects);
	}

}
