<!-- app/controller/VendorController.php -->
<?php

class VendorController extends \BaseController {

	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$vendors = Vendor::paginate(5);
    	$this->layout->content = View::make('vendors.index')
    		-> with('vendors', $vendors);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('vendors.create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'code'	=>'required',
			'name'	=>'required',
			'description'=>'required',
			'address'=>'required',
			'phone'=>'required',
			);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('vendors/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$vendor = new Vendor;
			$vendor->code =Input::get('code');
			$vendor->name =Input::get('name');
			$vendor->description =Input::get('description');
			$vendor->address =Input::get('address');
			$vendor->phone =Input::get('phone');
			$vendor->save();

			Session::flash('message','Successfully created vendor!');
			return Redirect::to('vendors');
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$vendor =Vendor::find($id);

		$this->layout->content = View::make('vendors.show')
			->with('vendor',$vendor);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vendor = Vendor::find($id);

		$this->layout->content = View::make('vendors.edit')
			->with('vendor',$vendor);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules=array(
			'code'	=>'required',
			'name'=>'required',
			'description'=>'required',
			'address'=>'required',
			'phone'=>'required'
		);	

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('vendors/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$vendor=Vendor::find($id);

			$vendor->code =Input::get('code');
			$vendor->name=Input::get('name');
			$vendor->description =Input::get('description');
			$vendor->address =Input::get('address');
			$vendor->phone =Input::get('phone');

			$vendor->save();

			Session::flash('message','Successfully updated vendor!' );
			return Redirect::to('vendors');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$purchase_order =DB::table('purchase_orders')->where('purchase_orders.vendor_id', '=', $id);
		if ($purchase_order->count() == 0) {
			$vendor = Vendor::find($id);
			$vendor->delete();

			Session::flash('message','Successfully deleted the vendor!');
		}else{

			Session::flash('warning','Vendor still used in purchase order!');
		}
		return Redirect::to('vendors');

	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchVendor()
	{
		$q = Input::get('vendor');
		$search='%'.$q.'%';
		$vendors = DB::table('vendors')
					->select(array('vendors.id','vendors.code', 'vendors.name', 'vendors.description', 'vendors.address', 'vendors.phone'))
					->where('vendors.name', 'like', $search)
					->paginate(5);
		if ($vendors->count() > 0) {
			Session::flash('message','Successfully search vendor!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('vendors.index')
			-> with('vendors', $vendors);
	}

}
