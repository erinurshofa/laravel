<?php

class ProjectDocController extends \BaseController {


	/**
	 * Use layout from app/views/layout.blade.php with-> public $layout = 'layout';
	 * Use layout from app/views/layout/default.blade.php with-> public $layout = 'layout.default';
	 * then change -> return with $this->layout->content =
	 */
	public $layout = 'layout';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$project_docs = ProjectDoc::paginate(5);
    	$this->layout->content = View::make('project_docs.index')
    		-> with('project_docs', $project_docs);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$populate_project = DB::table('projects')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('project_docs.create', array('populate_project' => $populate_project));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'project_id'	=>'required',
			'doc_content'	=>'required',
			'octet_type'=>'required',

		);
		$validator = Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('project_docs/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$project_doc = new ProjectDoc;
			$project_doc->project_id =Input::get('project_id');
			$project_doc->doc_content =Input::get('doc_content');
			$project_doc->octet_type =Input::get('octet_type');
            $project_doc->created_user=Auth::user()->username;
            $project_doc->updated_user=Auth::user()->username;
			
			$project_doc->save();

			Session::flash('message','Successfully created project doc!');
			return Redirect::to('project_docs');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project_doc =ProjectDoc::find($id);

		$this->layout->content = View::make('project_docs.show')
			->with('project_doc',$project_doc);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project_doc = ProjectDoc::find($id);
		$populate_project = DB::table('projects')->orderBy('name', 'asc')->lists('name','id');
		$this->layout->content = View::make('project_docs.edit', array('populate_project' => $populate_project))
			->with('project_doc',$project_doc);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'project_id'	=>'required',
			'doc_content'	=>'required',
			'octet_type'=>'required',

		);

		$validator =Validator::make(Input::all(),$rules);
		
		if ($validator->fails()) {
			return Redirect::to('project_docs/' .$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}else{
			$project_doc=ProjectDoc::find($id);

			$project_doc->project_id =Input::get('project_id');
			$project_doc->doc_content =Input::get('doc_content');
			$project_doc->octet_type =Input::get('octet_type');
            $project_doc->created_user=Auth::user()->username;
            $project_doc->updated_user=Auth::user()->username;

			$project_doc->save();
			Session::flash('message','Successfully updated project doc!' );
			return Redirect::to('project_docs');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$project_doc = ProjectDoc::find($id);
		$project_doc->delete();

		Session::flash('message','Successfully deleted the project doc');
		return Redirect::to('project_docs');
	}

	/**
	 * Search and display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchProjectDoc()
	{
		$q = Input::get('project_doc');
		$search='%'.$q.'%';
		$project_docs = DB::table('project_docs')
					->select(array('project_docs.id','project_docs.project_id', 'project_docs.doc_content',
								'project_docs.octet_type'))
					->where('project_docs.doc_content', 'like', $search)
					->paginate(5);
		if ($project_docs->count() > 0) {
			Session::flash('message','Successfully search project doc!' );
		}else{
			Session::flash('message','Search not found!' );
		}
		
		$this->layout->content = View::make('project_docs.index')
			-> with('project_docs', $project_docs);
	}


}
