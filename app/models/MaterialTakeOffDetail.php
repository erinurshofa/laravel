<?php
class MaterialTakeOffDetail extends Eloquent
{
	protected $fillable = array('name', 'material_id');
	// protected $table = 'materials';
	public function material()
    {
        return $this->belongsTo('Material', 'material_id');
    }
}