<?php
class MaterialTakeOff extends Eloquent
{
	// public function material_take_off_details()
 //    {
 //        return $this->hasMany('MaterialTakeOffDetail');
 //    }
    public function project()
    {
        return $this->belongsTo('Project', 'project_id');
    }

   /**
   * Present the created_at property
   * using a different format
   *
   * @return string
   */
  public function mto_date()
  {
    return $this->object->mto_date->format('Y-m-d');
  }
 
}