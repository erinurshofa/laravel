## Laravel PHP Framework

[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Laravel PHP Framework Indonesia

Laravel adalah framework aplikasi web dengan ekspresif, sintaks yang elegan. Kami percaya pembangunan harus menjadi pengalaman kreatif menyenangkan untuk benar-benar memuaskan. Laravel mencoba untuk mengambil rasa sakit dari pembangunan dengan mengurangi tugas-tugas umum digunakan dalam sebagian besar proyek-proyek web, seperti otentikasi, routing, sesi, dan caching. 

Laravel bertujuan untuk membuat proses pembangunan yang menyenangkan bagi pengembang tanpa mengorbankan fungsionalitas aplikasi. Selamat pengembang membuat kode terbaik. Untuk tujuan ini, kami telah mencoba untuk menggabungkan yang terbaik dari apa yang telah kita lihat dalam kerangka web lain, termasuk kerangka diimplementasikan dalam bahasa lain, seperti Ruby on Rails, ASP.NET MVC, dan Sinatra. 

Laravel dapat diakses, namun kuat, menyediakan alat-alat yang kuat yang diperlukan untuk besar, aplikasi yang kuat. Sebuah inversi yang luar biasa dari wadah kontrol, sistem migrasi ekspresif, dan dukungan pengujian unit terintegrasi memberikan alat yang Anda butuhkan untuk membangun aplikasi yang Anda bertugas.

### Dokumentasi Resmi

Dokumentasi resmi laravel dapat ditemukan di [Laravel website](http://laravel.com/docs).

### Server Requirements

The Laravel framework has a few system requirements:
PHP >= 5.3.7
MCrypt PHP Extension
As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension. When using Ubuntu, this can be done via apt-get install php5-json.
