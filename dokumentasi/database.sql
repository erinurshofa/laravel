-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-06-05 16:41:18
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for apptrackingdb
--DROP DATABASE IF EXISTS `apptrackingdb`;
--CREATE DATABASE IF NOT EXISTS `apptrackingdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jowostu1_apptrackingdb`;


-- Dumping structure for table apptrackingdb.materials
DROP TABLE IF EXISTS `materials`;
CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_category_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.materials: ~4 rows (approximately)
DELETE FROM `materials`;
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
INSERT INTO `materials` (`id`, `material_category_id`, `code`, `name`, `description`, `unit`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(1, 14, 'AD001', 'Super', 'description', 'kg', '', '', '2014-04-22 07:32:19', '2014-04-22 07:32:19'),
	(2, 14, 'AE001', 'Transaction', 'Super description', 'km', '', '', '2014-04-23 02:08:42', '2014-04-23 02:08:42'),
	(3, 14, 'AC001', 'Mantap', 'Ini Deskripsi super mantap', 'kg', '', '', '2014-05-19 04:08:33', '2014-05-19 04:08:33'),
	(4, 14, 'AD002', 'Coba', 'Super', 'kg', '', '', '2014-05-19 04:09:07', '2014-05-19 04:09:07');
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_categories
DROP TABLE IF EXISTS `material_categories`;
CREATE TABLE IF NOT EXISTS `material_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_categories: ~1 rows (approximately)
DELETE FROM `material_categories`;
/*!40000 ALTER TABLE `material_categories` DISABLE KEYS */;
INSERT INTO `material_categories` (`id`, `code`, `name`, `description`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(14, 'AA001', 'gas', 'this is description', '', '', '2014-04-21 08:46:41', '2014-04-21 08:46:41');
/*!40000 ALTER TABLE `material_categories` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_constructions
DROP TABLE IF EXISTS `material_constructions`;
CREATE TABLE IF NOT EXISTS `material_constructions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_issuance_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_constructions: ~0 rows (approximately)
DELETE FROM `material_constructions`;
/*!40000 ALTER TABLE `material_constructions` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_constructions` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_construction_details
DROP TABLE IF EXISTS `material_construction_details`;
CREATE TABLE IF NOT EXISTS `material_construction_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_construction_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_construction_details: ~0 rows (approximately)
DELETE FROM `material_construction_details`;
/*!40000 ALTER TABLE `material_construction_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_construction_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_docs
DROP TABLE IF EXISTS `material_docs`;
CREATE TABLE IF NOT EXISTS `material_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `doc_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `octet_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_docs: ~0 rows (approximately)
DELETE FROM `material_docs`;
/*!40000 ALTER TABLE `material_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_docs` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_issuances
DROP TABLE IF EXISTS `material_issuances`;
CREATE TABLE IF NOT EXISTS `material_issuances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_take_off_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `material_issuance_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_issuance_date` datetime NOT NULL,
  `material_issuance_request_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_issuance_request_date` datetime NOT NULL,
  `material_issuance_approved_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_issuance_approved_date` datetime NOT NULL,
  `material_issuance_received_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_issuance_received_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_issuances: ~0 rows (approximately)
DELETE FROM `material_issuances`;
/*!40000 ALTER TABLE `material_issuances` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_issuances` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_issuance_details
DROP TABLE IF EXISTS `material_issuance_details`;
CREATE TABLE IF NOT EXISTS `material_issuance_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_issuance_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_issuance_details: ~0 rows (approximately)
DELETE FROM `material_issuance_details`;
/*!40000 ALTER TABLE `material_issuance_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_issuance_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_receivings
DROP TABLE IF EXISTS `material_receivings`;
CREATE TABLE IF NOT EXISTS `material_receivings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `material_receiving_no` int(11) NOT NULL,
  `material_receiving_plbl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_receiving_shipment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_receiving_date` datetime NOT NULL,
  `material_receiving_unpacked_date` datetime NOT NULL,
  `material_receiving_checked_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_receiving_checked_date` datetime NOT NULL,
  `material_receiving_approved_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_receiving_aprroved_date` datetime NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_receivings: ~0 rows (approximately)
DELETE FROM `material_receivings`;
/*!40000 ALTER TABLE `material_receivings` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_receivings` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_receiving_details
DROP TABLE IF EXISTS `material_receiving_details`;
CREATE TABLE IF NOT EXISTS `material_receiving_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_receiving_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_receiving_details: ~0 rows (approximately)
DELETE FROM `material_receiving_details`;
/*!40000 ALTER TABLE `material_receiving_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `material_receiving_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_take_offs
DROP TABLE IF EXISTS `material_take_offs`;
CREATE TABLE IF NOT EXISTS `material_take_offs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `mto_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mto_revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mto_date` datetime NOT NULL,
  `mto_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mto_request_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mto_request_date` datetime NOT NULL,
  `mto_approved_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mto_approved_date` datetime NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_take_offs: ~70 rows (approximately)
DELETE FROM `material_take_offs`;
/*!40000 ALTER TABLE `material_take_offs` DISABLE KEYS */;
INSERT INTO `material_take_offs` (`id`, `project_id`, `mto_no`, `mto_revision`, `mto_date`, `mto_description`, `mto_request_by`, `mto_request_date`, `mto_approved_by`, `mto_approved_date`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(6, 3, '231313', 'aksldfjlf', '2014-05-27 00:00:00', 'desojrelrj', 'admin', '2014-05-27 00:00:00', 'admin', '2014-05-27 00:00:00', '', '', '2014-04-23 03:05:10', '2014-06-05 01:19:46'),
	(10, 1, '23232', 'ini revisinya', '2014-06-05 00:00:00', 'ini deskripsinya', 'joni', '2014-06-05 00:00:00', 'jony', '2014-06-05 00:00:00', '', '', '2014-04-23 04:10:27', '2014-06-05 01:21:32'),
	(11, 2, '23424', 'skud', '2014-05-08 00:00:00', 'super', 'admin', '2014-05-08 00:00:00', 'admin', '2014-05-08 00:00:00', '', '', '2014-05-08 03:45:26', '2014-05-08 03:45:26'),
	(22, 1, '232424', 'krle', '2014-05-19 00:00:00', 'klad', 'klafd', '2014-05-19 00:00:00', 'aklds', '2014-05-19 00:00:00', '', '', '2014-05-19 02:55:45', '2014-05-19 02:55:45'),
	(32, 1, '234', 'kae', '2014-05-19 00:00:00', 'aksfdj', 'aklf', '2014-05-19 00:00:00', 'jkw', '2014-05-19 00:00:00', '', '', '2014-05-19 03:23:19', '2014-05-19 03:23:19'),
	(33, 1, '232', 'jas', '2014-05-19 00:00:00', 'kasd', 'kasdl', '2014-05-19 00:00:00', 'df', '2014-05-19 00:00:00', '', '', '2014-05-19 03:26:17', '2014-05-19 03:26:17'),
	(34, 1, '232', 'jas', '2014-05-19 00:00:00', 'kasd', 'kasdl', '2014-05-19 00:00:00', 'df', '2014-05-19 00:00:00', '', '', '2014-05-19 03:27:54', '2014-05-19 03:27:54'),
	(35, 1, '23423', 'aksldf', '2014-05-19 00:00:00', 'ajsdlk', 'laskdf', '2014-05-19 00:00:00', 'akls', '2014-05-19 00:00:00', '', '', '2014-05-19 03:30:49', '2014-05-19 03:30:49'),
	(36, 3, '234234', 'akle', '2014-05-19 00:00:00', 'akdslf', 'kafls', '2014-05-19 00:00:00', 'klew', '2014-05-19 00:00:00', '', '', '2014-05-19 03:34:52', '2014-05-19 03:34:52'),
	(38, 3, '234234', 'akle', '2014-05-19 00:00:00', 'akdslf', 'kafls', '2014-05-19 00:00:00', 'klew', '2014-05-19 00:00:00', '', '', '2014-05-19 03:44:25', '2014-05-19 03:44:25'),
	(39, 2, '232', 'akdf', '2014-05-19 00:00:00', 'adf', 'akldf', '2014-05-19 00:00:00', 'akldf', '2014-05-19 00:00:00', '', '', '2014-05-19 03:45:10', '2014-05-19 03:45:10'),
	(40, 1, '234', '2342', '2014-05-19 00:00:00', 'aklsd', 'adk', '2014-05-19 00:00:00', 'jkw', '2014-05-19 00:00:00', '', '', '2014-05-19 04:00:46', '2014-05-19 04:00:46'),
	(41, 1, 'we', 'ads', '2014-05-19 00:00:00', 'adsf', 'wea', '2014-05-19 00:00:00', 'adf', '2014-05-19 00:00:00', '', '', '2014-05-19 04:10:23', '2014-05-19 04:10:23'),
	(42, 1, 'we', 'ads', '2014-05-19 00:00:00', 'adsf', 'wea', '2014-05-19 00:00:00', 'adf', '2014-05-19 00:00:00', '', '', '2014-05-19 04:15:13', '2014-05-19 04:15:13'),
	(43, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-05-19 04:26:55', '2014-05-19 04:26:55'),
	(44, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-05-19 04:33:01', '2014-06-05 00:41:58'),
	(45, 1, '11231', 'klasd', '2014-05-19 00:00:00', 'akd', 'akdlf', '2014-05-19 00:00:00', 'adskjf', '2014-05-19 00:00:00', '', '', '2014-05-19 05:47:23', '2014-05-19 05:47:23'),
	(46, 1, '234', 'adfskl', '2014-05-19 00:00:00', 'adf', 'akdlf', '2014-05-19 00:00:00', 'aklsd', '2014-05-19 00:00:00', '', '', '2014-05-19 06:20:19', '2014-05-19 06:20:19'),
	(47, 1, '2323', 'alsdk', '2014-05-19 00:00:00', 'akdlf', 'akdlf', '2014-05-19 00:00:00', 'kale', '2014-05-19 00:00:00', '', '', '2014-05-19 06:22:38', '2014-05-19 06:22:38'),
	(48, 2, '232341', 'kaler', '2014-05-19 00:00:00', 'asdklf', 'akfl', '2014-05-19 00:00:00', 'klak', '2014-05-19 00:00:00', '', '', '2014-05-19 06:31:31', '2014-05-19 06:31:31'),
	(49, 2, '232341', 'kaler', '2014-05-19 00:00:00', 'asdklf', 'akfl', '2014-05-19 00:00:00', 'klak', '2014-05-19 00:00:00', '', '', '2014-05-19 06:32:23', '2014-05-19 06:32:23'),
	(50, 2, '232341', 'kaler', '2014-05-19 00:00:00', 'asdklf', 'akfl', '2014-05-19 00:00:00', 'klak', '2014-05-19 00:00:00', '', '', '2014-05-19 06:32:27', '2014-05-19 06:32:27'),
	(51, 2, '232341', 'kaler', '2014-05-19 00:00:00', 'asdklf', 'akfl', '2014-05-19 00:00:00', 'klak', '2014-05-19 00:00:00', '', '', '2014-05-19 06:40:50', '2014-05-19 06:40:50'),
	(52, 2, '23424', 'aklfjle', '2014-05-19 00:00:00', 'kafd', 'lakdf', '2014-05-19 00:00:00', 'aklje', '2014-05-19 00:00:00', '', '', '2014-05-19 06:49:52', '2014-05-19 06:49:52'),
	(54, 1, 'adf', 'afdkl', '2014-05-19 00:00:00', 'ajkfl', 'akfdj', '2014-05-19 00:00:00', 'akldf', '2014-05-19 00:00:00', '', '', '2014-05-19 08:36:57', '2014-05-19 08:36:57'),
	(55, 1, 'adf', 'afdkl', '2014-05-19 00:00:00', 'ajkfl', 'akfdj', '2014-05-19 00:00:00', 'akldf', '2014-05-19 00:00:00', '', '', '2014-05-19 08:38:38', '2014-05-19 08:38:38'),
	(56, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:08:45', '2014-05-22 04:08:45'),
	(57, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:10:28', '2014-05-22 04:10:28'),
	(58, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:22:33', '2014-05-22 04:22:33'),
	(59, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:24:09', '2014-05-22 04:24:09'),
	(60, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:24:32', '2014-05-26 07:41:18'),
	(61, 1, '234234', 'super mantap revision', '2014-05-25 00:00:00', 'material terbagus', 'client', '2014-05-25 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-22 04:29:49', '2014-05-22 04:29:49'),
	(62, 1, '234234234', 'ajlkdsfj', '0000-00-00 00:00:00', 'ini deskripsinya', 'request by', '0000-00-00 00:00:00', 'approved by', '0000-00-00 00:00:00', '', '', '2014-05-22 04:32:08', '2014-05-22 04:32:08'),
	(63, 1, 'afj', 'jadkf', '0000-00-00 00:00:00', 'klafj', 'lkajfl', '0000-00-00 00:00:00', 'afsd', '0000-00-00 00:00:00', '', '', '2014-05-22 05:57:35', '2014-05-22 05:57:35'),
	(64, 1, '13412', 'qr', '0000-00-00 00:00:00', 'kljl', 'adf', '0000-00-00 00:00:00', 'qrew', '0000-00-00 00:00:00', '', '', '2014-05-22 05:59:10', '2014-05-22 05:59:10'),
	(65, 1, '13412', 'qr', '0000-00-00 00:00:00', 'kljl', 'adf', '0000-00-00 00:00:00', 'qrew', '0000-00-00 00:00:00', '', '', '2014-05-22 05:59:17', '2014-05-22 05:59:17'),
	(66, 1, '23423423', 'REVISION', '0000-00-00 00:00:00', 'KALDSJFK', 'ADMIN', '0000-00-00 00:00:00', 'ADMIN', '0000-00-00 00:00:00', '', '', '2014-05-22 07:04:15', '2014-05-22 07:04:15'),
	(67, 1, 'sdafk', 'kldasf', '0000-00-00 00:00:00', 'adsf', 'aldsfk', '0000-00-00 00:00:00', 'alksdf', '0000-00-00 00:00:00', '', '', '2014-05-22 07:44:44', '2014-05-22 07:44:44'),
	(68, 3, '2232', 'revision', '0000-00-00 00:00:00', 'ini deskriptsiona ', 'admin', '0000-00-00 00:00:00', 'alksd', '0000-00-00 00:00:00', '', '', '2014-05-23 01:28:56', '2014-05-23 01:28:56'),
	(69, 1, '2332', 'fsla', '2014-05-27 00:00:00', 'akfd', 'afdk', '2014-05-27 00:00:00', 'admin', '2014-05-27 00:00:00', '', '', '2014-05-23 02:43:47', '2014-05-27 03:52:27'),
	(70, 1, '2332', 'fsla', '0000-00-00 00:00:00', 'akfd', 'afdk', '0000-00-00 00:00:00', 'admin', '0000-00-00 00:00:00', '', '', '2014-05-23 02:53:14', '2014-05-23 02:53:14'),
	(71, 1, '2342432', 'KLASJDF', '0000-00-00 00:00:00', 'JAKJLSD', 'AKLSDFEW', '0000-00-00 00:00:00', 'ADMIN', '0000-00-00 00:00:00', '', '', '2014-05-23 03:30:17', '2014-05-23 03:30:17'),
	(72, 1, '234234', 'EKWLK', '0000-00-00 00:00:00', 'AKLD', 'AKFLKS', '0000-00-00 00:00:00', 'ADKLA', '0000-00-00 00:00:00', '', '', '2014-05-23 03:32:50', '2014-05-23 03:32:50'),
	(73, 1, '2324', 'ladf', '1970-01-01 00:00:00', 'kajfdl', 'akldf', '1970-01-01 00:00:00', 'akf', '1970-01-01 00:00:00', '', '', '2014-05-23 03:43:33', '2014-05-27 03:41:26'),
	(74, 1, '3232323', 'dsklwlk', '2014-05-23 00:00:00', 'aklsd', 'klael', '2014-05-23 00:00:00', 'adkl', '2014-05-23 00:00:00', '', '', '2014-05-23 03:50:23', '2014-05-23 03:50:23'),
	(75, 2, '12131', 'aksel', '2014-05-23 00:00:00', 'akelel', 'akw', '2014-05-23 00:00:00', 'adnub', '2014-05-23 00:00:00', '', '', '2014-05-23 06:08:22', '2014-06-03 01:36:17'),
	(76, 2, '122332', 'super', '2014-05-27 00:00:00', 'mantap', 'admin', '2014-05-27 00:00:00', 'supe', '2014-05-27 00:00:00', '', '', '2014-05-27 02:24:15', '2014-05-27 02:24:15'),
	(77, 1, '23234', 'AJFH', '2014-05-28 00:00:00', 'AJDKFH', 'JASDK', '2014-05-28 00:00:00', 'JSYDKE', '2014-05-28 00:00:00', '', '', '2014-05-28 01:56:56', '2014-05-28 04:44:20'),
	(78, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 04:24:54', '2014-06-04 04:24:54'),
	(79, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 04:26:41', '2014-06-04 04:26:41'),
	(80, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 04:27:36', '2014-06-04 04:27:36'),
	(81, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 04:27:51', '2014-06-04 04:27:51'),
	(82, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:12:25', '2014-06-04 06:12:25'),
	(83, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:13:24', '2014-06-04 06:13:24'),
	(84, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:13:27', '2014-06-04 06:13:27'),
	(85, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:13:38', '2014-06-04 06:13:38'),
	(86, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:13:54', '2014-06-04 06:13:54'),
	(87, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:14:08', '2014-06-04 06:14:08'),
	(88, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:14:26', '2014-06-04 06:14:26'),
	(89, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:15:55', '2014-06-04 06:15:55'),
	(90, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:17:12', '2014-06-04 06:17:12'),
	(91, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:17:30', '2014-06-04 06:17:30'),
	(92, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:17:43', '2014-06-04 06:17:43'),
	(93, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:17:50', '2014-06-04 06:17:50'),
	(94, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:18:00', '2014-06-04 06:18:00'),
	(95, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:18:43', '2014-06-04 06:18:43'),
	(96, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:21:18', '2014-06-04 06:21:18'),
	(97, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:34:19', '2014-06-04 06:34:19'),
	(98, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:47:09', '2014-06-04 06:47:09'),
	(99, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:47:15', '2014-06-04 06:47:15'),
	(101, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:47:45', '2014-06-04 06:47:45'),
	(102, 3, 'akldsf', 'kaldsf', '2014-05-19 00:00:00', 'afdksl', 'kldsf', '2014-05-19 00:00:00', 'akl', '2014-05-19 00:00:00', '', '', '2014-06-04 06:48:00', '2014-06-04 06:48:00');
/*!40000 ALTER TABLE `material_take_offs` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.material_take_off_details
DROP TABLE IF EXISTS `material_take_off_details`;
CREATE TABLE IF NOT EXISTS `material_take_off_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_take_off_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.material_take_off_details: ~89 rows (approximately)
DELETE FROM `material_take_off_details`;
/*!40000 ALTER TABLE `material_take_off_details` DISABLE KEYS */;
INSERT INTO `material_take_off_details` (`id`, `material_take_off_id`, `material_id`, `quantity`, `remarks`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(1, 10, 1, 8, 'remark', 'admin', 'admin', '2014-04-23 04:10:27', '2014-06-05 01:21:32'),
	(2, 6, 1, 23, 'test', 'admin', 'admin', '2014-04-23 04:47:22', '2014-06-05 01:19:47'),
	(3, 11, 0, 0, '', '', '', '2014-05-08 03:45:26', '2014-05-08 03:45:26'),
	(17, 32, 0, 0, '', '', '', '2014-05-19 03:23:19', '2014-05-19 03:23:19'),
	(18, 32, 0, 0, '', '', '', '2014-05-19 03:23:19', '2014-05-19 03:23:19'),
	(19, 34, 0, 0, '0', '', '', '2014-05-19 03:27:54', '2014-05-19 03:27:54'),
	(20, 34, 1, 1, '1', '', '', '2014-05-19 03:27:54', '2014-05-19 03:27:54'),
	(21, 35, 1, 1, '1', '', '', '2014-05-19 03:30:49', '2014-05-19 03:30:49'),
	(22, 35, 2, 2, '2', '', '', '2014-05-19 03:30:49', '2014-05-19 03:30:49'),
	(23, 40, 0, 22, 'just for test', '', '', '2014-05-19 04:00:46', '2014-05-19 04:00:46'),
	(24, 40, 0, 22, 'just for test', '', '', '2014-05-19 04:00:46', '2014-05-19 04:00:46'),
	(25, 41, 0, 22, 'just for test', '', '', '2014-05-19 04:10:23', '2014-05-19 04:10:23'),
	(26, 41, 0, 22, 'just for test', '', '', '2014-05-19 04:10:23', '2014-05-19 04:10:23'),
	(27, 41, 0, 22, 'just for test', '', '', '2014-05-19 04:10:23', '2014-05-19 04:10:23'),
	(28, 42, 0, 22, 'just for test', '', '', '2014-05-19 04:15:13', '2014-05-19 04:15:13'),
	(29, 42, 0, 22, 'just for test', '', '', '2014-05-19 04:15:13', '2014-05-19 04:15:13'),
	(30, 42, 0, 22, 'just for test', '', '', '2014-05-19 04:15:13', '2014-05-19 04:15:13'),
	(31, 43, 0, 22, 'just for test', '', '', '2014-05-19 04:26:55', '2014-05-19 04:26:55'),
	(32, 43, 0, 22, 'just for test', '', '', '2014-05-19 04:26:55', '2014-05-19 04:26:55'),
	(33, 43, 0, 22, 'just for test', '', '', '2014-05-19 04:26:55', '2014-05-19 04:26:55'),
	(34, 43, 0, 22, 'just for test', '', '', '2014-05-19 04:26:55', '2014-05-19 04:26:55'),
	(35, 44, 1, 0, '', 'admin', 'admin', '2014-05-19 04:33:01', '2014-06-04 15:29:11'),
	(36, 44, 2, 0, '', 'admin', 'admin', '2014-05-19 04:33:01', '2014-06-04 15:29:11'),
	(37, 44, 3, 0, '', 'admin', 'admin', '2014-05-19 04:33:01', '2014-06-04 15:29:11'),
	(38, 44, 4, 0, '', 'admin', 'admin', '2014-05-19 04:33:01', '2014-06-04 15:29:11'),
	(39, 45, 2, 22, 'just for test', '', '', '2014-05-19 05:47:23', '2014-05-19 05:47:23'),
	(40, 46, 1, 0, '', '', '', '2014-05-19 06:20:19', '2014-05-19 06:20:19'),
	(41, 46, 2, 20, 'mantap', '', '', '2014-05-19 06:20:19', '2014-05-19 06:20:19'),
	(42, 46, 3, 23, 'super', '', '', '2014-05-19 06:20:19', '2014-05-19 06:20:19'),
	(43, 47, 2, 12, 'akd', '', '', '2014-05-19 06:22:38', '2014-05-19 06:22:38'),
	(44, 47, 4, 32, 'hebat', '', '', '2014-05-19 06:22:38', '2014-05-19 06:22:38'),
	(47, 49, 2, 12, 'akldf', 'admin', 'admin', '2014-05-19 06:32:23', '2014-05-19 06:32:23'),
	(48, 49, 3, 23, 'alkf', 'admin', 'admin', '2014-05-19 06:32:23', '2014-05-19 06:32:23'),
	(49, 50, 2, 12, 'akldf', 'admin', 'admin', '2014-05-19 06:32:28', '2014-05-19 06:32:28'),
	(50, 50, 3, 23, 'alkf', 'admin', 'admin', '2014-05-19 06:32:28', '2014-05-19 06:32:28'),
	(51, 51, 2, 12, 'akldf', 'admin', 'admin', '2014-05-19 06:40:50', '2014-05-19 06:40:50'),
	(52, 51, 3, 23, 'alkf', 'admin', 'admin', '2014-05-19 06:40:50', '2014-05-19 06:40:50'),
	(53, 52, 2, 32, 'akle', 'admin', 'admin', '2014-05-19 06:49:52', '2014-05-19 06:49:52'),
	(54, 52, 3, 23, 'kldle', 'admin', 'admin', '2014-05-19 06:49:52', '2014-05-19 06:49:52'),
	(55, 54, 2, 11, 'aldfs', 'admin', 'admin', '2014-05-19 08:36:57', '2014-05-19 08:36:57'),
	(56, 54, 3, 2, 'adfjkej', 'admin', 'admin', '2014-05-19 08:36:58', '2014-05-19 08:36:58'),
	(57, 55, 2, 11, 'aldfs', 'admin', 'admin', '2014-05-19 08:38:38', '2014-05-19 08:38:38'),
	(58, 55, 3, 2, 'adfjkej', 'admin', 'admin', '2014-05-19 08:38:38', '2014-05-19 08:38:38'),
	(59, 56, 1, 33, 'super remarks', 'admin', 'admin', '2014-05-22 04:08:45', '2014-05-22 04:08:45'),
	(60, 56, 2, 2, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:08:46', '2014-05-22 04:08:46'),
	(61, 56, 3, 3, 'coba remarks', 'admin', 'admin', '2014-05-22 04:08:46', '2014-05-22 04:08:46'),
	(62, 57, 1, 24, 'super', 'admin', 'admin', '2014-05-22 04:10:28', '2014-05-22 04:10:28'),
	(63, 57, 2, 33, 'super remarks', 'admin', 'admin', '2014-05-22 04:10:28', '2014-05-22 04:10:28'),
	(64, 57, 3, 2, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:10:28', '2014-05-22 04:10:28'),
	(65, 57, 4, 3, 'coba remarks', 'admin', 'admin', '2014-05-22 04:10:28', '2014-05-22 04:10:28'),
	(66, 58, 1, 24, 'super', 'admin', 'admin', '2014-05-22 04:22:33', '2014-05-22 04:22:33'),
	(67, 58, 2, 38, 'super remarks', 'admin', 'admin', '2014-05-22 04:22:34', '2014-05-22 04:22:34'),
	(68, 58, 3, 36, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:22:34', '2014-05-22 04:22:34'),
	(69, 58, 4, 37, 'coba remarks', 'admin', 'admin', '2014-05-22 04:22:34', '2014-05-22 04:22:34'),
	(70, 59, 1, 24, 'super remarks', 'admin', 'admin', '2014-05-22 04:24:09', '2014-05-22 04:24:09'),
	(71, 59, 2, 38, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:24:09', '2014-05-22 04:24:09'),
	(72, 59, 3, 36, 'coba remarks', 'admin', 'admin', '2014-05-22 04:24:09', '2014-05-22 04:24:09'),
	(73, 60, 1, 24, 'super', 'admin', 'admin', '2014-05-22 04:24:32', '2014-05-22 04:24:32'),
	(74, 60, 2, 38, 'super remarks', 'admin', 'admin', '2014-05-22 04:24:32', '2014-05-22 04:24:32'),
	(75, 60, 3, 36, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:24:32', '2014-05-22 04:24:32'),
	(76, 60, 4, 37, 'coba remarks', 'admin', 'admin', '2014-05-22 04:24:32', '2014-05-22 04:24:32'),
	(77, 61, 1, 24, 'super', 'admin', 'admin', '2014-05-22 04:29:49', '2014-05-22 04:29:49'),
	(78, 61, 2, 38, 'super remarks', 'admin', 'admin', '2014-05-22 04:29:49', '2014-05-22 04:29:49'),
	(79, 61, 3, 36, 'remarks super mantap', 'admin', 'admin', '2014-05-22 04:29:49', '2014-05-22 04:29:49'),
	(80, 61, 4, 37, 'coba remarks', 'admin', 'admin', '2014-05-22 04:29:50', '2014-05-22 04:29:50'),
	(81, 62, 1, 23, 'ini remarksnya', 'admin', 'admin', '2014-05-22 04:32:08', '2014-05-22 04:32:08'),
	(82, 62, 2, 23, 'iniremarks yang kedua', 'admin', 'admin', '2014-05-22 04:32:08', '2014-05-22 04:32:08'),
	(83, 63, 1, 32, 'afsdfa', 'admin', 'admin', '2014-05-22 05:57:35', '2014-05-22 05:57:35'),
	(84, 64, 3, 23, 'qwe', 'admin', 'admin', '2014-05-22 05:59:10', '2014-05-22 05:59:10'),
	(85, 65, 3, 23, 'qwe', 'admin', 'admin', '2014-05-22 05:59:17', '2014-05-22 05:59:17'),
	(86, 66, 1, 23, 'AKLFD', 'admin', 'admin', '2014-05-22 07:04:15', '2014-05-22 07:04:15'),
	(87, 66, 3, 12, 'AKEMN', 'admin', 'admin', '2014-05-22 07:04:15', '2014-05-22 07:04:15'),
	(88, 67, 1, 1, 'arklejalrkalfa', 'admin', 'admin', '2014-05-22 07:44:45', '2014-05-22 07:44:45'),
	(89, 68, 2, 23, 'lkajd', 'admin', 'admin', '2014-05-23 01:28:56', '2014-05-23 01:28:56'),
	(90, 69, 2, 23, 'ad', 'admin', 'admin', '2014-05-23 02:43:47', '2014-05-23 02:43:47'),
	(91, 70, 2, 23, 'ad', 'admin', 'admin', '2014-05-23 02:53:14', '2014-05-23 02:53:14'),
	(92, 71, 2, 23, 'ERE', 'admin', 'admin', '2014-05-23 03:30:17', '2014-05-23 03:30:17'),
	(93, 72, 3, 23, 'AKLKW', 'admin', 'admin', '2014-05-23 03:32:50', '2014-05-23 03:32:50'),
	(94, 73, 1, 2, 'fadf', 'admin', 'admin', '2014-05-23 03:43:33', '2014-05-23 03:43:33'),
	(95, 74, 3, 21, 'akle', 'admin', 'admin', '2014-05-23 03:50:23', '2014-05-23 03:50:23'),
	(96, 75, 1, 23, 'affa', 'admin', 'admin', '2014-05-23 06:08:22', '2014-05-23 06:08:22'),
	(97, 76, 1, 23, 'akld', 'admin', 'admin', '2014-05-27 02:24:15', '2014-05-27 02:24:15'),
	(98, 77, 1, 23, 'SYOER', 'admin', 'admin', '2014-05-28 01:56:56', '2014-05-28 01:56:56'),
	(99, 77, 2, 3, 'AKE', 'admin', 'admin', '2014-05-28 01:56:56', '2014-05-28 01:56:56');
/*!40000 ALTER TABLE `material_take_off_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.migrations: ~22 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_04_17_020101_create_material_categories_table', 1),
	('2014_04_17_022419_create_material_constructions_table', 1),
	('2014_04_17_031456_create_material_construction_details_table', 1),
	('2014_04_17_033243_create_nonconformance_details_table', 1),
	('2014_04_17_034854_create_projects_table', 1),
	('2014_04_17_035532_create_material_docs_table', 1),
	('2014_04_17_040433_create_project_docs_table', 1),
	('2014_04_17_040944_create_purchase_orders_table', 1),
	('2014_04_17_041032_create_material_issuances_table', 1),
	('2014_04_17_041503_create_material_issuance_details_table', 1),
	('2014_04_17_041929_create_purchase_order_details_table', 1),
	('2014_04_17_042105_create_material_receivings_table', 1),
	('2014_04_17_042524_create_material_receiving_details_table', 1),
	('2014_04_17_042612_create_material_take_off_details_table', 1),
	('2014_04_17_042657_create_nonconformances_table', 1),
	('2014_04_17_043709_create_sysdiagrams_table', 1),
	('2014_04_17_044351_create_vendors_table', 1),
	('2014_04_17_044746_create_material_take_offs_table', 1),
	('2014_04_17_054440_create_warehouses_table', 1),
	('2014_04_17_074513_create_materials_table', 1),
	('2014_04_18_083356_create_password_reminders_table', 2),
	('2014_04_18_074513_create_users_table', 3),
	('2014_04_16_074513_create_users_table', 4),
	('2014_05_16_033404_create_session_table', 5),
	('2014_06_05_041032_create_material_issuances_table', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.nonconformance_details
DROP TABLE IF EXISTS `nonconformance_details`;
CREATE TABLE IF NOT EXISTS `nonconformance_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nonconformance_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.nonconformance_details: ~0 rows (approximately)
DELETE FROM `nonconformance_details`;
/*!40000 ALTER TABLE `nonconformance_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `nonconformance_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.nonconormances
DROP TABLE IF EXISTS `nonconormances`;
CREATE TABLE IF NOT EXISTS `nonconormances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nonconformance_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nonconformance_date` datetime NOT NULL,
  `nonconformance_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nonconformance_created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nonconformance_created_date` datetime NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.nonconormances: ~0 rows (approximately)
DELETE FROM `nonconormances`;
/*!40000 ALTER TABLE `nonconormances` DISABLE KEYS */;
/*!40000 ALTER TABLE `nonconormances` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.password_reminders
DROP TABLE IF EXISTS `password_reminders`;
CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.password_reminders: ~0 rows (approximately)
DELETE FROM `password_reminders`;
/*!40000 ALTER TABLE `password_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reminders` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `budget` decimal(8,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `duration` int(11) NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.projects: ~3 rows (approximately)
DELETE FROM `projects`;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `code`, `name`, `description`, `owner_name`, `owner_address`, `owner_phone`, `budget`, `start_date`, `duration`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(1, 'PRJ001', 'Project 1', 'Super', 'Frasnsiskus', 'Jln Ahmad yani', '938434050345345', 20.00, '2014-04-23 00:00:00', 40, '', '', '2014-04-18 09:24:36', '2014-04-23 01:15:54'),
	(2, 'PRJ002', 'Project 2', 'Super description', 'Super owner', 'jln abc', '03294234234234', 40.00, '2014-04-30 00:00:00', 80, '', '', '2014-04-22 02:13:44', '2014-04-23 01:49:23'),
	(3, 'PRJ003', 'Super', 'description', 'sudpf', 'akdfjl', '23094234', 9000.00, '2014-04-23 00:00:00', 50, '', '', '2014-04-22 02:16:35', '2014-04-23 01:37:33');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.project_docs
DROP TABLE IF EXISTS `project_docs`;
CREATE TABLE IF NOT EXISTS `project_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `doc_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `octet_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.project_docs: ~0 rows (approximately)
DELETE FROM `project_docs`;
/*!40000 ALTER TABLE `project_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_docs` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.purchase_orders
DROP TABLE IF EXISTS `purchase_orders`;
CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_take_off_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `po_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_date` datetime NOT NULL,
  `po_estimate_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_promised_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.purchase_orders: ~0 rows (approximately)
DELETE FROM `purchase_orders`;
/*!40000 ALTER TABLE `purchase_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_orders` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.purchase_order_details
DROP TABLE IF EXISTS `purchase_order_details`;
CREATE TABLE IF NOT EXISTS `purchase_order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.purchase_order_details: ~0 rows (approximately)
DELETE FROM `purchase_order_details`;
/*!40000 ALTER TABLE `purchase_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_details` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.sessions
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.sessions: ~0 rows (approximately)
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'erinurshofa@gmail.com', '$2y$10$JxiEpsW/zIyzcLuBXCi2T.1KVgixHyf8I9p3sxOaNNtCWUhJpeMRq', 'WVzscsLKu9jqBPvAmfALY7EzvSgVXmHKECvSIQ50iLM4ievsdG7PpX0npueo', '2014-04-21 08:46:40', '2014-04-22 07:38:06'),
	(2, 'john', 'john@gmail.com', '$2y$10$1Aez.Kxbf9We133G4u11tenFjRG7/pZlbD8E2PvyXeV0ts7Fp6R5i', NULL, '2014-04-21 09:43:52', '2014-04-21 09:43:52'),
	(4, 'pisa', 'pisa@gmail.com', '$2y$10$xFFnYi9Wo1EbB973vixqzuNkJ4r5.y8xCgBsUvAPrs/c3KexNbxhu', NULL, '2014-04-21 09:45:19', '2014-04-21 09:45:19'),
	(6, 'super', 'super@gmail.com', '$2y$10$gTuAuEXvEaZq/t/d4PJd9e4uO/D1qaSgchkMh5F1xfattD2yW2FKq', NULL, '2014-04-21 09:47:08', '2014-04-21 09:47:08'),
	(7, '', '', '', NULL, '2014-05-06 01:32:57', '2014-05-06 01:32:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.vendors
DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.vendors: ~1 rows (approximately)
DELETE FROM `vendors`;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` (`id`, `code`, `name`, `description`, `address`, `phone`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(15, 'VR001', 'nokia', 'this is description', 'jln syuhada', '0932039288', 'john', 'john', '2014-04-21 08:46:40', '2014-04-21 08:46:40');
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;


-- Dumping structure for table apptrackingdb.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `administrator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table apptrackingdb.warehouses: ~1 rows (approximately)
DELETE FROM `warehouses`;
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` (`id`, `code`, `description`, `address`, `phone`, `administrator`, `created_user`, `updated_user`, `created_at`, `updated_at`) VALUES
	(15, 'WR001', 'Gudang 1', 'Jln Syuhada', '082348293432', 'admin', 'john', '', '2014-04-21 08:46:40', '2014-04-21 08:46:40');
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
